const test = require('ava')
const Promise = require('bluebird')
const bcrypt = require('bcrypt')
const {DateTime,} = require('luxon')

require('./common')

test.serial(`An authenticated user can create a new group`, async (t) => {
  const [user,] = await setupDatabase(t)
  await logUserIn(t, user)

  const {page, pgPool,} = t.context
  const name = 'Test'
  const addBtnSel = '#submit-add-group-button'
  const inputPathSel = '#input-path'
  const description = 'This is a test'

  t.log(`Navigating to groups page`)
  await page.goto(`${t.context.rootUrl}/groups`)

  const newGroupBtn = await page.waitForSelector('#new-group-button', {visible: true,})
  await Promise.all([
    page.waitForNavigation(),
    newGroupBtn.click(),
  ])
  let url = new URL(page.url())
  t.is(url.pathname, `/new-group`)

  await page.waitForSelector('#new-group-view', {visible: true,})
  t.is(await page.$eval('#new-group-view h1', (elem) => {return elem.innerText}), 'Add a Group')
  const nameInputElem = await page.waitForSelector('#input-name', {visible: true,})
  const pathInputElem = await page.waitForSelector(inputPathSel, {visible: true,})
  const descriptionEditorElem = await page.waitForSelector('#description-editor')
  const addGroupBtnElem = await page.waitForSelector(addBtnSel)
  const cancelAddGroupBtnElem = await page.waitForSelector('#cancel-add-group-button')
  t.is(await nameInputElem.evaluate((elem) => {return elem.required}), true)
  t.is(await pathInputElem.evaluate((elem) => {return elem.required}), true)
  t.is(await addGroupBtnElem.evaluate((elem) => {return !elem.disabled}), false)
  t.is(await cancelAddGroupBtnElem.evaluate((elem) => {return !elem.disabled}), true)

  t.log('Filling in form')
  await nameInputElem.type(name)
  // The path should be deduced from the name by default
  await page.waitForFunction((inputPathSel, name) => {
    const text = document.querySelector(inputPathSel).value
    return text === name.toLowerCase()
  }, {}, inputPathSel, name)
  t.log('Verifying that form can be submitted')
  await page.waitForFunction((addBtnSel) => {
    return !document.querySelector(addBtnSel).disabled
  }, {}, addBtnSel)

  await descriptionEditorElem.type(description)

  t.log('Submitting form')
  await Promise.all([
    page.waitForNavigation(),
    addGroupBtnElem.click(),
  ])

  t.log('Verifying that group page is navigated to')
  url = new URL(page.url())
  t.true(/\/g\/.+/.test(url.pathname))
  const nameElem = await page.waitForSelector('#group-name', {visible: true,})
  t.is(await nameElem.evaluate((elem) => {return elem.innerText}), name)
  const pathElem = await page.waitForSelector('#group-path', {visible: true,})
  t.is(await pathElem.evaluate((elem) => {return elem.innerText}), name.toLowerCase())

  t.log('Verifying database records')
  let row
  const client = await pgPool.connect()
  try {
    const rslt = await client.query(`
      SELECT groups.path, groups.name, groups.avatar_url, groups.description, users_groups.user_id,
      users_groups.membership FROM groups
      INNER JOIN users_groups ON users_groups.group_id = groups.id
      `)
    t.is(rslt.rows.length, 1)
    row = rslt.rows[0]
  } finally {
    client.release()
  }

  t.deepEqual(row, {
    path: name.toLowerCase(),
    name,
    avatar_url: null,
    description,
    user_id: user.id,
    membership: 'admin',
  })
})

test.serial(`An unauthenticated user cannot create a new group`, async (t) => {
  const {page,} = t.context

  t.log(`Navigating to groups page`)
  await page.goto(`${t.context.rootUrl}/groups`)
  await page.waitForSelector('#groups-view', {visible: true,})

  t.true(await page.$('#new-group-button') == null)
})

const setupDatabase = async (t) => {
  const {pgPool,} = t.context

  const added = DateTime.utc()
  const user = {
    username: 'test',
    name: 'Some User',
    emailAddress: 'test@example.com',
    added,
    bio: 'About a user.',
    password: 'p@ssword1234',
  }
  const passwordHash = await bcrypt.hash(user.password, 10)

  t.log('Inserting test data into database', {
    user,
  })
  const client = await pgPool.connect()
  let userId
  try {
    const rslt = await client.query(`
      INSERT INTO users (username, name, email_address, password_hash, added,
      bio) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id`, [user.username, user.name,
        user.emailAddress, passwordHash, added.toISO(), user.bio,])
    userId = rslt.rows[0].id
  } finally {
    client.release()
  }

  user.id = userId

  return [user,]
}

const logUserIn = async(t, user) => {
  const {page,} = t.context

  t.log(`Navigating to ${t.context.rootUrl}/login`)
  await page.goto(`${t.context.rootUrl}/login`)
  t.log(`Waiting for login form to become visible`)
  await page.waitForSelector('#login-form', {visible: true,})
  const loginButton = await page.$('#submit-login-button')

  await page.type('#login-input-usernameoremail', user.username)
  await page.type('#login-input-password', user.password)
  await page.waitForFunction(() => {
    const btn = document.querySelector(`#submit-login-button`)
    return !btn.disabled
  })
  t.log('Logging user in')
  await Promise.all([
    page.waitForNavigation(),
    loginButton.click(),
  ])

  let url = new URL(page.url())
  t.is(url.pathname, '/')
}
