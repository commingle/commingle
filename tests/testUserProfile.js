const test = require('ava')
const Promise = require('bluebird')
const bcrypt = require('bcrypt')
const {DateTime,} = require('luxon')

require('./common')

test.serial(`An unauthenticated user can visit another user's profile`, async (t) => {
  const [user, group,] = await setupDatabase(t)
  const {page,} = t.context

  t.log(`Navigating to user profile page`)
  await page.goto(`${t.context.rootUrl}/u/${user.username}`)

  await page.waitForSelector('#user-avatar', {visible: true,})
  t.is(await page.$eval('#user-name', (elem) => {return elem.innerText}), user.name)
  t.is(await page.$eval('#user-username', (elem) => {return elem.innerText}), user.username)
  t.is(await page.$eval('#user-bio', (elem) => {return elem.innerText}), user.bio)
  const addedStr = `Joined ${user.added.toLocal().toLocaleString(DateTime.DATE_FULL)}`
  t.is(await page.$eval('#user-join-date', (elem) => {return elem.innerText}), addedStr)
  t.is(await page.$eval('#user-num-meets-gone-to', (elem) => {return elem.innerText}),
    'Has gone to 0 meets')
  t.is(await page.$eval('#user-num-groups', (elem) => {return elem.innerText}), 'Member of 1 group')

  const infoTabs = await page.$$eval('#user-info-tabs .tab', (elems) => {
    return elems.map((elem) => {
      return elem.innerText
    })
  })
  t.deepEqual(infoTabs, ['Activity', 'Groups',])
  const activityTabs = await page.$$eval('#user-activity-header .tab', (elems) => {
    return elems.map((elem) => {
      return elem.innerText
    })
  })
  t.deepEqual(activityTabs, ['Upcoming', 'Past',])
  t.is(await page.$eval('#upcoming-meets', (elem) => {return elem.innerText}),
    `${user.name} doesn't have any upcoming meets.`)

  t.log('Clicking Past tab')
  const pastTabElem = await page.waitForSelector('#past-tab', {visible: true,})
  await pastTabElem.click()
  await page.waitForSelector('#past-meets', {visible: true,})
  t.is(await page.$eval('#past-meets', (elem) => {return elem.innerText}),
    `${user.name} hasn't gone to any meets.`)

  t.log('Clicking Groups tab')
  const groupsTabElem = await page.waitForSelector('#groups-tab', {visible: true,})
  await groupsTabElem.click()
  await page.waitForSelector('#user-groups', {visible: true,})
  const groups = await page.$$eval('#user-groups .group-container', (elems) => {
    return elems.map((elem) => {
      const link = elem.querySelector('.group-content > a').pathname
      const name = elem.querySelector('.group-name').innerText
      const description = elem.querySelector('.group-description').innerText
      return {
        link,
        name,
        description,
      }
    })
  })
  t.deepEqual(groups, [
    {
      link: `/g/${group.path}`,
      name: group.name,
      description: group.description,
    },
  ])
})

test.serial(`An authenticated user can visit their own profile`, async (t) => {
  const [user,] = await setupDatabase(t)
  await logUserIn(t, user)
  const {page,} = t.context

  t.log(`Navigating to user profile page`)
  const accountIconElem = await page.waitForSelector('#account-icon', {visible: true,})
  await accountIconElem.click()
  const profileLinkElem = await page.waitForSelector('#profile-link', {visible: true,})
  await Promise.all([
    page.waitForNavigation(),
    profileLinkElem.click(),
  ])
  const url = new URL(page.url())
  t.is(url.pathname, `/u/${user.username}`)

  await page.waitForSelector('#user-avatar', {visible: true,})
  t.is(await page.$eval('#user-name', (elem) => {return elem.innerText}), user.name)
  t.is(await page.$eval('#user-username', (elem) => {return elem.innerText}), user.username)
  t.is(await page.$eval('#user-bio', (elem) => {return elem.innerText}), user.bio)
  const addedStr = `Joined ${user.added.toLocal().toLocaleString(DateTime.DATE_FULL)}`
  t.is(await page.$eval('#user-join-date', (elem) => {return elem.innerText}), addedStr)
  t.is(await page.$eval('#user-num-meets-gone-to', (elem) => {return elem.innerText}),
    'Has gone to 0 meets')
  t.is(await page.$eval('#user-num-groups', (elem) => {return elem.innerText}), 'Member of 1 group')
})

const logUserIn = async(t, user) => {
  const {page,} = t.context

  t.log(`Navigating to ${t.context.rootUrl}/login`)
  await page.goto(`${t.context.rootUrl}/login`)
  t.log(`Waiting for login form to become visible`)
  await page.waitForSelector('#login-form', {visible: true,})
  const loginButton = await page.$('#submit-login-button')

  await page.type('#login-input-usernameoremail', user.username)
  await page.type('#login-input-password', user.password)
  await page.waitForFunction(() => {
    const btn = document.querySelector(`#submit-login-button`)
    return !btn.disabled
  })
  t.log('Logging user in')
  await Promise.all([
    page.waitForNavigation(),
    loginButton.click(),
  ])

  let url = new URL(page.url())
  t.is(url.pathname, '/')
}

const setupDatabase = async (t) => {
  const {pgPool,} = t.context

  const added = DateTime.utc()
  const user = {
    username: 'test',
    name: 'Some User',
    emailAddress: 'test@example.com',
    added,
    bio: 'About a user.',
    password: 'p@ssword1234',
  }
  const passwordHash = await bcrypt.hash(user.password, 10)
  const group = {
    path: 'test',
    name: 'Test',
    added,
    description: 'A test group',
    members: {
      [user.username]: 'admin',
    },
  }

  t.log('Inserting test data into database', {
    user,
    group,
  })
  const client = await pgPool.connect()
  let userId
  let groupId
  try {
    let rslt = await client.query(`INSERT INTO users (username, name, email_address, password_hash, added,
      bio) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id`, [user.username, user.name, user.emailAddress,
        passwordHash, added.toISO(), user.bio,])
    userId = rslt.rows[0].id
    rslt = await client.query(`INSERT INTO groups (path, name, added, description)
      VALUES ($1, $2, $3, $4) RETURNING id`, [group.path, group.name, added.toISO(),
        group.description,])
    groupId = rslt.rows[0].id
    await client.query(`INSERT INTO users_groups (group_id, user_id, added, membership)
      VALUES ($1, $2, $3, 'admin')`, [groupId, userId, added.toISO(),])
  } finally {
    client.release()
  }

  user.id = userId
  group.id = groupId

  return [user, group,]
}
