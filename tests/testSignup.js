const test = require('ava')
const Promise = require('bluebird')

require('./common')

test.serial('A user can sign up', async (t) => {
  const {page,} = t.context

  const username = 'test'
  const password = 'p@ssword1234'

  // First go to the homepage, in order to test viewing this before and after signup
  await page.goto(`${t.context.rootUrl}`)
  await page.waitForSelector('#home-tab-content', {visible: true,})

  t.log(`Navigating to ${t.context.rootUrl}/signup`)
  await page.goto(`${t.context.rootUrl}/signup`)
  t.log(`Waiting for signup form to become visible`)
  await page.waitForSelector('#signup-form', {visible: true,})
  const signupButton = await page.$('#submit-signup-button')
  const isSignupDisabled = await signupButton.evaluate((elem) => {
    return elem.disabled
  })
  t.is(isSignupDisabled, true)

  await page.type('#signup-input-email', 'test@example.com')
  await page.type('#signup-input-username', username)
  await page.type('#signup-input-name', 'Test Johnson')
  await page.type('#signup-input-password', password)
  await page.waitForFunction(() => {
    const btn = document.querySelector(`#submit-signup-button`)
    return !btn.disabled
  })
  t.log('Signing user up')
  await Promise.all([
    page.waitForNavigation(),
    signupButton.click(),
  ])

  let url = new URL(page.url())
  t.is(url.pathname, '/')
  await page.waitForSelector('#account-icon', {visible: true,})
  await page.click('#account-icon')
  await page.waitForSelector('#logout-link', {visible: true,})
  t.log('Logging user out')
  await page.click('#logout-link')

  const loginElem = await page.waitForSelector('#login-link', {visible: true,})
  url = new URL(page.url())
  t.is(url.pathname, '/')
  t.log('Going to login page')
  await Promise.all([
    page.waitForNavigation(),
    loginElem.click(),
  ])

  url = new URL(page.url())
  t.is(url.pathname, '/login')
  await page.waitForSelector('#login-form', {visible: true,})
  const loginButton = await page.$('#submit-login-button')
  const isLoginDisabled = await loginButton.evaluate((elem) => {
    return elem.disabled
  })
  t.is(isLoginDisabled, true)
  await page.type('#login-input-usernameoremail', username)
  await page.type('#login-input-password', password)
  await page.waitForFunction(() => {
    const btn = document.querySelector(`#submit-login-button`)
    return !btn.disabled
  })
  t.log('Logging user in')
  await Promise.all([
    page.waitForNavigation(),
    loginButton.click(),
  ])

  url = new URL(page.url())
  t.is(url.pathname, '/')
  await page.waitForSelector('#account-icon', {visible: true,})
  await page.waitForSelector('#upcoming-meets', {visible: true,})
})
