const puppeteer = require('puppeteer')
const crypto = require('crypto')
const mergeRight = require('ramda/src/mergeRight')
const url = require('url')
const Promise = require('bluebird')
const childProcess = require('child_process')
const got = require('got')
const test = require('ava')
const pg = require('pg')
const dotenv = require('dotenv')
const http = require('http')

const DB_NAME = `commingle_test_${crypto.randomBytes(20).toString('hex')}`

const waitOnServer = async (t, serverExitPromise) => {
  const {rootUrl,} = t.context

  await Promise.delay(1000)

  let attempt = 1
  const interval = 1000
  const maxAttempts = 10
  while (serverExitPromise.isPending() && attempt < maxAttempts) {
    try {
      await got(`${rootUrl}/healthz`)
    } catch (error) {
      ++attempt
      await Promise.delay(interval)
      continue
    }

    return
  }

  throw new Error(`Failed to connect to server after ${attempt} attempt(s), its stderr:\n${
    t.context.stderrBuffer}\nIts stdout:\n${t.context.stdoutBuffer}`)
}

// Start a fake Mandrill API server.
const startFakeMandrill = async(t) => {
  t.context.mandrillRequests = []

  const expPath = '/send.json'

  const server = http.createServer({}, (req, res) => {
    const path = url.parse(req.url).pathname
    const method = req.method.toLowerCase()
    t.log(`Received request to fake Mandrill API, path: '${path}', method: ${method}`)
    if (path !== expPath) {
      t.log(`Request is for invalid path`)
      res.writeHead(404)
      res.end()
      return
    }
    if (method !== 'post') {
      t.log(`Request has invalid method`)
      res.writeHead(404)
      res.end()
      return
    }

    let body = ''
    req.on('data', (chunk) => {
      body += chunk
    })
    req.on('end', () => {
      t.log(`Received Mandrill request payload:`, body)
      const payload = JSON.parse(body)
      t.context.mandrillRequests.push(payload)

      res.writeHead(200)
      res.end()
    })
  })
  server.listen()
  t.context.fakeMandrillServer = server

  const port = server.address().port
  return `http://localhost:${port}${expPath}`
}

const startFakeGMaps = async (t) => {
  const gmapsProcess = childProcess.spawn('cargo', [
    'run',
    '-p',
    'gmaps_api',
  ])
  t.context.gmapsProcess = gmapsProcess
  gmapsProcess.on('exit', (code) => {
    t.fail(`Fake Google Maps API server exited cleanly with code ${code}`)
  })
  gmapsProcess.on('error', (err) => {
    t.fail(`Fake Google Maps API server exited with an error: ${err}`)
  })

  t.context.gmapsStdoutBuffer = ''
  const readyPromise = new Promise((resolve) => {
    let stillLooking = true
    gmapsProcess.stdout.on('data', (data) => {
      data = data.toString()
      t.context.gmapsStdoutBuffer += data
      if (stillLooking) {
        const m = /\* Listening on port (\d+)/.exec(t.context.gmapsStdoutBuffer)
        if (m != null) {
          stillLooking = false
          const port = m[1]
          t.log(`Fake Google Maps API server is ready and listening on port ${port}`)
          resolve([port,])
        }
      }
    })
  })
  t.context.gmapsStderrBuffer = ''
  gmapsProcess.stderr.on('data', (data) => {
    data = data.toString()
    t.context.gmapsStderrBuffer += data
  })
  const [port,] = await readyPromise

  return `http://localhost:${port}`
}

test.serial.beforeEach('Create database', async (t) => {
  dotenv.config()

  const pgUser = process.env.DATABASE_USER
  const pgPassword = process.env.DATABASE_PASSWORD
  const pgHost = process.env.DATABASE_HOST || 'localhost'
  t.context.pgUser = pgUser
  t.context.pgPassword = pgPassword
  t.context.pgHost = pgHost
  const pgClient = new pg.Client({
    host: pgHost,
    user: pgUser,
    password: pgPassword,
    database: 'postgres',
  })
  await pgClient.connect()

  t.log(`Creating database ${DB_NAME}`)
  try {
    await pgClient.query(`CREATE DATABASE ${DB_NAME}`)
  } finally {
    await pgClient.end()
  }

  t.context.pgPool = new pg.Pool({
    host: pgHost,
    user: pgUser,
    password: pgPassword,
    database: DB_NAME,
  })
})

test.serial.beforeEach('Launch server', async (t) => {
  const mandrillUrl = await startFakeMandrill(t)
  const gmapsUrl = await startFakeGMaps(t)

  const env = mergeRight(process.env, {
    MAPS_API_KEY: 'test',
    MAPS_STATIC_SIGNING_KEY: 'test',
    MANDRILL_API_KEY: 'test',
    RUST_BACKTRACE: '1',
  })
  t.log(`Starting server`)
  const serverProcess = childProcess.spawn('cargo', [
    'run',
    '--',
    '-D', DB_NAME,
    '-p', 0,
    '--mandrill-api-url', mandrillUrl,
    '--gmaps-api-url', gmapsUrl,
  ], {
    env,
  })
  t.context.serverProcess = serverProcess
  const serverExitPromise = new Promise((resolve) => {
    serverProcess.on('exit', (code) => {
      t.log(`Server exited cleanly with code ${code}`)
      resolve()
    })
    serverProcess.on('error', (err) => {
      t.log(`Server exited with an error`, err)
      resolve()
    })
  })
  t.context.serverExitPromise = serverExitPromise

  t.context.stdoutBuffer = ''
  const serverReadyPromise = new Promise((resolve) => {
    let stillLooking = true
    serverProcess.stdout.on('data', (data) => {
      data = data.toString()
      t.context.stdoutBuffer += data
      if (stillLooking) {
        const m = /\* Serving on localhost:(\d+)/.exec(t.context.stdoutBuffer)
        if (m != null) {
          stillLooking = false
          const port = m[1]
          t.log(`Server is ready and listening on port ${port}`)
          resolve([port,])
        }
      }
    })
  })
  t.context.stderrBuffer = ''
  serverProcess.stderr.on('data', (data) => {
    data = data.toString()
    t.context.stderrBuffer += data
  })
  await Promise.any([serverExitPromise, serverReadyPromise,])
  if (serverExitPromise.isPending()) {
    const [port,] = await serverReadyPromise
    t.context.rootUrl = `http://localhost:${port}`
    await waitOnServer(t, serverExitPromise)
  } else {
    throw new Error(
      `Server exited unexpectedly, its stderr:\n${t.context.stderrBuffer}\nIts stdout:\n${
        t.context.stdoutBuffer}`)
  }
})

test.serial.beforeEach('Launch browser', async (t) => {
  const headless = (process.env.TEST_HEADLESS || '') !== 'false'
  t.log(`Launching browser`, {headless,})
  // Specify a certain locale to ensure deterministic date/time formatting
  let args = ['--lang=en-US',]
  if (process.env.GITLAB_CI === 'true') {
    args.push('--no-sandbox')
  }
  const browser = await puppeteer.launch({
    headless,
    args,
  })
  t.context.browser = browser
  t.context.page = await browser.newPage()
  t.context.page.on('pageerror', (error) => {
    t.fail(`An error was caught in the browser: ${error}`)
  })
})

test.serial.afterEach.always('Close browser', async (t) => {
  if (t.context.browser != null) {
    await t.context.browser.close()
  }
})

test.serial.afterEach.always('Stop server', async (t) => {
  const {serverProcess, serverExitPromise, gmapsProcess,} = t.context
  if (serverProcess == null) {
    return
  }

  t.log(`Terminating server`)
  serverProcess.kill()
  await serverExitPromise

  if (process.env.COMMINGLE_DUMP_SERVER_LOGS === 'true') {
    t.log(`The server's stdout:\n${t.context.stdoutBuffer}`)
    t.log(`The server's stderr:\n${t.context.stderrBuffer}`)
  }

  t.log(`Stopping fake Mandrill server`)
  await Promise.promisify(t.context.fakeMandrillServer.close,
    {context: t.context.fakeMandrillServer,})()

  if (gmapsProcess != null) {
    t.log(`Stopping fake Google Maps API server`)
    gmapsProcess.kill()
    if (process.env.COMMINGLE_DUMP_SERVER_LOGS === 'true') {
      t.log(`The fake Google Maps API server's stdout:\n${t.context.gmapsStdoutBuffer}`)
      t.log(`The fake Google Maps API server's stderr:\n${t.context.gmapsStderrBuffer}`)
    }
  }
})

test.serial.afterEach.always('Drop database', async (t) => {
  const {pgPool,} = t.context
  if (pgPool == null) {
    return
  }

  await pgPool.end()

  t.log(`Dropping database ${DB_NAME}`)
  const pgClient = new pg.Client({
    host: t.context.pgHost,
    user: t.context.pgUser,
    password: t.context.pgPassword,
    database: 'postgres',
  })
  await pgClient.connect()
  try {
    // Get rid of left-over sessions using the test database
    await pgClient.query(`SELECT pg_terminate_backend(pid) FROM pg_stat_activity
      WHERE pid <> pg_backend_pid()
      AND datname = $1::text`, [DB_NAME,])
    await pgClient.query(`DROP DATABASE ${DB_NAME}`)
  } finally {
    await pgClient.end()
  }
})
