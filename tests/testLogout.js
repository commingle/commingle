const test = require('ava')

require('./common')

test.serial('Unauthenticated requests to logout page are redirected', async (t) => {
  const {page,} = t.context

  await page.goto(`${t.context.rootUrl}`)
  await page.waitForSelector('#login-link', {visible: true,})

  t.log(`Navigating to ${t.context.rootUrl}/logout`)
  await page.goto(`${t.context.rootUrl}/logout`)
  await page.waitForFunction(() => {
    return window.location.pathname === '/'
  })

  const url = new URL(page.url())
  t.is(url.pathname, '/')
})
