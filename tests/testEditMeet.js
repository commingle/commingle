const test = require('ava')
const Promise = require('bluebird')
const bcrypt = require('bcrypt')
const {DateTime,} = require('luxon')
const range = require('ramda/src/range')

require('./common')

test.serial(`A group admin can edit an owned meet`, async (t) => {
  const {admin, attendee, meet,} = await setupDatabase(t)
  await logUserIn(t, admin)

  const {page, pgPool,} = t.context
  const title = 'New title'
  const saveBtnSel = '#submit-edit-meet-button'
  const description = 'Edited description'

  t.log(`Navigating to meet page`)
  await page.goto(`${t.context.rootUrl}/m/${meet.uuid}`)
  await page.waitForSelector('#meet-view', {visible: true,})

  const editMeetElem = await page.waitForSelector('#meet-edit-control', {visible: true,})
  await Promise.all([
    page.waitForNavigation(),
    editMeetElem.click(),
  ])
  let url = new URL(page.url())
  t.is(url.pathname, `/m/${meet.uuid}/edit`)

  await page.waitForSelector('#edit-meet-view', {visible: true,})
  t.is(await page.$eval('#edit-meet-view h1', (elem) => {return elem.innerText}), 'Edit Meet')
  const titleInputElem = await page.waitForSelector('#input-title', {visible: true,})
  const changeLocBtnElem = await page.waitForSelector('#meet-location-change-button',
    {visible: true,})
  const locNameElem = await page.waitForSelector('#meet-location-name', {visible: true,})
  const locAddrElem = await page.waitForSelector('#meet-location-address', {visible: true,})
  const startDateInputElem = await page.waitForSelector('#input-start-date', {visible: true,})
  const startTimeInputElem = await page.waitForSelector('#input-start-time', {visible: true,})
  const endDateInputElem = await page.waitForSelector('#input-end-date', {visible: true,})
  const endTimeInputElem = await page.waitForSelector('#input-end-time', {visible: true,})
  const descriptionEditorElem = await page.waitForSelector('#wmd-input-markdown', {visible: true,})
  const hostsElem = await page.waitForSelector('#hosts', {visible: true,})
  const saveMeetBtnElem = await page.waitForSelector(saveBtnSel)
  const cancelSaveMeetBtnElem = await page.waitForSelector('#cancel-edit-meet-button',
    {visible: true,})
  t.is(await titleInputElem.evaluate((elem) => {return elem.required}), true)
  t.is(await titleInputElem.evaluate((elem) => {return elem.value}), meet.title)
  t.false(await changeLocBtnElem.evaluate((elem) => {return elem.disabled}))
  t.is(await locNameElem.evaluate((elem) => {return elem.innerText}), meet.locationName)
  t.is(await locAddrElem.evaluate((elem) => {return elem.innerText}), meet.locationAddress)
  const curStartDate = await startDateInputElem.evaluate((elem) => {return elem.value})
  const curStartTime = await startTimeInputElem.evaluate((elem) => {return elem.value})
  const curStartDateTime = DateTime.fromISO(`${curStartDate}T${curStartTime}`)
  t.true(curStartDateTime.equals(meet.starts),
    `${curStartDateTime.toISO()} and ${meet.starts.toISO()} should be the same`)
  const curEndDate = await endDateInputElem.evaluate((elem) => {return elem.value})
  const curEndTime = await endTimeInputElem.evaluate((elem) => {return elem.value})
  const curEndDateTime = DateTime.fromISO(`${curEndDate}T${curEndTime}`)
  t.true(curEndDateTime.equals(meet.ends),
    `${curEndDateTime.toISO()} and ${meet.ends.toISO()} should be the same`)
  t.deepEqual(await hostsElem.$$eval('.host', (elems) => {
    return elems.map((elem) => {return elem.innerText})
  }), [admin.name,])
  t.is(await descriptionEditorElem.evaluate((elem) => {return elem.value}), meet.description)
  t.false(await saveMeetBtnElem.evaluate((elem) => {return elem.disabled}))
  t.false(await cancelSaveMeetBtnElem.evaluate((elem) => {return elem.disabled}))

  t.log('Filling in fields')
  // Clear the input
  await Promise.each(range(0, meet.title.length), async () => {
    await titleInputElem.press('Backspace')
  })
  await titleInputElem.type(title)
  // TODO: Change dates/times
  await changeLocBtnElem.click()
  const inputLocElem = await page.waitForSelector('#input-location', {visible: true,})
  await inputLocElem.type('Hermannplatz, Berlin, Germany')
  const dropdownElem = await page.waitForSelector('.typeahead-dropdown li', {visible: true,})
  await dropdownElem.click()
  // Wait for location change to be effected
  await page.waitForSelector('#meet-location-meta', {visible: true,})
  // Clear the editor
  await Promise.each(range(0, meet.description.length), async () => {
    await descriptionEditorElem.press('Backspace')
  })
  await descriptionEditorElem.type(description)

  t.log('Submitting form')
  await Promise.all([
    page.waitForNavigation(),
    saveMeetBtnElem.click(),
  ])

  t.log('Verifying that meet page is navigated to')
  url = new URL(page.url())
  t.is(url.pathname, `/m/${meet.uuid}`)

  t.log('Verifying database records')
  let row
  const client = await pgPool.connect()
  try {
    const rslt = await client.query(`SELECT title, description FROM meets`)
    t.is(rslt.rows.length, 1)
    row = rslt.rows[0]
  } finally {
    client.release()
  }
  t.deepEqual(row, {
    title,
    description,
  })

  t.log('Waiting for fake Mandrill API server to receive request to send emails...')
  for (let i = 0; i < 4 && t.context.mandrillRequests.length < 2; i++) {
    await Promise.delay(100)
  }

  // Emails should have been sent to the organizer and the attendee
  t.is(t.context.mandrillRequests.length, 2)
  const [payload1, payload2,] = t.context.mandrillRequests
  t.deepEqual(payload1, {
    key: 'test',
    message: {
      subject: `Meet updated: ${title}`,
      html: `<p>The meet <em>${title}</em> has been updated.</p>`,
      // TODO: Change to commingle.xyz once the domain is verified with Mandrill
      from_email: 'contact@experimental.berlin',
      from_name: 'Commingle',
      to: [
        {
          email: admin.emailAddress,
          name: admin.name,
          type: 'to',
        },
      ],
      headers: {
        'Reply-To': '<no-reply>@commingle.xyz',
      },
    },
  })
  t.deepEqual(payload2, {
    key: 'test',
    message: {
      subject: `Meet updated: ${title}`,
      html: `<p>The meet <em>${title}</em> has been updated.</p>`,
      // TODO: Change to commingle.xyz once the domain is verified with Mandrill
      from_email: 'contact@experimental.berlin',
      from_name: 'Commingle',
      to: [
        {
          email: attendee.emailAddress,
          name: attendee.name,
          type: 'to',
        },
      ],
      headers: {
        'Reply-To': '<no-reply>@commingle.xyz',
      },
    },
  })
})

test.serial(`An unauthenticated user cannot edit a meet`, async (t) => {
  const {meet,} = await setupDatabase(t)

  const {page,} = t.context

  t.log(`Navigating to meet page`)
  await page.goto(`${t.context.rootUrl}/m/${meet.uuid}`)
  await page.waitForSelector('#meet-view', {visible: true,})

  t.true(await page.$('#edit-meet-control') == null)
})

const setupDatabase = async (t) => {
  const {pgPool,} = t.context

  const added = DateTime.utc()
  const admin = {
    username: 'test',
    name: 'Some User',
    emailAddress: 'test@example.com',
    added,
    bio: 'About a user.',
    password: 'p@ssword1234',
  }
  const attendee = {
    username: 'attendee',
    name: 'An Attendee',
    emailAddress: 'attendee@example.com',
    added,
    bio: 'About an attendee.',
    password: 'p@ssword1234',
  }
  const passwordHash = await bcrypt.hash(admin.password, 10)
  const group = {
    path: 'test',
    name: 'Test',
    added,
    description: 'A test group',
    members: {
      [admin.username]: 'admin',
    },
  }
  const meet = {
    uuid: '27fd6d6d-d518-446e-b2ca-b7d3dc486d7e',
    title: 'Test',
    starts: DateTime.fromISO('2020-05-10T12:00:00Z'),
    ends: DateTime.fromISO('2020-05-10T14:00:00Z'),
    description: 'Test meet',
    locationPlaceId: 'ChIJbygR2x5OqEcRbhbkZsMB_DA',
    locationName: 'Alexanderplatz',
    locationAddress: '10178 Berlin, Germany',
  }

  t.log('Inserting test data into database', {
    admin,
    attendee,
    group,
    meet,
  })
  const client = await pgPool.connect()
  let adminId
  let attendeeId
  let groupId
  let meetId
  try {
    let rslt = await client.query(`INSERT INTO users (username, name, email_address, password_hash, added,
      bio) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id`, [admin.username, admin.name, admin.emailAddress,
        passwordHash, added.toISO(), admin.bio,])
    adminId = rslt.rows[0].id
    rslt = await client.query(`INSERT INTO users (username, name, email_address, password_hash, added,
      bio) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id`, [attendee.username, attendee.name,
        attendee.emailAddress, passwordHash, added.toISO(), attendee.bio,])
    attendeeId = rslt.rows[0].id
    rslt = await client.query(`INSERT INTO groups (path, name, added, description)
      VALUES ($1, $2, $3, $4) RETURNING id`, [group.path, group.name, added.toISO(),
        group.description,])
    groupId = rslt.rows[0].id
    await client.query(`INSERT INTO users_groups (group_id, user_id, added, membership)
      VALUES ($1, $2, $3, 'admin')`, [groupId, adminId, added.toISO(),])
    rslt = await client.query(
      `INSERT INTO meets (uuid, group_id, title, starts, ends, description, location_place_id,
      location_name, location_address, added) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10) RETURNING id`,
      [
        meet.uuid, groupId, meet.title, meet.starts, meet.ends, meet.description,
        meet.locationPlaceId, meet.locationName, meet.locationAddress, added.toISO(),
      ])
    meetId = rslt.rows[0].id
    await client.query(
      `INSERT INTO meets_hosts (meet_id, user_id, added)
      VALUES ($1, $2, $3)`, [meetId, adminId, added.toISO(),])
    await client.query(
      `INSERT INTO meets_attendees (meet_id, user_id, added)
      VALUES ($1, $2, $3)`, [meetId, attendeeId, added.toISO(),])
  } finally {
    client.release()
  }

  admin.id = adminId
  attendee.id = attendeeId
  group.id = groupId
  meet.groupId = groupId

  return {admin, attendee, group, meet,}
}

const logUserIn = async(t, user) => {
  const {page,} = t.context

  t.log(`Navigating to ${t.context.rootUrl}/login`)
  await page.goto(`${t.context.rootUrl}/login`)
  t.log(`Waiting for login form to become visible`)
  await page.waitForSelector('#login-form', {visible: true,})
  const loginButton = await page.$('#submit-login-button')

  await page.type('#login-input-usernameoremail', user.username)
  await page.type('#login-input-password', user.password)
  await page.waitForFunction(() => {
    const btn = document.querySelector(`#submit-login-button`)
    return !btn.disabled
  })
  t.log('Logging user in')
  await Promise.all([
    page.waitForNavigation(),
    loginButton.click(),
  ])

  let url = new URL(page.url())
  t.is(url.pathname, '/')
}
