const test = require('ava')
const Promise = require('bluebird')
const bcrypt = require('bcrypt')
const {DateTime,} = require('luxon')
const range = require('ramda/src/range')

require('./common')

test.serial(`A group admin can edit the group`, async (t) => {
  const {user, group,} = await setupDatabase(t)
  await logUserIn(t, user)

  const {page, pgPool,} = t.context
  const name = 'New name'
  const saveBtnSel = '#submit-edit-group-button'
  const description = 'Edited description'

  t.log(`Navigating to group profile page`)
  await page.goto(`${t.context.rootUrl}/g/${group.path}`)
  await page.waitForSelector('#group-profile-view', {visible: true,})

  const editGroupBtn = await page.waitForSelector('#edit-group-button', {visible: true,})
  await Promise.all([
    page.waitForNavigation(),
    editGroupBtn.click(),
  ])
  let url = new URL(page.url())
  t.is(url.pathname, `/g/${group.path}/edit`)

  await page.waitForSelector('#edit-group-view', {visible: true,})
  t.is(await page.$eval('#edit-group-view h1', (elem) => {return elem.innerText}), 'Edit Group')
  const nameInputElem = await page.waitForSelector('#input-name', {visible: true,})
  const descriptionEditorElem = await page.waitForSelector('#description-editor', {visible: true,})
  const saveGroupBtnElem = await page.waitForSelector(saveBtnSel, {visible: true,})
  const cancelSaveGroupBtnElem = await page.waitForSelector('#cancel-edit-group-button',
    {visible: true,})
  t.is(await nameInputElem.evaluate((elem) => {return elem.required}), true)
  t.is(await saveGroupBtnElem.evaluate((elem) => {return !elem.disabled}), true)
  t.is(await cancelSaveGroupBtnElem.evaluate((elem) => {return !elem.disabled}), true)

  t.log('Filling in form')
  // Clear the input
  await Promise.each(range(0, group.name.length), async () => {
    await nameInputElem.press('Backspace')
  })
  await nameInputElem.type(name)
  // Clear the editor
  await Promise.each(range(0, group.description.length), async () => {
    await descriptionEditorElem.press('Backspace')
  })
  await descriptionEditorElem.type(description)
  t.log('Verifying that form can be submitted')
  await page.waitForFunction((saveBtnSel) => {
    return !document.querySelector(saveBtnSel).disabled
  }, {}, saveBtnSel)

  t.log('Submitting form')
  await Promise.all([
    page.waitForNavigation(),
    saveGroupBtnElem.click(),
  ])

  t.log('Verifying that group page is navigated to')
  url = new URL(page.url())
  t.true(/\/g\/.+/.test(url.pathname))

  t.log('Verifying database records')
  let row
  const client = await pgPool.connect()
  try {
    const rslt = await client.query(`SELECT name, description FROM groups`)
    t.is(rslt.rows.length, 1)
    row = rslt.rows[0]
  } finally {
    client.release()
  }
  t.deepEqual(row, {
    name,
    description,
  })
})

test.serial(`An unauthenticated user cannot edit a group`, async (t) => {
  const {group,} = await setupDatabase(t)

  const {page,} = t.context

  t.log(`Navigating to group profile page`)
  await page.goto(`${t.context.rootUrl}/g/${group.path}`)
  await page.waitForSelector('#group-profile-view', {visible: true,})

  t.true(await page.$('#edit-group-button') == null)
})

const setupDatabase = async (t) => {
  const {pgPool,} = t.context

  const added = DateTime.utc()
  const user = {
    username: 'test',
    name: 'Some User',
    emailAddress: 'test@example.com',
    added,
    bio: 'About a user.',
    password: 'p@ssword1234',
  }
  const passwordHash = await bcrypt.hash(user.password, 10)
  const group = {
    path: 'test',
    name: 'Test',
    added,
    description: 'A test group',
    members: {
      [user.username]: 'admin',
    },
  }

  t.log('Inserting test data into database', {
    user,
    group,
  })
  const client = await pgPool.connect()
  let userId
  let groupId
  try {
    let rslt = await client.query(`INSERT INTO users (username, name, email_address, password_hash, added,
      bio) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id`, [user.username, user.name, user.emailAddress,
        passwordHash, added.toISO(), user.bio,])
    userId = rslt.rows[0].id
    rslt = await client.query(`INSERT INTO groups (path, name, added, description)
      VALUES ($1, $2, $3, $4) RETURNING id`, [group.path, group.name, added.toISO(),
        group.description,])
    groupId = rslt.rows[0].id
    await client.query(`INSERT INTO users_groups (group_id, user_id, added, membership)
      VALUES ($1, $2, $3, 'admin')`, [groupId, userId, added.toISO(),])
  } finally {
    client.release()
  }

  user.id = userId
  group.id = groupId

  return {user, group,}
}

const logUserIn = async(t, user) => {
  const {page,} = t.context

  t.log(`Navigating to ${t.context.rootUrl}/login`)
  await page.goto(`${t.context.rootUrl}/login`)
  t.log(`Waiting for login form to become visible`)
  await page.waitForSelector('#login-form', {visible: true,})
  const loginButton = await page.$('#submit-login-button')

  await page.type('#login-input-usernameoremail', user.username)
  await page.type('#login-input-password', user.password)
  await page.waitForFunction(() => {
    const btn = document.querySelector(`#submit-login-button`)
    return !btn.disabled
  })
  t.log('Logging user in')
  await Promise.all([
    page.waitForNavigation(),
    loginButton.click(),
  ])

  let url = new URL(page.url())
  t.is(url.pathname, '/')
}
