const test = require('ava')
const Promise = require('bluebird')
const bcrypt = require('bcrypt')
const {DateTime,} = require('luxon')
const sortBy = require('ramda/src/sortBy')
const prop = require('ramda/src/prop')

require('./common')

test.serial('A group can be deleted by one of its admins', async (t) => {
  const {admin, group,} = await setupDatabase(t)
  await logUserIn(t, admin)

  t.log(`Navigating to group profile page`)
  const {page, pgPool,} = t.context
  await page.goto(`${t.context.rootUrl}/g/${group.path}`)

  const deleteButtonElem = await page.waitForSelector('#delete-group-button', {visible: true,})
  const isEnabled = await deleteButtonElem.evaluate((elem) => {
    return !elem.disabled
  })
  t.true(isEnabled)

  t.log(`Deleting group`)
  await deleteButtonElem.click()
  const modalContent = await page.waitForSelector('.modal-body-content', {visible: true,})
  t.is(await modalContent.evaluate((elem) => {return elem.innerText}),
    'Are you really sure you wish to delete this group?')
  const modalYes = await page.waitForSelector('#modal-yes', {visible: true,})
  await Promise.all([
    page.waitForNavigation(),
    modalYes.click(),
  ])
  const url = new URL(page.url())
  t.is(url.pathname, '/')

  t.log(`Verifying that group is deleted client side`)
  const exploreTabElem = await page.waitForSelector('#explore-tab', {visible: true,})
  await exploreTabElem.click()
  const groupsElem = await page.waitForSelector('#explore-groups', {visible: true,})
  t.is(await groupsElem.$$eval('.group-container', (elems) => {return elems.length}), 0)

  t.log(`Navigating to groups page, to verify that group is deleted client side`)
  const groupsLink = await page.waitForSelector('#menu-item-groups', {visible: true,})
  await Promise.all([
    page.waitForNavigation(),
    groupsLink.click(),
  ])
  // Verify that "Your Groups" tab is active
  await page.waitForSelector('#your-groups-tab.active', {visible: true,})
  await Promise.delay(5000)
  const groupNames = await page.$$eval('.group-content .group-name', (elems) => {
    return elems.map((elem) => {
      return elem.innerText
    })
  })
  t.deepEqual(groupNames, [])

  t.log(`Verifying that group is deleted server side`)
  const pgClient = await pgPool.connect()
  let rslt
  try {
    rslt = await pgClient.query('SELECT * FROM groups where id=$1', [group.id,])
  } finally {
    pgClient.release()
  }
  t.deepEqual(rslt.rows, [])
})

test.serial('A user can join a group', async (t) => {
  const {user, member, admin, group,} = await setupDatabase(t)
  await logUserIn(t, user)

  t.log(`Navigating to group profile page`)
  const {page, pgPool,} = t.context
  await page.goto(`${t.context.rootUrl}/g/${group.path}`)

  const joinButtonElem = await page.waitForSelector('#join-group-button', {visible: true,})
  const isEnabled = await joinButtonElem.evaluate((elem) => {
    return !elem.disabled
  })
  t.true(isEnabled)

  t.log(`Joining group`)
  await joinButtonElem.click()

  t.log(`Verifying that group has been joined client side`)
  const leaveButtonElem = await page.waitForSelector('#leave-group-button', {visible: true,})
  t.is(await leaveButtonElem.evaluate((elem) => {return elem.innerText}), 'Leave')
  t.false(await leaveButtonElem.evaluate((elem) => {return elem.disabled}))
  const numMembersText = await page.$eval('#group-num-members', (elem) => {
    return elem.innerText
  })
  t.is(numMembersText, 'Has 3 members')

  t.log(`Verifying that group has been joined server side`)
  const pgClient = await pgPool.connect()
  let rslt
  try {
    rslt = await pgClient.query('SELECT user_id, membership FROM users_groups where group_id=$1',
      [group.id,])
  } finally {
    pgClient.release()
  }
  t.deepEqual(sortBy(prop('id'), rslt.rows), [
    {
      user_id: admin.id,
      membership: 'admin',
    },
    {
      user_id: member.id,
      membership: 'member',
    },
    {
      user_id: user.id,
      membership: 'member',
    },
  ])
})

test.serial('A user can leave a group', async (t) => {
  const {member, admin, group,} = await setupDatabase(t)
  await logUserIn(t, member)

  t.log(`Navigating to group profile page`)
  const {page, pgPool,} = t.context
  await page.goto(`${t.context.rootUrl}/g/${group.path}`)

  const leaveButtonElem = await page.waitForSelector('#leave-group-button', {visible: true,})
  const isEnabled = await leaveButtonElem.evaluate((elem) => {
    return !elem.disabled
  })
  t.true(isEnabled)

  t.log(`Leaving group`)
  await leaveButtonElem.click()

  t.log(`Verifying that group has been left client side`)
  const joinButtonElem = await page.waitForSelector('#join-group-button', {visible: true,})
  t.is(await joinButtonElem.evaluate((elem) => {return elem.innerText}), 'Join')
  t.false(await joinButtonElem.evaluate((elem) => {return elem.disabled}))
  const numMembersText = await page.$eval('#group-num-members', (elem) => {
    return elem.innerText
  })
  t.is(numMembersText, 'Has 1 member')

  t.log(`Verifying that group has been left server side`)
  const pgClient = await pgPool.connect()
  let rslt
  try {
    rslt = await pgClient.query('SELECT user_id, membership FROM users_groups where group_id=$1',
      [group.id,])
  } finally {
    pgClient.release()
  }
  t.deepEqual(rslt.rows, [
    {
      user_id: admin.id,
      membership: 'admin',
    },
  ])
})

test.serial('An unauthenticated user can view a group', async (t) => {
  const {group,} = await setupDatabase(t)

  t.log(`Navigating to group profile page`)
  const {page,} = t.context
  await page.goto(`${t.context.rootUrl}/g/${group.path}`)
  await page.waitForSelector('#group-info-tab-content', {visible: true,})

  t.is(await page.$eval('#group-toolbar', (elem) => {
    return elem.children.length
  }), 0)
  t.true(await page.$('#join-group-button') == null)
})

const setupDatabase = async (t) => {
  const {pgPool,} = t.context

  const added = DateTime.utc().toISO()
  const admin = {
    username: 'admin',
    name: 'An Admin',
    emailAddress: 'admin@example.com',
    added,
    bio: 'About an admin.',
    password: 'p@ssword1234',
  }
  const user = {
    username: 'test',
    name: 'Some User',
    emailAddress: 'test@example.com',
    added,
    bio: 'About a user.',
    password: 'p@ssword1234',
  }
  const passwordHash = await bcrypt.hash(user.password, 10)
  const member = {
    username: 'member',
    name: 'Group Member',
    emailAddress: 'member@example.com',
    added,
    bio: 'About a member.',
    password: 'p@ssword1234',
  }
  const group = {
    path: 'test',
    name: 'Test',
    added,
    description: 'A test group',
    members: {
      [admin.username]: 'admin',
      [member.username]: 'member',
    },
  }

  t.log('Inserting test data into database', {
    admin,
    user,
    group,
  })
  const client = await pgPool.connect()
  let adminId
  let userId
  let memberId
  let groupId
  try {
    let rslt = await client.query(`INSERT INTO users (username, name, email_address, password_hash, added,
      bio) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id`, [admin.username, admin.name,
        admin.emailAddress, passwordHash, admin.added, admin.bio,])
    adminId = rslt.rows[0].id
    rslt = await client.query(`INSERT INTO users (username, name, email_address, password_hash, added,
      bio) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id`, [user.username, user.name, user.emailAddress,
        passwordHash, user.added, user.bio,])
    userId = rslt.rows[0].id
    rslt = await client.query(`INSERT INTO users (username, name, email_address, password_hash, added,
      bio) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id`, [member.username, member.name,
        member.emailAddress, passwordHash, member.added, member.bio,])
    memberId = rslt.rows[0].id
    rslt = await client.query(`INSERT INTO groups (path, name, added, description)
      VALUES ($1, $2, $3, $4) RETURNING id`, [group.path, group.name, added, group.description,])
    groupId = rslt.rows[0].id
    await client.query(`INSERT INTO users_groups (group_id, user_id, added, membership)
      VALUES ($1, $2, $3, 'admin')`, [groupId, adminId, added,])
    await client.query(`INSERT INTO users_groups (group_id, user_id, added, membership)
      VALUES ($1, $2, $3, 'member')`, [groupId, memberId, added,])
  } finally {
    client.release()
  }

  admin.id = adminId
  user.id = userId
  member.id = memberId
  group.id = groupId

  return {admin, user, member, group,}
}

const logUserIn = async(t, user) => {
  const {page,} = t.context

  t.log(`Navigating to ${t.context.rootUrl}/login`)
  await page.goto(`${t.context.rootUrl}/login`)
  t.log(`Waiting for login form to become visible`)
  await page.waitForSelector('#login-form', {visible: true,})
  const loginButton = await page.$('#submit-login-button')

  await page.type('#login-input-usernameoremail', user.username)
  await page.type('#login-input-password', user.password)
  await page.waitForFunction(() => {
    const btn = document.querySelector(`#submit-login-button`)
    return !btn.disabled
  })
  t.log('Logging user in')
  await Promise.all([
    page.waitForNavigation(),
    loginButton.click(),
  ])

  let url = new URL(page.url())
  t.is(url.pathname, '/')
}
