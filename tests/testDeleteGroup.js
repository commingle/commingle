const test = require('ava')
const Promise = require('bluebird')
const bcrypt = require('bcrypt')
const {DateTime,} = require('luxon')

require('./common')

test.serial(`A group admin can delete the group`, async (t) => {
  const {user, group,} = await setupDatabase(t)
  await logUserIn(t, user)

  const {page, pgPool,} = t.context

  t.log(`Navigating to group profile page`)
  await page.goto(`${t.context.rootUrl}/g/${group.path}`)
  await page.waitForSelector('#group-profile-view', {visible: true,})

  t.log(`Clicking delete control`)
  const deleteGroupBtn = await page.waitForSelector('#delete-group-button', {visible: true,})
  await deleteGroupBtn.click()

  const modalElem = await page.waitForSelector('.modal', {visible: true,})
  const modalHeading = await modalElem.$eval('.modal-header-content .modal-label', (elem) => {
    return elem.innerText
  })
  t.is(modalHeading, 'Delete Group?')
  const modalBody = await modalElem.$eval('.modal-body-content', (elem) => {
    return elem.innerText
  })
  t.is(modalBody, 'Are you really sure you wish to delete this group?')

  const modalYesElem = await modalElem.$('.button-group #modal-yes')
  const modalNoElem = await modalElem.$('.button-group #modal-no')
  t.is(await modalYesElem.evaluate((elem) => {return elem.innerText}), 'Yes')
  t.is(await modalNoElem.evaluate((elem) => {return elem.innerText}), 'No')

  t.log('Clicking Yes button')
  await Promise.all([
    page.waitForNavigation(),
    modalYesElem.click(),
  ])

  t.log('Verifying that homepage is navigated to')
  const url = new URL(page.url())
  t.is(url.pathname, `/`)

  t.log('Verifying database records')
  const client = await pgPool.connect()
  try {
    let rslt = await client.query(`SELECT id FROM groups`)
    t.is(rslt.rows.length, 0)
    rslt = await client.query(`SELECT id FROM users_groups`)
    t.is(rslt.rows.length, 0)
    rslt = await client.query(`SELECT id FROM meets`)
    t.is(rslt.rows.length, 0)
  } finally {
    client.release()
  }
})

test.serial(`Deleting a group can be canceled`, async (t) => {
  const {user, group,} = await setupDatabase(t)
  await logUserIn(t, user)

  const {page,} = t.context

  t.log(`Navigating to group profile page`)
  await page.goto(`${t.context.rootUrl}/g/${group.path}`)
  await page.waitForSelector('#group-profile-view', {visible: true,})

  t.log(`Clicking delete control`)
  const deleteGroupElem = await page.waitForSelector('#delete-group-button', {visible: true,})
  await deleteGroupElem.click()

  t.log(`Waiting for modal`)
  const modalElem = await page.waitForSelector('.modal', {visible: true,})
  const modalNoElem = await modalElem.$('.button-group #modal-no')

  t.log('Clicking No button')
  await modalNoElem.click()

  t.log('Verifying that modal is closed and the page is the same')
  await page.waitForSelector('.modal', {hidden: true,})
  const url = new URL(page.url())
  t.is(url.pathname, `/g/${group.path}`)
})

test.serial(`An unauthenticated user cannot delete a group`, async (t) => {
  const {group,} = await setupDatabase(t)

  const {page,} = t.context

  t.log(`Navigating to group profile page`)
  await page.goto(`${t.context.rootUrl}/g/${group.path}`)
  await page.waitForSelector('#group-profile-view', {visible: true,})

  t.true(await page.$('#delete-group-button') == null)
})

const setupDatabase = async (t) => {
  const {pgPool,} = t.context

  const added = DateTime.utc()
  const user = {
    username: 'test',
    name: 'Some User',
    emailAddress: 'test@example.com',
    added,
    bio: 'About a user.',
    password: 'p@ssword1234',
  }
  const passwordHash = await bcrypt.hash(user.password, 10)
  const group = {
    path: 'test',
    name: 'Test',
    added,
    description: 'A test group',
    members: {
      [user.username]: 'admin',
    },
  }

  t.log('Inserting test data into database', {
    user,
    group,
  })
  const client = await pgPool.connect()
  let userId
  let groupId
  try {
    let rslt = await client.query(`INSERT INTO users (username, name, email_address, password_hash, added,
      bio) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id`, [user.username, user.name, user.emailAddress,
        passwordHash, added.toISO(), user.bio,])
    userId = rslt.rows[0].id
    rslt = await client.query(`INSERT INTO groups (path, name, added, description)
      VALUES ($1, $2, $3, $4) RETURNING id`, [group.path, group.name, added.toISO(),
        group.description,])
    groupId = rslt.rows[0].id
    await client.query(`INSERT INTO users_groups (group_id, user_id, added, membership)
      VALUES ($1, $2, $3, 'admin')`, [groupId, userId, added.toISO(),])

    // Insert a meet, to verify it gets deleted with the group
    const uuid = '53113186-8840-4fbb-99aa-eb55963b4462'
    const starts = DateTime.fromISO('2020-05-10T12:00:00Z')
    const ends = DateTime.fromISO('2020-05-10T14:00:00Z')
    const locationPlaceId = 'ChIJbygR2x5OqEcRbhbkZsMB_DA'
    const locationName = 'Alexanderplatz'
    const locationAddress = '10178 Berlin, Germany'
    await client.query(`INSERT INTO meets (uuid, group_id, title, starts, ends, description,
      added, location_place_id, location_name, location_address)
      VALUES ($1, $2, 'Test', $3, $4, 'Test', $5, $6, $7, $8)`,
      [
        uuid, groupId, starts.toISO(), ends.toISO(), added.toISO(), locationPlaceId, locationName,
        locationAddress,
      ])
  } finally {
    client.release()
  }

  user.id = userId
  group.id = groupId

  return {user, group,}
}

const logUserIn = async(t, user) => {
  const {page,} = t.context

  t.log(`Navigating to ${t.context.rootUrl}/login`)
  await page.goto(`${t.context.rootUrl}/login`)
  t.log(`Waiting for login form to become visible`)
  await page.waitForSelector('#login-form', {visible: true,})
  const loginButton = await page.$('#submit-login-button')

  await page.type('#login-input-usernameoremail', user.username)
  await page.type('#login-input-password', user.password)
  await page.waitForFunction(() => {
    const btn = document.querySelector(`#submit-login-button`)
    return !btn.disabled
  })
  t.log('Logging user in')
  await Promise.all([
    page.waitForNavigation(),
    loginButton.click(),
  ])

  let url = new URL(page.url())
  t.is(url.pathname, '/')
}
