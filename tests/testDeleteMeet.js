const test = require('ava')
const Promise = require('bluebird')
const bcrypt = require('bcrypt')
const {DateTime,} = require('luxon')

require('./common')

test.serial(`A group admin can delete an owned meet`, async (t) => {
  const {admin, attendee, meet,} = await setupDatabase(t)
  await logUserIn(t, admin)

  const {page, pgPool,} = t.context

  t.log(`Navigating to meet page`)
  await page.goto(`${t.context.rootUrl}/m/${meet.uuid}`)
  await page.waitForSelector('#meet-view', {visible: true,})

  t.log(`Clicking delete control`)
  const deleteMeetElem = await page.waitForSelector('#meet-delete-control', {visible: true,})
  await deleteMeetElem.click()

  const modalElem = await page.waitForSelector('.modal', {visible: true,})
  const modalHeading = await modalElem.$eval('.modal-header-content .modal-label', (elem) => {
    return elem.innerText
  })
  t.is(modalHeading, 'Delete Meet?')
  const modalBody = await modalElem.$eval('.modal-body-content', (elem) => {
    return elem.innerText
  })
  t.is(modalBody, 'Are you really sure you wish to delete this meet? This cannot be undone!')

  const modalYesElem = await modalElem.$('.button-group #modal-yes')
  const modalNoElem = await modalElem.$('.button-group #modal-no')
  t.is(await modalYesElem.evaluate((elem) => {return elem.innerText}), 'Yes')
  t.is(await modalNoElem.evaluate((elem) => {return elem.innerText}), 'No')

  t.log('Clicking Yes button')
  await Promise.all([
    page.waitForNavigation(),
    modalYesElem.click(),
  ])

  t.log('Verifying that homepage is navigated to')
  const url = new URL(page.url())
  t.is(url.pathname, `/`)

  t.log('Verifying database records')
  const client = await pgPool.connect()
  try {
    const rslt = await client.query(`SELECT id FROM meets`)
    t.is(rslt.rows.length, 0)
  } finally {
    client.release()
  }

  t.log('Waiting for fake Mandrill API server to receive request to send emails...')
  for (let i = 0; i < 4 && t.context.mandrillRequests.length < 2; i++) {
    await Promise.delay(100)
  }

  // Emails should have been sent to the organizer and the attendee
  t.is(t.context.mandrillRequests.length, 2)
  const [payload1, payload2,] = t.context.mandrillRequests
  t.deepEqual(payload1, {
    key: 'test',
    message: {
      subject: `Meet deleted: ${meet.title}`,
      html: `<p>The meet <em>${meet.title}</em> has been deleted.</p>`,
      // TODO: Change to commingle.xyz once the domain is verified with Mandrill
      from_email: 'contact@experimental.berlin',
      from_name: 'Commingle',
      to: [
        {
          email: admin.emailAddress,
          name: admin.name,
          type: 'to',
        },
      ],
      headers: {
        'Reply-To': '<no-reply>@commingle.xyz',
      },
    },
  })
  t.deepEqual(payload2, {
    key: 'test',
    message: {
      subject: `Meet deleted: ${meet.title}`,
      html: `<p>The meet <em>${meet.title}</em> has been deleted.</p>`,
      // TODO: Change to commingle.xyz once the domain is verified with Mandrill
      from_email: 'contact@experimental.berlin',
      from_name: 'Commingle',
      to: [
        {
          email: attendee.emailAddress,
          name: attendee.name,
          type: 'to',
        },
      ],
      headers: {
        'Reply-To': '<no-reply>@commingle.xyz',
      },
    },
  })
})

test.serial(`Deleting a meet can be canceled`, async (t) => {
  const {admin, meet,} = await setupDatabase(t)
  await logUserIn(t, admin)

  const {page,} = t.context

  t.log(`Navigating to meet page`)
  await page.goto(`${t.context.rootUrl}/m/${meet.uuid}`)
  await page.waitForSelector('#meet-view', {visible: true,})

  t.log(`Clicking delete control`)
  const deleteMeetElem = await page.waitForSelector('#meet-delete-control', {visible: true,})
  await deleteMeetElem.click()

  const modalElem = await page.waitForSelector('.modal', {visible: true,})
  const modalNoElem = await modalElem.$('.button-group #modal-no')

  t.log('Clicking No button')
  await modalNoElem.click()

  t.log('Verifying that modal is closed and the page is the same')
  await page.waitForSelector('.modal', {hidden: true,})
  const url = new URL(page.url())
  t.is(url.pathname, `/m/${meet.uuid}`)
})

test.serial(`An unauthenticated user cannot delete a meet`, async (t) => {
  const {meet,} = await setupDatabase(t)

  const {page,} = t.context

  t.log(`Navigating to meet page`)
  await page.goto(`${t.context.rootUrl}/m/${meet.uuid}`)
  await page.waitForSelector('#meet-view', {visible: true,})

  t.true(await page.$('#delete-meet-control') == null)
})

const setupDatabase = async (t) => {
  const {pgPool,} = t.context

  const added = DateTime.utc()
  const admin = {
    username: 'test',
    name: 'Some Admin',
    emailAddress: 'test@example.com',
    added,
    bio: 'About an admin.',
    password: 'p@ssword1234',
  }
  const attendee = {
    username: 'attendee',
    name: 'An Attendee',
    emailAddress: 'attendee@example.com',
    added,
    bio: 'About an attendee.',
    password: 'p@ssword1234',
  }
  const passwordHash = await bcrypt.hash(admin.password, 10)
  const group = {
    path: 'test',
    name: 'Test',
    added,
    description: 'A test group',
    members: {
      [admin.username]: 'admin',
    },
  }
  const meet = {
    uuid: '27fd6d6d-d518-446e-b2ca-b7d3dc486d7e',
    title: 'Test',
    starts: DateTime.fromISO('2020-05-10T12:00:00Z'),
    ends: DateTime.fromISO('2020-05-10T14:00:00Z'),
    description: 'Test meet',
    locationPlaceId: 'ChIJbygR2x5OqEcRbhbkZsMB_DA',
    locationName: 'Alexanderplatz',
    locationAddress: '10178 Berlin, Germany',
  }

  t.log('Inserting test data into database', {
    admin,
    group,
    meet,
  })
  const client = await pgPool.connect()
  let adminId
  let attendeeId
  let groupId
  let meetId
  try {
    let rslt = await client.query(`INSERT INTO users (username, name, email_address, password_hash, added,
      bio) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id`, [admin.username, admin.name,
        admin.emailAddress, passwordHash, added.toISO(), admin.bio,])
    adminId = rslt.rows[0].id
    rslt = await client.query(`INSERT INTO users (username, name, email_address, password_hash, added,
      bio) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id`, [attendee.username, attendee.name,
        attendee.emailAddress, passwordHash, added.toISO(), attendee.bio,])
    attendeeId = rslt.rows[0].id
    rslt = await client.query(`INSERT INTO groups (path, name, added, description)
      VALUES ($1, $2, $3, $4) RETURNING id`, [group.path, group.name, added.toISO(),
        group.description,])
    groupId = rslt.rows[0].id
    await client.query(`INSERT INTO users_groups (group_id, user_id, added, membership)
      VALUES ($1, $2, $3, 'admin')`, [groupId, adminId, added.toISO(),])
    rslt = await client.query(
      `INSERT INTO meets (uuid, group_id, title, starts, ends, description, location_place_id,
      location_name, location_address, added) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10) RETURNING id`,
      [
        meet.uuid, groupId, meet.title, meet.starts, meet.ends, meet.description,
        meet.locationPlaceId, meet.locationName, meet.locationAddress, added.toISO(),
      ])
    meetId = rslt.rows[0].id
    await client.query(
      `INSERT INTO meets_hosts (meet_id, user_id, added)
      VALUES ($1, $2, $3)`, [meetId, adminId, added.toISO(),])
    await client.query(
      `INSERT INTO meets_attendees (meet_id, user_id, added)
      VALUES ($1, $2, $3)`, [meetId, attendeeId, added.toISO(),])
  } finally {
    client.release()
  }

  admin.id = adminId
  attendee.id = attendeeId
  group.id = groupId
  meet.groupId = groupId

  return {admin, attendee, group, meet,}
}

const logUserIn = async(t, user) => {
  const {page,} = t.context

  t.log(`Navigating to ${t.context.rootUrl}/login`)
  await page.goto(`${t.context.rootUrl}/login`)
  t.log(`Waiting for login form to become visible`)
  await page.waitForSelector('#login-form', {visible: true,})
  const loginButton = await page.$('#submit-login-button')

  await page.type('#login-input-usernameoremail', user.username)
  await page.type('#login-input-password', user.password)
  await page.waitForFunction(() => {
    const btn = document.querySelector(`#submit-login-button`)
    return !btn.disabled
  })
  t.log('Logging user in')
  await Promise.all([
    page.waitForNavigation(),
    loginButton.click(),
  ])

  let url = new URL(page.url())
  t.is(url.pathname, '/')
}
