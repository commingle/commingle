const test = require('ava')

require('./common')

test.serial('An error message is displayed if a non-existent user tries to log in', async (t) => {
  const {page,} = t.context

  t.log(`Navigating to ${t.context.rootUrl}/login`)
  await page.goto(`${t.context.rootUrl}/login`)
  t.log(`Waiting for login form to become visible`)
  await page.waitForSelector('#login-form', {visible: true,})
  const loginButton = await page.$('#submit-login-button')
  const isLoginDisabled = await loginButton.evaluate((elem) => {
    return elem.disabled
  })
  t.is(isLoginDisabled, true)

  await page.type('#login-input-usernameoremail', 'non-existent')
  await page.type('#login-input-password', 'password1234')
  await page.waitForFunction(() => {
    const btn = document.querySelector(`#submit-login-button`)
    return !btn.disabled
  })
  await loginButton.click()

  const errorElem = await page.waitForSelector(`#login-form-error`, {visible: true,})
  const errorText = await errorElem.evaluate((elem) => {return elem.innerText})
  t.is(errorText, `Unrecognized username or password`)
})
