const test = require('ava')
const Promise = require('bluebird')
const bcrypt = require('bcrypt')
const {DateTime,} = require('luxon')

require('./common')

test.serial(`An authenticated user can register their attendance to a meet`, async (t) => {
  const {user, admin, meet,} = await setupDatabase(t)
  await logUserIn(t, user)

  const {page, pgPool,} = t.context

  t.log(`Navigating to meet page`)
  await page.goto(`${t.context.rootUrl}/m/${meet.uuid}`)
  await page.waitForSelector('#meet-view', {visible: true,})

  const attendBtnId = '#attend-button'
  const attendElem = await page.waitForSelector(attendBtnId, {visible: true,})
  t.is(await attendElem.evaluate((elem) => {return elem.innerText}), 'Attend')

  t.log('Clicking attend button')
  await attendElem.click()
  t.log('Waiting for attend button to change state')
  await page.waitForFunction((btnId) => {
    const btn = document.querySelector(btnId)
    return btn != null && btn.innerText === 'Attending'
  }, {}, attendBtnId)

  t.log('Verifying database records')
  let row
  const client = await pgPool.connect()
  try {
    const rslt = await client.query(`SELECT meet_id, user_id FROM meets_attendees`)
    t.is(rslt.rows.length, 1)
    row = rslt.rows[0]
  } finally {
    client.release()
  }
  t.deepEqual(row, {
    meet_id: meet.id,
    user_id: user.id,
  })

  t.log('Waiting for fake Mandrill API server to receive request to send emails...')
  for (let i = 0; i < 4 && t.context.mandrillRequests.length < 2; i++) {
    await Promise.delay(100)
  }

  // Emails should have been sent to the attendee and the admin
  t.is(t.context.mandrillRequests.length, 2)
  const [payload1, payload2,] = t.context.mandrillRequests
  t.deepEqual(payload1, {
    key: 'test',
    message: {
      subject: 'Signed up for meet: Test',
      html: `<p>You've signed up for the meet <em>Test</em>. See you there!</p>`,
      // TODO: Change to commingle.xyz once the domain is verified with Mandrill
      from_email: 'contact@experimental.berlin',
      from_name: 'Commingle',
      to: [
        {
          email: user.emailAddress,
          name: user.name,
          type: 'to',
        },
      ],
      headers: {
        'Reply-To': '<no-reply>@commingle.xyz',
      },
    },
  })
  t.deepEqual(payload2, {
    key: 'test',
    message: {
      subject: 'A user has signed up for your meet: Test',
      html: `<p>The user <em>Some User</em> has signed up for your meet <em>Test</em>.</p>`,
      // TODO: Change to commingle.xyz once the domain is verified with Mandrill
      from_email: 'contact@experimental.berlin',
      from_name: 'Commingle',
      to: [
        {
          email: admin.emailAddress,
          name: admin.name,
          type: 'to',
        },
      ],
      headers: {
        'Reply-To': '<no-reply>@commingle.xyz',
      },
    },
  })
})

test.serial(`An unauthenticated user cannot register their attendance to a meet`, async (t) => {
  const {meet,} = await setupDatabase(t)

  const {page,} = t.context

  t.log(`Navigating to meet page`)
  await page.goto(`${t.context.rootUrl}/m/${meet.uuid}`)
  await page.waitForSelector('#meet-view', {visible: true,})

  t.true(await page.$('#attend-button') == null)
})

test.serial.only(`A meet host cannot register their attendance to their own meet`, async (t) => {
  const {admin, meet,} = await setupDatabase(t)
  await logUserIn(t, admin)

  const {page,} = t.context

  t.log(`Navigating to meet page`)
  await page.goto(`${t.context.rootUrl}/m/${meet.uuid}`)
  await page.waitForSelector('#meet-view', {visible: true,})

  // There should be no attend button
  t.true(await page.$('#attend-button') == null)

  const organizersHtml = await page.$eval('#meet-organizers', (elem) => {return elem.innerHTML})
  t.is(organizersHtml, `Organized by <a href="/u/${admin.username}">you</a>`)
})

test.serial(`A user can rescind their attendance to a meet`, async (t) => {
  const {user, admin, meet,} = await setupDatabase(t)
  await logUserIn(t, user)

  const {page, pgPool,} = t.context

  t.log(`Navigating to meet page`)
  await page.goto(`${t.context.rootUrl}/m/${meet.uuid}`)
  await page.waitForSelector('#meet-view', {visible: true,})

  const attendBtnId = '#attend-button'
  let attendElem = await page.waitForSelector(attendBtnId, {visible: true,})
  t.is(await attendElem.evaluate((elem) => {return elem.innerText}), 'Attend')

  t.log('Clicking attend button')
  await attendElem.click()
  t.log('Waiting for attend button to change state')
  await page.waitForFunction((btnId) => {
    const btn = document.querySelector(btnId)
    return btn != null && btn.innerText === 'Attending'
  }, {}, attendBtnId)

  // Have to get this element again, since it's been re-rendered
  attendElem = await page.waitForSelector(attendBtnId, {visible: true,})
  t.log('Clicking attend button again, to de-attend')
  await attendElem.click()
  t.log('Waiting for attend button to change state')
  await page.waitForFunction((btnId) => {
    const btn = document.querySelector(btnId)
    return btn != null && btn.innerText === 'Attend'
  }, {}, attendBtnId)

  t.log('Verifying database records')
  const client = await pgPool.connect()
  try {
    const rslt = await client.query(`SELECT meet_id, user_id FROM meets_attendees`)
    t.is(rslt.rows.length, 0)
  } finally {
    client.release()
  }

  t.log('Waiting for fake Mandrill API server to receive request to send emails...')
  for (let i = 0; i < 4 && t.context.mandrillRequests.length < 4; i++) {
    await Promise.delay(300)
  }

  // There should have been sent 2 emails to the attendee, and 2 to the admin
  t.is(t.context.mandrillRequests.length, 4)
  // Get the second payload, notifying of the rescinded attendance
  const [,,payload1, payload2,] = t.context.mandrillRequests
  t.deepEqual(payload1, {
    key: 'test',
    message: {
      subject: 'Signup rescinded for meet: Test',
      html: `<p>You've rescinded your signup for the meet <em>Test</em>.</p>`,
      // TODO: Change to commingle.xyz once the domain is verified with Mandrill
      from_email: 'contact@experimental.berlin',
      from_name: 'Commingle',
      to: [
        {
          email: user.emailAddress,
          name: user.name,
          type: 'to',
        },
      ],
      headers: {
        'Reply-To': '<no-reply>@commingle.xyz',
      },
    },
  })
  t.deepEqual(payload2, {
    key: 'test',
    message: {
      subject: 'A user has rescinded their signup for your meet: Test',
      html: `<p>The user <em>Some User</em> has rescinded their signup for your meet <em>Test</em>.</p>`,
      // TODO: Change to commingle.xyz once the domain is verified with Mandrill
      from_email: 'contact@experimental.berlin',
      from_name: 'Commingle',
      to: [
        {
          email: admin.emailAddress,
          name: admin.name,
          type: 'to',
        },
      ],
      headers: {
        'Reply-To': '<no-reply>@commingle.xyz',
      },
    },
  })
})

const setupDatabase = async (t) => {
  const {pgPool,} = t.context

  const added = DateTime.utc()
  const user = {
    username: 'test',
    name: 'Some User',
    emailAddress: 'test@example.com',
    added,
    bio: 'About a user.',
    password: 'p@ssword1234',
  }
  const passwordHash = await bcrypt.hash(user.password, 10)
  const admin = {
    username: 'admin',
    name: 'Group Admin',
    emailAddress: 'admin@example.com',
    added,
    bio: 'About a group admin.',
    password: 'p@ssword1234',
  }
  const group = {
    path: 'test',
    name: 'Test',
    added,
    description: 'A test group',
    members: {
      [admin.username]: 'admin',
    },
  }
  const meet = {
    uuid: '27fd6d6d-d518-446e-b2ca-b7d3dc486d7e',
    title: 'Test',
    starts: DateTime.fromISO('2020-05-10T12:00:00Z'),
    ends: DateTime.fromISO('2020-05-10T14:00:00Z'),
    description: 'Test meet',
    locationPlaceId: 'ChIJbygR2x5OqEcRbhbkZsMB_DA',
    locationName: 'Alexanderplatz',
    locationAddress: '10178 Berlin, Germany',
  }

  t.log('Inserting test data into database', {
    user,
    admin,
    group,
    meet,
  })
  const client = await pgPool.connect()

  // We add two users, one admin and one regular
  let userId
  let groupId
  let meetId
  try {
    // Insert the regular user
    let rslt = await client.query(`INSERT INTO users (username, name, email_address, password_hash,
      added, bio) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id`, [
        user.username, user.name, user.emailAddress, passwordHash, added.toISO(), user.bio,
      ])
    userId = rslt.rows[0].id
    // Insert the admin
    rslt = await client.query(`INSERT INTO users (username, name, email_address, password_hash,
      added, bio) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id`, [
        admin.username, admin.name, admin.emailAddress, passwordHash, added.toISO(), admin.bio,
      ])
    const adminId = rslt.rows[0].id
    // Insert the group
    rslt = await client.query(`INSERT INTO groups (path, name, added, description)
      VALUES ($1, $2, $3, $4) RETURNING id`, [group.path, group.name, added.toISO(),
        group.description,])
    groupId = rslt.rows[0].id
    await client.query(`INSERT INTO users_groups (group_id, user_id, added, membership)
      VALUES ($1, $2, $3, 'admin')`, [groupId, adminId, added.toISO(),])
    // Insert the meet
    rslt = await client.query(
      `INSERT INTO meets (uuid, group_id, title, starts, ends, description, location_place_id,
      location_name, location_address, added) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)
      RETURNING id`,
      [
        meet.uuid, groupId, meet.title, meet.starts, meet.ends, meet.description,
        meet.locationPlaceId, meet.locationName, meet.locationAddress, added.toISO(),
      ])
    meetId = rslt.rows[0].id
    // Insert the meet/host relation
    await client.query(
      `INSERT INTO meets_hosts (meet_id, user_id, added)
      VALUES ($1, $2, $3)`, [meetId, adminId, added.toISO(),])
  } finally {
    client.release()
  }

  user.id = userId
  group.id = groupId
  meet.groupId = groupId
  meet.id = meetId

  return {user, admin, group, meet,}
}

const logUserIn = async(t, user) => {
  const {page,} = t.context

  t.log(`Navigating to ${t.context.rootUrl}/login`)
  await page.goto(`${t.context.rootUrl}/login`)
  t.log(`Waiting for login form to become visible`)
  await page.waitForSelector('#login-form', {visible: true,})
  const loginButton = await page.$('#submit-login-button')

  await page.type('#login-input-usernameoremail', user.username)
  await page.type('#login-input-password', user.password)
  await page.waitForFunction(() => {
    const btn = document.querySelector(`#submit-login-button`)
    return !btn.disabled
  })
  t.log('Logging user in')
  await Promise.all([
    page.waitForNavigation(),
    loginButton.click(),
  ])

  let url = new URL(page.url())
  t.is(url.pathname, '/')
}
