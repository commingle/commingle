const test = require('ava')
const Promise = require('bluebird')
const bcrypt = require('bcrypt')
const {DateTime,} = require('luxon')

require('./common')

test.serial.only(`A group admin can create a new meet`, async (t) => {
  const [user, group,] = await setupDatabase(t)
  await logUserIn(t, user)

  const {page,} = t.context
  const locationName = 'Test'
  const locationAddress = `${locationName}, Test Street, Test City, Test Country`
  const now = DateTime.local()

  t.log(`Navigating to group profile page`)
  await page.goto(`${t.context.rootUrl}/g/${group.path}`)

  const newMeetBtn = await page.waitForSelector('#new-meet-button', {visible: true,})
  await Promise.all([
    page.waitForNavigation(),
    newMeetBtn.click(),
  ])
  let url = new URL(page.url())
  t.is(url.pathname, `/g/${group.path}/new-meet`)

  await page.waitForSelector('#new-meet-view', {visible: true,})
  t.is(await page.$eval('#new-meet-view h1', (elem) => {return elem.innerText}), 'Add a Meet')
  const titleInputElem = await page.waitForSelector('#input-title', {visible: true,})
  const locationInputElem = await page.waitForSelector('#input-location', {visible: true,})
  const startDateInputElem = await page.waitForSelector('#add-meet-input-start-date',
    {visible: true,})
  const startTimeInputElem = await page.waitForSelector('#add-meet-input-start-time',
    {visible: true,})
  const endDateInputElem = await page.waitForSelector('#add-meet-input-end-date', {visible: true,})
  const endTimeInputElem = await page.waitForSelector('#add-meet-input-end-time', {visible: true,})
  const descriptionEditorElem = await page.waitForSelector('#description-editor textarea',
    {visible: true,})
  const hostInputElem = await page.waitForSelector('#add-meet-host-input', {visible: true,})
  const addMeetBtnElem = await page.waitForSelector('#submit-add-meet-button', {visible: true,})
  const cancelAddMeetBtnElem = await page.waitForSelector('#cancel-add-meet-button',
    {visible: true,})
  t.is(await addMeetBtnElem.evaluate((elem) => {return !elem.disabled}), false)
  t.is(await cancelAddMeetBtnElem.evaluate((elem) => {return !elem.disabled}), true)

  t.log('Filling in form')
  await titleInputElem.type('Test')
  await locationInputElem.type(locationAddress)
  const locDropdownItem = await page.waitForSelector('#location-dropdown li', {visible: true,})
  await locDropdownItem.click()
  const locationNameElem = await page.waitForSelector('#meet-location-name', {visible: true,})
  t.is(await locationNameElem.evaluate((elem) => {return elem.innerText}), locationName)
  const localizedStart = await page.evaluate((year) => {
    const date = new Date(`${year + 1}-01-01T12:00:00`)
    return {
      date: date.toLocaleDateString(navigator.language, {
        day: '2-digit',
        month: '2-digit',
        year: 'numeric',
      }),
      time: date.toLocaleTimeString(navigator.language, {hour: '2-digit', minute: '2-digit',}),
    }
  }, now.year)
  t.log(`Matching localized start time`, localizedStart)
  const localizedStartDate = localizedStart.date.replace(/\//g, '').replace(/-/g, '')
  const localizedStartTime = localizedStart.time.replace(/:/, '').replace(/ /, '')
  const localizedEnd = await page.evaluate((year) => {
    const date = new Date(`${year + 1}-01-01T14:00:00`)
    return {
      date: date.toLocaleDateString(navigator.language, {
        day: '2-digit',
        month: '2-digit',
        year: 'numeric',
      }),
      time: date.toLocaleTimeString(navigator.language, {hour: '2-digit', minute: '2-digit',}),
    }
  }, now.year)
  t.log(`Matching localized end time`, localizedEnd)
  const localizedEndDate = localizedEnd.date.replace(/\//g, '').replace(/-/g, '')
  const localizedEndTime = localizedEnd.time.replace(/:/, '').replace(/ /, '')
  await startDateInputElem.type(localizedStartDate)
  await startTimeInputElem.type(localizedStartTime)
  await endDateInputElem.type(localizedEndDate)
  await endTimeInputElem.type(localizedEndTime)
  await descriptionEditorElem.type(`# Test Meet`)
  await hostInputElem.type(user.name)
  const hostDropdownItem = await page.waitForSelector('#host-dropdown li', {visible: true,})
  await hostDropdownItem.click()
  const hostElem = await page.waitForSelector('#hosts .host')
  t.is(await hostElem.evaluate((elem) => {return elem.innerText}), user.name)
  await page.waitForFunction(() => {
    return !document.querySelector('#submit-add-meet-button').disabled
  })

  t.log('Submitting form')
  await Promise.all([
    page.waitForNavigation(),
    addMeetBtnElem.click(),
  ])

  // TODO: Verify meet in database

  url = new URL(page.url())
  t.true(/\/m\/.+/.test(url.pathname))

  const titleElem = await page.waitForSelector('#meet-title', {visible: true,})
  t.is(await titleElem.evaluate((elem) => {return elem.innerText}), 'Test')
})

test.serial(`An authenticated user can visit their own profile`, async (t) => {
  const [user,] = await setupDatabase(t)
  await logUserIn(t, user)
  const {page,} = t.context

  t.log(`Navigating to user profile page`)
  const accountIconElem = await page.waitForSelector('#account-icon', {visible: true,})
  await accountIconElem.click()
  const profileLinkElem = await page.waitForSelector('#profile-link', {visible: true,})
  await Promise.all([
    page.waitForNavigation(),
    profileLinkElem.click(),
  ])
  const url = new URL(page.url())
  t.is(url.pathname, `/u/${user.username}`)

  await page.waitForSelector('#user-avatar', {visible: true,})
  t.is(await page.$eval('#user-name', (elem) => {return elem.innerText}), user.name)
  t.is(await page.$eval('#user-username', (elem) => {return elem.innerText}), user.username)
  t.is(await page.$eval('#user-bio', (elem) => {return elem.innerText}), user.bio)
  const addedStr = `Joined ${user.added.toLocal().toLocaleString(DateTime.DATE_FULL)}`
  t.is(await page.$eval('#user-join-date', (elem) => {return elem.innerText}), addedStr)
  t.is(await page.$eval('#user-num-meets-gone-to', (elem) => {return elem.innerText}),
    'Has gone to 0 meets')
  t.is(await page.$eval('#user-num-groups', (elem) => {return elem.innerText}), 'Member of 1 group')
})

const logUserIn = async(t, user) => {
  const {page,} = t.context

  t.log(`Navigating to ${t.context.rootUrl}/login`)
  await page.goto(`${t.context.rootUrl}/login`)
  t.log(`Waiting for login form to become visible`)
  await page.waitForSelector('#login-form', {visible: true,})
  const loginButton = await page.$('#submit-login-button')

  await page.type('#login-input-usernameoremail', user.username)
  await page.type('#login-input-password', user.password)
  await page.waitForFunction(() => {
    const btn = document.querySelector(`#submit-login-button`)
    return !btn.disabled
  })
  t.log('Logging user in')
  await Promise.all([
    page.waitForNavigation(),
    loginButton.click(),
  ])

  let url = new URL(page.url())
  t.is(url.pathname, '/')
}

const setupDatabase = async (t) => {
  const {pgPool,} = t.context

  const added = DateTime.utc()
  const user = {
    username: 'test',
    name: 'Some User',
    emailAddress: 'test@example.com',
    added,
    bio: 'About a user.',
    password: 'p@ssword1234',
  }
  const passwordHash = await bcrypt.hash(user.password, 10)
  const group = {
    path: 'test',
    name: 'Test',
    added,
    description: 'A test group',
    members: {
      [user.username]: 'admin',
    },
  }

  t.log('Inserting test data into database', {
    user,
    group,
  })
  const client = await pgPool.connect()
  let userId
  let groupId
  try {
    let rslt = await client.query(`INSERT INTO users (username, name, email_address, password_hash, added,
      bio) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id`, [user.username, user.name, user.emailAddress,
        passwordHash, added.toISO(), user.bio,])
    userId = rslt.rows[0].id
    rslt = await client.query(`INSERT INTO groups (path, name, added, description)
      VALUES ($1, $2, $3, $4) RETURNING id`, [group.path, group.name, added.toISO(),
        group.description,])
    groupId = rslt.rows[0].id
    await client.query(`INSERT INTO users_groups (group_id, user_id, added, membership)
      VALUES ($1, $2, $3, 'admin')`, [groupId, userId, added.toISO(),])
  } finally {
    client.release()
  }

  user.id = userId
  group.id = groupId

  return [user, group,]
}
