const test = require('ava')
const Promise = require('bluebird')
const bcrypt = require('bcrypt')
const {DateTime,} = require('luxon')

require('./common')

test.serial('An unauthenticated user can visit the groups page', async (t) => {
  const {page,} = t.context

  await page.goto(`${t.context.rootUrl}`)

  const groupsElem = await page.waitForSelector('#menu-item-groups')
  await Promise.all([
    page.waitForNavigation(),
    groupsElem.click(),
  ])

  const url = new URL(page.url())
  t.is(url.pathname, '/groups')
  const headingElem = await page.waitForSelector(`#groups-heading`, {visible: true,})
  const heading = await headingElem.evaluate((elem) => {return elem.innerText})
  t.is(heading, `Groups`)

  const tabsElem = await page.waitForSelector('#groups-tabs', {visible: true,})
  const tabs = await tabsElem.$$eval('ul > li', (elems) => {
    return elems.map((elem) => {
      return elem.innerHTML
    })
  })
  t.deepEqual(tabs, ['Explore',])
  const tabContent = await page.$eval('#groups-tab-content', (elem) => {return elem.innerText})
  t.is(tabContent, 'No content to show.')
})

const logUserIn = async (t) => {
  const {page, pgPool,} = t.context

  const added = DateTime.utc().toISO()
  const user = {
    username: 'test',
    name: 'Some User',
    emailAddress: 'test@example.com',
    added,
    bio: 'About a user.',
    password: 'p@ssword1234',
  }
  const passwordHash = await bcrypt.hash(user.password, 10)
  t.log('Inserting test user into database', user)
  const client = await pgPool.connect()
  try {
    await client.query(`INSERT INTO users (username, name, email_address, password_hash, added,
      bio) VALUES ($1, $2, $3, $4, $5, $6)`, [user.username, user.name, user.emailAddress,
        passwordHash, user.added, user.bio,])
  } finally {
    client.release()
  }

  t.log(`Navigating to ${t.context.rootUrl}/login`)
  await page.goto(`${t.context.rootUrl}/login`)
  t.log(`Waiting for login form to become visible`)
  await page.waitForSelector('#login-form', {visible: true,})
  const loginButton = await page.$('#submit-login-button')

  await page.type('#login-input-usernameoremail', user.username)
  await page.type('#login-input-password', user.password)
  await page.waitForFunction(() => {
    const btn = document.querySelector(`#submit-login-button`)
    return !btn.disabled
  })
  t.log('Logging user in')
  await Promise.all([
    page.waitForNavigation(),
    loginButton.click(),
  ])

  let url = new URL(page.url())
  t.is(url.pathname, '/')
  const groupsElem = await page.waitForSelector('#menu-item-groups')
  await Promise.all([
    page.waitForNavigation(),
    groupsElem.click(),
  ])
  url = new URL(page.url())
  t.is(url.pathname, '/groups')
  t.log('Navigated to the groups page')

  return user
}

test.serial('An authenticated user can visit the groups page', async (t) => {
  await logUserIn(t)

  const {page,} = t.context

  const headingElem = await page.waitForSelector(`#groups-heading`, {visible: true,})
  const heading = await headingElem.evaluate((elem) => {return elem.innerText})
  t.is(heading, `Groups`)
  const newGroupElem = await page.waitForSelector('#new-group-button', {visible: true,})
  t.is(await newGroupElem.evaluate((elem) => {return elem.innerText}), 'New Group')
  const tabsElem = await page.waitForSelector('#groups-tabs', {visible: true,})
  const tabs = await tabsElem.$$eval('ul > li', (elems) => {
    return elems.map((elem) => {
      return elem.innerText
    })
  })
  t.deepEqual(tabs, ['Your Groups', 'Explore',])
  const activeTab = await tabsElem.$eval('ul > li.active', (elem) => {
    return elem.innerText
  })
  t.is(activeTab, 'Your Groups')
  const tabContent = await page.$eval('#groups-tab-content', (elem) => {return elem.innerText})
  t.is(tabContent, `You're not a member of any groups.`)

  t.log(`Clicking Explore tab`)
  const exploreTabElem = await page.waitForSelector('#explore-tab')
  await exploreTabElem.click()
  const exploreGroupsElem = await page.waitForSelector('#groups-tab-content #explore-groups')
  const exploreGroupsText = await exploreGroupsElem.evaluate((elem) => {return elem.innerText})
  t.is(exploreGroupsText, 'No content to show.')
})

test.serial('An authenticated user can create a group', async (t) => {
  const user = await logUserIn(t)

  const {page,} = t.context
  const groupName = 'Test'

  const newGroupElem = await page.waitForSelector('#new-group-button', {visible: true,})
  t.log('Clicking button for going to new group page')
  await Promise.all([
    page.waitForNavigation(),
    newGroupElem.click(),
  ])
  let url = new URL(page.url())
  t.is(url.pathname, '/new-group')

  const headingElem = await page.waitForSelector('h1#heading', {visible: true,})
  const heading = await headingElem.evaluate((elem) => {return elem.innerText})
  t.is(heading, 'Add a Group')
  const nameInputElem = await page.waitForSelector('#input-name', {visible: true,})
  const pathInputElem = await page.waitForSelector('#input-path', {visible: true,})
  const descriptionEditorElem = await page.waitForSelector('#description-editor', {visible: true,})
  const addButtonElem = await page.waitForSelector('#submit-add-group-button', {visible: true,})
  const cancelButtonElem = await page.waitForSelector('#cancel-add-group-button', {visible: true,})

  t.is(await nameInputElem.evaluate((elem) => {return elem.innerText}), '')
  t.is(await pathInputElem.evaluate((elem) => {return elem.innerText}), '')
  t.is(await descriptionEditorElem.evaluate((elem) => {return elem.innerText}), '')
  t.true(await addButtonElem.evaluate((elem) => {return elem.disabled}))
  t.false(await cancelButtonElem.evaluate((elem) => {return elem.disabled}))

  t.log('Filling in form')
  await nameInputElem.type(groupName)
  await pathInputElem.type('test')
  await descriptionEditorElem.type('This is a test')
  await page.waitForFunction(() => {
    return !document.querySelector('#submit-add-group-button').disabled
  })

  t.log('Submitting form')
  await Promise.all([
    page.waitForNavigation(),
    addButtonElem.click(),
  ])
  url = new URL(page.url())
  t.is(url.pathname, '/g/test')
  t.log(`Navigated to new group's page, verifying content`)

  await page.waitForSelector('#avatar-container', {visible: true,})
  t.is(await page.$eval('#group-name', (elem) => {return elem.innerText}), 'Test')
  t.is(await page.$eval('#group-path', (elem) => {return elem.innerText}), 'test')
  t.is(await page.$eval('#group-description', (elem) => {return elem.innerText}), 'This is a test')
  t.is(await page.$eval('#group-num-members', (elem) => {return elem.innerText}), 'Has 1 member')

  const tabsElem = await page.waitForSelector('#group-info-tabs', {visible: true,})
  const tabs = await tabsElem.$$eval('.tab', (elems) => {
    return elems.map((elem) => {
      return elem.innerText
    })
  })
  t.deepEqual(tabs, ['Upcoming', 'Past', 'Members',])
  const activeTab = await tabsElem.$eval('.tab.active', (elem) => {
    return elem.innerText
  })
  t.is(activeTab, 'Upcoming')
  t.is(await page.$eval('#group-upcoming', (elem) => {return elem.innerText}),
    `Test doesn't have any upcoming meets.`)

  const toolbar = await page.waitForSelector('#group-toolbar', {visible: true,})
  const buttons = await toolbar.$$eval('.button', (elems) => {
    return elems.map((elem) => {return elem.innerText})
  })
  t.deepEqual(buttons, ['Edit', 'Delete', 'New Meet',])

  t.log(`Clicking Past tab`)
  const pastTabElem = await page.waitForSelector(`#past-tab`, {visible: true,})
  await pastTabElem.click()
  const pastContentElem = await page.waitForSelector(`#group-past`, {visible: true,})
  const pastContent = await pastContentElem.evaluate((elem) => {return elem.innerText})
  t.is(pastContent, `Test doesn't have any past meets.`)

  t.log(`Clicking Members tab`)
  const membersTabElem = await page.waitForSelector(`#members-tab`, {visible: true,})
  await membersTabElem.click()
  const membersContentElem = await page.waitForSelector(`#group-members`, {visible: true,})
  const membersContent = await membersContentElem.$$eval(
    '.group-member-container .group-member-content a', (elems) => {
      return elems.map((elem) => {
        const rslt = {
          pathname: elem.pathname,
          name: elem.querySelector('.group-member-name').innerText,
          bio: elem.querySelector('.group-member-bio').innerText,
        }
        return rslt
      })
    })
  t.deepEqual(membersContent, [
    {
      pathname: `/u/${user.username}`,
      name: `${user.name} (${user.username})`,
      bio: user.bio,
    },
  ])

  t.log(`Navigating to groups page, to verify that group is added client side`)
  const groupsLink = await page.waitForSelector('#menu-item-groups', {visible: true,})
  await Promise.all([
    page.waitForNavigation(),
    groupsLink.click(),
  ])
  // Verify that "Your Groups" tab is active
  await page.waitForSelector('#your-groups-tab.active', {visible: true,})
  const groupNames = await page.$$eval('.group-content .group-name', (elems) => {
    return elems.map((elem) => {
      return elem.innerText
    })
  })
  t.deepEqual(groupNames, [groupName,])
})
