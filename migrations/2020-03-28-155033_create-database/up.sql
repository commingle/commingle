CREATE TABLE users (
  id BIGSERIAL PRIMARY KEY,
  username TEXT NOT NULL UNIQUE,
  name TEXT NOT NULL,
  email_address TEXT NOT NULL UNIQUE,
  password_hash TEXT NOT NULL,
  added TIMESTAMPTZ NOT NULL,
  avatar_url TEXT,
  bio TEXT
);
CREATE TABLE groups (
  id BIGSERIAL PRIMARY KEY,
  path TEXT NOT NULL UNIQUE,
  name TEXT NOT NULL,
  added TIMESTAMPTZ NOT NULL,
  avatar_url TEXT,
  description TEXT NOT NULL
);
CREATE TABLE meets (
  id BIGSERIAL PRIMARY KEY,
  uuid TEXT NOT NULL,
  group_id BIGINT NOT NULL REFERENCES groups(id) ON DELETE CASCADE,
  title TEXT NOT NULL,
  starts TIMESTAMPTZ NOT NULL,
  ends TIMESTAMPTZ NOT NULL,
  description TEXT NOT NULL,
  attendee_limit INT,
  added TIMESTAMPTZ NOT NULL,
  location_place_id TEXT NOT NULL,
  location_name TEXT NOT NULL,
  location_address TEXT NOT NULL
);
CREATE TABLE meets_hosts (
  id BIGSERIAL PRIMARY KEY,
  meet_id BIGINT NOT NULL REFERENCES meets(id) ON DELETE CASCADE,
  user_id BIGINT NOT NULL REFERENCES users(id) ON DELETE CASCADE,
  UNIQUE (meet_id, user_id)
);
CREATE TABLE meets_attendees (
  id BIGSERIAL PRIMARY KEY,
  meet_id BIGINT NOT NULL REFERENCES meets(id) ON DELETE CASCADE,
  user_id BIGINT NOT NULL REFERENCES users(id) ON DELETE CASCADE,
  UNIQUE (meet_id, user_id)
);
CREATE TABLE users_groups (
  id BIGSERIAL PRIMARY KEY,
  group_id BIGINT NOT NULL REFERENCES groups(id) ON DELETE CASCADE,
  user_id BIGINT NOT NULL REFERENCES users(id) ON DELETE CASCADE,
  UNIQUE (group_id, user_id)
);
