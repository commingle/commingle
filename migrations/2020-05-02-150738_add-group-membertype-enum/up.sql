CREATE TYPE group_membership AS ENUM ('member', 'admin');
ALTER TABLE users_groups ADD COLUMN membership group_membership NOT NULL DEFAULT 'admin';
ALTER TABLE users_groups ALTER COLUMN membership DROP DEFAULT;
