CREATE TABLE meets_hosts_external (
  id BIGSERIAL PRIMARY KEY,
  meet_id BIGINT NOT NULL REFERENCES meets(id) ON DELETE CASCADE,
  name TEXT NOT NULL,
  added TIMESTAMPTZ NOT NULL,
  UNIQUE (meet_id, name)
);
CREATE TABLE meets_attendees_external (
  id BIGSERIAL PRIMARY KEY,
  meet_id BIGINT NOT NULL REFERENCES meets(id) ON DELETE CASCADE,
  name TEXT NOT NULL,
  added TIMESTAMPTZ NOT NULL,
  UNIQUE (meet_id, name)
);
