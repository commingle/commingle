/* global process */
const fs = require('fs-extra')
const gulp = require('gulp')
const gulpRename = require('gulp-rename')
const browserify = require('browserify')
const exorcist = require('exorcist')
const source = require('vinyl-source-stream')

fs.removeSync('dist')
fs.mkdirSync('dist')

const createBundle = () => {
  const b = browserify({
    entries: 'client/index.js',
    debug: true,
  })
    .transform('babelify')
    .transform('sheetify', {
      'transform': [
        [
          '@arve.knudsen/sheetify-jstransformer',
          {
            transform: require('jstransformer-stylus'),
          },
        ],
      ],
    })
    .transform('uglifyify')
    .plugin('css-extract', {out: 'dist/bundle.css',})
    .plugin('common-shakeify')
  return b.bundle()
    .pipe(exorcist('dist/bundle.js.map', 'bundle.js.map'))
    .pipe(source('index.js'))
    .pipe(gulpRename('bundle.js'))
    .pipe(gulp.dest('dist/'))
}

const assets = () => {
  return gulp
    .src('public/**/*')
    .pipe(gulp.dest('dist/public/'))
}

module.exports = {
  default: gulp.series(createBundle, assets),
}
