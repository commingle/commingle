#![feature(proc_macro_hygiene)]
#![feature(async_closure)]

use clap::{App, Arg};
use dotenv::dotenv;
use std::env;

mod migrations;

use database::*;
use migrations::run_migrations;

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    match dotenv() {
        Ok(_) => println!("* Successfully loaded .env file"),
        Err(err) => println!("* Failed to load .env file: {}", err),
    };

    let version = env::var("CARGO_PKG_VERSION").expect("Failed to get $CARGO_PKG_VERSION");
    let matches = App::new("Commingle")
        .version(version.as_str())
        .about("Open meetup platform")
        .arg(
            Arg::with_name("port")
                .short("p")
                .long("port")
                .takes_value(true)
                .help("Port to listen on [default: 8000]"),
        )
        .arg(
            Arg::with_name("database")
                .short("D")
                .long("database")
                .takes_value(true)
                .help("Database to use [default: commingle]"),
        )
        .arg(
            Arg::with_name("mandrillApiUrl")
                .long("mandrill-api-url")
                .takes_value(true)
                .help("Override Mandrill API URL"),
        )
        .arg(
            Arg::with_name("gmapsApiUrl")
                .long("gmaps-api-url")
                .takes_value(true)
                .help("Override Google Maps API URL"),
        )
        .get_matches();
    let port = matches.value_of("port").unwrap_or("8000").parse::<u16>()?;
    let db_name = matches.value_of("database").unwrap_or("commingle");
    let mandrill_api_url = matches
        .value_of("mandrillApiUrl")
        .unwrap_or("https://mandrillapp.com/api/1.0/messages/send.json");
    let gmaps_api_url = matches
        .value_of("gmapsApiUrl")
        .unwrap_or("https://maps.googleapis.com");

    let db_host = env::var("DATABASE_HOST").unwrap_or(String::from("localhost"));
    let db_user = env::var("DATABASE_USER").expect("DATABASE_USER must be set");
    let db_pass = env::var("DATABASE_PASSWORD").expect("DATABASE_PASSWORD must be set");
    run_migrations(&db_host, &db_user, &db_pass, db_name)?;

    let database: Database = Database::new(&db_host, &db_user, &db_pass, db_name).await;

    let maps_api_key = env::var("MAPS_API_KEY").expect("Failed to get $MAPS_API_KEY");
    let maps_signing_key_b64 =
        std::env::var("MAPS_STATIC_SIGNING_KEY").expect("Failed to get $MAPS_STATIC_SIGNING_KEY");
    // The signing key will be URL safe base64 encoded, where - is replaced with + and _ with /
    let maps_signing_key_b64 = str::replace(&maps_signing_key_b64, "-", "+");
    let maps_signing_key_b64 = str::replace(&maps_signing_key_b64, "_", "/");
    let maps_signing_key =
        base64::decode(&maps_signing_key_b64).expect("Failed to decode signing key");
    let mandrill_api_key = env::var("MANDRILL_API_KEY").expect("Failed to get $MANDRILL_API_KEY");

    let ctx = context::Context {
        port: port,
        database: database.clone(),
        conf: conf::Conf {
            maps_api_url: String::from(gmaps_api_url),
            maps_api_key,
            maps_signing_key,
            mandrill_api_url: String::from(mandrill_api_url),
            mandrill_api_key,
        },
    };
    println!("* Server starting");
    web::serve(ctx).await
}
