use anyhow::Result;
use diesel::connection::Connection;
use diesel::pg::PgConnection;

/// Run pending Diesel migrations.
pub fn run_migrations(db_host: &str, db_user: &str, db_pass: &str, db_name: &str) -> Result<()> {
    let db_url = format!("postgres://{}:{}@{}/{}", db_user, db_pass, db_host, db_name);
    println!("* Running any pending database migrations...");
    let conn = PgConnection::establish(&db_url)?;
    diesel_migrations::run_pending_migrations(&conn)?;
    Ok(())
}
