# Commingle
[![pipeline status](https://gitlab.com/commingle/commingle/badges/master/pipeline.svg)](https://gitlab.com/commingle/commingle/commits/master)

An open and community focused meetup platform.

## Development
Before you run Commingle for the first time, you need a .env file in order to set required
environment variables. You can copy .env.example as a template.

### Prerequisites
In order to run the development version of Commingle, you need the following:

* Rust: Install via [Rustup](https://rustup.rs).
* Node.js: Use the [Volta](volta.sh) version manager, and you'll get the right version automatically
* Diesel: `cargo install diesel_cli --no-default-features --features "postgres sqlite"`

### Running Commingle
In order to run Commingle locally, you can use the run-server script:

```
./run-server
```

### Testing
In order to run the test suite, execute the following:

```
npm install
npm test
```

## Contribute
If you wish to contribute, please join our [Discord chat server](https://discord.gg/xbbQmnN).
