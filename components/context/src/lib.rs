use conf::Conf;
use database::Database;

// Context holds app context.
#[derive(Clone)]
pub struct Context {
    pub port: u16,
    pub database: Database,
    pub conf: Conf,
}
