use conf::Conf;
use hmac::{Mac, NewMac};

/// Get a Google Maps URL from an address.
pub fn get_map_url(conf: &Conf, address: &str) -> String {
    let zoom = "17";
    let size = "300x187";
    let markers = format!("|{}", address);

    // Construct basic URL, with a marker for the requested location
    let url: reqwest::Url = reqwest::Url::parse_with_params(
        format!("{}/maps/api/staticmap", conf.maps_api_url).as_str(),
        &[
            ("zoom", zoom),
            ("size", size),
            ("markers", &markers),
            ("key", &conf.maps_api_key),
        ],
    )
    .expect("Failed to parse URL");
    let url_to_sign = format!(
        "{}?{}",
        url.path(),
        url.query().expect("Failed to get query string")
    );
    println!("Signing URL '{}'", url_to_sign);

    // Make URL signature
    let mut mac =
        hmac::Hmac::<sha1::Sha1>::new_varkey(&conf.maps_signing_key).expect("Failed to create mac");
    mac.update(&url_to_sign.as_bytes());
    let signature = mac.finalize();
    // Get URL safe base64 encoded signature
    let enc_signature = base64::encode(&signature.into_bytes());
    let enc_signature = str::replace(&enc_signature, "+", "-");
    let enc_signature = str::replace(&enc_signature, "/", "_");

    let signed_url = reqwest::Url::parse_with_params(
        format!("{}/maps/api/staticmap", conf.maps_api_url).as_str(),
        &[
            ("zoom", zoom),
            ("size", size),
            ("markers", &markers),
            ("key", &conf.maps_api_key),
            ("signature", &enc_signature),
        ],
    )
    .expect("Failed to parse URL");
    String::from(signed_url.as_str())
}
