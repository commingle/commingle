#![feature(proc_macro_hygiene)]
#![feature(async_closure)]
#[macro_use]
extern crate maud;

use anyhow::Result;
use auth::try_auth;
use context::Context;
use serde::{Deserialize, Serialize};
use std::env;
use std::sync::Arc;
use viewmodels::*;
use warp::Filter;

mod svg;

use svg::*;

pub async fn serve(ctx: Context) -> Result<()> {
    let svgs = load_svgs()?;

    let ctx_clone = ctx.clone();
    let ctx_filter = warp::any().map(move || ctx_clone.clone());

    let homepage = warp::get()
        .and(warp::filters::cookie::optional("sid"))
        .and(ctx_filter.clone())
        .and_then(try_auth)
        .and_then(move |session: Option<UserViewModel>| {
            println!("Getting index, logged in: {}", session.is_some());
            get_index(session, svgs.clone())
        });
    let js_bundle = warp::get()
        .and(warp::path("bundle.js"))
        .and(warp::path::end())
        .and(warp::fs::file("./dist/bundle.js"));
    let js_map = warp::get()
        .and(warp::path("bundle.js.map"))
        .and(warp::path::end())
        .and(warp::fs::file("./dist/bundle.js.map"));
    let css_bundle = warp::get()
        .and(warp::path("bundle.css"))
        .and(warp::path::end())
        .and(warp::fs::file("./dist/bundle.css"));
    let healthz = warp::path("healthz").and(warp::get()).and_then(get_healthz);

    let routes = js_bundle.or(js_map).or(css_bundle).or(healthz);
    let routes = api::add_routes(routes.boxed(), &ctx).or(homepage);

    let (addr, fut) = warp::serve(routes).bind_ephemeral(([127, 0, 0, 1], ctx.port));
    println!("* Serving on localhost:{}", addr.port());
    fut.await;

    Ok(())
}

/// Handler for site index route. Renders HTML index document.
async fn get_index(
    session: Option<UserViewModel>,
    svgs: Arc<Vec<Svg>>,
) -> Result<impl warp::Reply, warp::Rejection> {
    println!("Getting index");
    match session.as_ref() {
        Some(ref user) => {
            println!("User is authenticated: {}", user.username);
        }
        None => {
            println!("User is not authenticated");
        }
    };

    let app_config = AppConfig {
        log_level: "debug".into(),
    };
    let initial_state = InitialState {
        logged_in_user: session.clone(),
        version: env::var("CARGO_PKG_VERSION").expect("Failed to get $CARGO_PKG_VERSION"),
    };
    let app_config_str = serde_json::to_string(&app_config).expect("Serialize app config");
    let initial_state_str =
        serde_json::to_string(&initial_state).expect("Failed to serialize initial state");
    println!("Sending initial state to client: {}", initial_state_str);

    // Render HTML using Maud templating engine
    let doc = html! {
        head {
            meta charset="utf-8";
            meta name="viewport" content="width=device-width, initial-scale=1";
            meta property="og:type" content="website";

            title {"Commingle"}

            link rel="stylesheet" href="https://unpkg.com/tachyons@4.10.0/css/tachyons.min.css";
            link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat|Proza+Libre";
            link rel="stylesheet" href="/bundle.css";

            style {
                "#container {visibility: hidden};"
            }

            script type="text/javascript" src="/bundle.js" defer="true" {}
        }
        body."w-100" {
            svg aria-hidden="true" style="position: absolute; width: 0; height: 0; overflow: hidden;" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" {
                defs {
                    @for svg in svgs.iter() {
                        (svg)
                    }
                }
            }
            #app-config data-json=(app_config_str) {}
            #initial-state data-json=(initial_state_str) {}
            #container {}
        }
    };
    Ok(warp::reply::html(doc.into_string()))
}

pub async fn get_healthz() -> Result<impl warp::Reply, warp::Rejection> {
    // TODO: Test database connection
    Ok(warp::reply::reply())
}

#[derive(Serialize, Deserialize)]
struct Session {
    // TODO: Convert to username
    id: i64,
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
struct AppConfig {
    pub log_level: String,
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
struct InitialState {
    pub logged_in_user: Option<UserViewModel>,
    pub version: String,
}
