use anyhow::Result;
use maud::{Markup, Render};
use std::fs::File;
use std::io::prelude::*;
use std::sync::Arc;

#[derive(Clone)]
pub struct Svg {
    pub name: String,
    pub paths: Vec<String>,
    pub view_box: String,
}

impl Render for Svg {
    fn render(&self) -> Markup {
        html! {
            symbol id=({format!("icon-{}", self.name)}) viewBox=(self.view_box) {
                title {(self.name)}
                @for path in &self.paths {
                    path d=(path){}
                }
            }
        }
    }
}

pub fn load_svgs() -> Result<Arc<Vec<Svg>>> {
    // TODO: Compile resource into binary
    let mut file =
        File::open("assets/icons/selection.json").expect("Failed to open selection.json");
    let mut contents = String::new();
    file.read_to_string(&mut contents)
        .expect("Failed to read selection.json");
    let v: serde_json::Value =
        serde_json::from_str(&contents).expect("Failed to decode selection.json");
    let icons = v["icons"].as_array().expect("Failed to get icons as array");

    let mut svgs: Vec<Svg> = vec![];
    for icon in icons {
        let icon = icon.as_object().expect("Failed to get icon as object");
        let props = icon
            .get("properties")
            .expect("Failed to get icon properties");
        let props = props
            .as_object()
            .expect("Failed to get icon properties as object");
        let name = String::from(
            props
                .get("name")
                .expect("Failed to get icon name")
                .as_str()
                .expect("Failed to get icon name"),
        );

        let mut file =
            File::open(format!("assets/icons/SVG/{}.svg", name)).expect("Failed to open SVG");
        let mut contents = String::new();
        file.read_to_string(&mut contents)
            .expect("Failed to read SVG");
        let doc = roxmltree::Document::parse(&contents).expect("Failed to parse SVG");
        let svg_elem = doc
            .descendants()
            .find(|n| n.has_tag_name("svg"))
            .expect("Couldn't find SVG element");
        let view_box = String::from(
            svg_elem
                .attribute("viewBox")
                .expect("Failed to get viewBox attribute"),
        );
        let paths: Vec<String> = svg_elem
            .children()
            .filter_map(|n| {
                if !n.has_tag_name("path") {
                    return None;
                }
                Some(String::from(
                    n.attribute("d")
                        .expect("Failed to get 'd' attribute of SVG path element"),
                ))
            })
            .collect();

        svgs.push(Svg {
            name,
            view_box,
            paths,
        });
    }
    Ok(Arc::new(svgs))
}
