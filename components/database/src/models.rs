use chrono::{DateTime, Utc};
use md5::{Digest, Md5};
use serde::{Deserialize, Serialize};

/// Representation of a user to be added to the database.
#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct NewUser {
    pub username: String,
    pub name: String,
    pub email_address: String,
    pub password_hash: String,
    pub added: DateTime<Utc>,
    pub avatar_url: Option<String>,
    pub bio: Option<String>,
}

impl NewUser {
    pub fn new(
        username: String,
        name: String,
        email_address: String,
        password_hash: String,
        bio: Option<String>,
    ) -> Self {
        Self {
            username,
            name,
            email_address,
            password_hash,
            added: Utc::now(),
            avatar_url: None,
            bio,
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct User {
    pub id: i64,
    pub username: String,
    pub name: String,
    pub email_address: String,
    #[serde(skip_serializing)]
    pub password_hash: String,
    pub added: DateTime<Utc>,
    pub avatar_url: Option<String>,
    pub bio: Option<String>,
}

/// Representation of a group to be added to the database.
#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct NewGroup {
    pub path: String,
    pub name: String,
    pub added: DateTime<Utc>,
    pub avatar_url: Option<String>,
    pub description: String,
}

/// A payload sent with a request to edit a group.
#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct EditGroupPayload {
    pub name: String,
    pub description: String,
    pub gitlab_group_path: Option<String>,
}

#[derive(Serialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Group {
    #[serde(skip_serializing)]
    pub id: i64,
    pub path: String,
    pub name: String,
    pub added: DateTime<Utc>,
    pub avatar_url: Option<String>,
    pub description: String,
}

/// Representation of a user<->group membership to be added to the database.
#[derive(Debug, Serialize, Deserialize)]
pub struct NewUserGroup {
    pub user_id: i64,
    pub group_id: i64,
    pub added: DateTime<Utc>,
}

/// A many-to-many association between users and groups.
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct UserGroup {
    pub id: i64,
    pub user_id: i64,
    pub group_id: i64,
    pub added: DateTime<Utc>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Meet {
    #[serde(skip_serializing)]
    pub id: i64,
    pub uuid: String,
    pub title: String,
    pub location: Option<MeetLocation>,
    pub starts: DateTime<Utc>,
    pub ends: DateTime<Utc>,
    pub description: String,
    pub attendee_limit: Option<i32>,
    pub added: DateTime<Utc>,
    #[serde(skip_serializing)]
    pub group_id: i64,
    pub is_online: bool,
}

/// Representation of a meet attendance to be added to the database.
#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct NewAttendance {
    pub meet_id: i64,
    pub user_id: i64,
}

/// Representation of a meet host to be added to the database.
#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct NewHost {
    pub meet_id: i64,
    pub user_id: i64,
}

/// A payload sent with a request to add a meet.
#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AddMeetPayload {
    pub group_path: String,
    pub title: String,
    pub location: Option<MeetLocation>,
    pub starts: DateTime<Utc>,
    pub ends: Option<DateTime<Utc>>,
    pub description: String,
    pub hosts: Vec<String>,
    pub hosts_external: Vec<String>,
    pub attendees_external: Vec<String>,
    pub attendee_limit: Option<i32>,
    pub is_online: bool,
}

/// A payload sent with a request to edit a meet.
#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct EditMeetPayload {
    pub title: String,
    pub location: Option<MeetLocation>,
    pub starts: DateTime<Utc>,
    pub ends: Option<DateTime<Utc>>,
    pub description: String,
    pub hosts: Vec<String>,
    pub attendee_limit: Option<i32>,
    pub is_online: bool,
}

/// Representation of a location for a meet.
#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct MeetLocation {
    /// ID of a Google Maps location.
    pub place_id: String,
    /// Name of a Google Maps location.
    pub name: String,
    /// Address of a Google Maps location.
    pub address: String,
}

pub fn get_user_avatar_url(url: &Option<String>, email_address: &str) -> String {
    match &url {
        Some(url) => url.clone(),
        None => {
            let mut hasher = Md5::new();
            hasher.input(email_address.to_lowercase());
            let email_hash = hasher.result();
            format!(
                "http://www.gravatar.com/avatar/{:x}?d=identicon&s=300",
                email_hash
            )
        }
    }
}
