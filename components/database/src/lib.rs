use anyhow::{anyhow, Result};
use bb8::Pool;
use bb8_postgres::PostgresConnectionManager;
use chrono::{DateTime, Duration, Utc};
use futures::future::join_all;
use google;
use postgres_types::ToSql;
use serde::Deserialize;
use std::collections::{HashMap, HashSet};
use std::ops::Add;
use tokio_postgres::{NoTls, Transaction};
use uuid::Uuid;

pub mod models;

use conf::Conf;
pub use models::*;
use viewmodels::*;

#[derive(Clone)]
pub struct Database {
    pool: Pool<PostgresConnectionManager<NoTls>>,
}

/// A patch to user data.
#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct UserPatch {
    pub name: Option<String>,
    pub email_address: Option<String>,
    pub avatar_url: Option<String>,
    pub bio: Option<String>,
}

/// An update to a user's password.
#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct PasswordUpdate {
    pub new_password: String,
    pub old_password: String,
}

async fn get_user_by_username(
    username: &str,
    txn: &Transaction<'_>,
) -> Result<Option<UserViewModel>> {
    let rows = txn
        .query(
            "SELECT users.*, groups.path AS group_path, users_groups.membership AS
        group_membership
        FROM users
        LEFT JOIN users_groups ON users.id = users_groups.user_id
        LEFT JOIN groups ON users_groups.group_id = groups.id
        WHERE users.username = $1",
            &[&username],
        )
        .await
        .expect("Error searching for user");
    if rows.is_empty() {
        return Ok(None);
    }

    let row = &rows[0];
    let id: i64 = row.get("id");
    let username: String = row.get("username");
    let password_hash: String = row.get("password_hash");
    let name: String = row.get("name");
    let email_address: String = row.get("email_address");
    let added: DateTime<Utc> = row.get("added");
    let avatar_url: Option<String> = row.get("avatar_url");
    let bio: Option<String> = row.get("bio");
    println!("Found user {}", username);

    let groups: Vec<GroupMembershipViewModel> = rows
        .iter()
        .filter_map(|row| match row.get("group_path") {
            Some(group_path) => {
                let membership: GroupMembership = row.get("group_membership");
                Some(GroupMembershipViewModel {
                    group_path,
                    membership,
                })
            }
            _ => None,
        })
        .collect();
    let avatar_url = get_user_avatar_url(&avatar_url, &email_address);
    let user = UserViewModel {
        id,
        username,
        password_hash,
        name,
        email_address,
        added,
        avatar_url,
        bio,
        groups,
    };

    Ok(Some(user))
}

impl Database {
    pub async fn new(db_host: &str, db_user: &str, db_pass: &str, db_name: &str) -> Self {
        let db_url = format!("postgres://{}:{}@{}/{}", db_user, db_pass, db_host, db_name);
        let manager = PostgresConnectionManager::new_from_stringlike(db_url, NoTls)
            .expect("Failed to create Postgres connection manager");
        let pool = bb8::Builder::new()
            .build(manager)
            .await
            .expect("Failed to build DB connection pool");
        Self { pool }
    }

    /// Get user by ID.
    pub async fn get_user(&self, id: i64) -> Option<UserViewModel> {
        let conn = self
            .pool
            .get()
            .await
            .expect("Failed to obtain DB connection");
        let rows = conn
            .query(
                "SELECT users.*, groups.path AS group_path, users_groups.membership AS
                group_membership
                FROM users
                LEFT JOIN users_groups ON users.id = users_groups.user_id
                LEFT JOIN groups ON groups.id = users_groups.group_id
                WHERE users.id = $1",
                &[&id],
            )
            .await
            .expect("Error searching for user");
        if rows.is_empty() {
            return None;
        }

        let row = &rows[0];
        let id: i64 = row.get("id");
        let username: String = row.get("username");
        let password_hash: String = row.get("password_hash");
        let name: String = row.get("name");
        let email_address: String = row.get("email_address");
        let added: DateTime<Utc> = row.get("added");
        let avatar_url: Option<String> = row.get("avatar_url");
        let bio: Option<String> = row.get("bio");
        println!("Found user {}", username);

        let groups: Vec<GroupMembershipViewModel> = rows
            .iter()
            .filter_map(|row| match row.get("group_path") {
                Some(group_path) => {
                    let membership: GroupMembership = row.get("group_membership");
                    Some(GroupMembershipViewModel {
                        group_path,
                        membership,
                    })
                }
                _ => None,
            })
            .collect();
        let avatar_url = get_user_avatar_url(&avatar_url, &email_address);
        let user = UserViewModel {
            id,
            username,
            password_hash,
            name,
            email_address,
            added,
            avatar_url,
            bio,
            groups,
        };

        Some(user)
    }

    /// Get user by username.
    pub async fn get_user_by_username(&self, username: &str) -> Option<UserViewModel> {
        let mut conn = self
            .pool
            .get()
            .await
            .expect("Failed to obtain DB connection");
        let txn: Transaction = conn.transaction().await.expect("Failed to get transaction");
        get_user_by_username(username, &txn)
            .await
            .expect("Failed to get user")
    }

    /// Get user by username or email address.
    pub async fn get_user_by_username_or_email(
        &self,
        username_or_email: &str,
    ) -> Option<UserViewModel> {
        let conn = self
            .pool
            .get()
            .await
            .expect("Failed to obtain DB connection");
        let rows = conn
            .query(
                "SELECT users.*, groups.path AS group_path, users_groups.membership AS
            group_membership
            FROM users
            LEFT JOIN users_groups ON users.id = users_groups.user_id
            LEFT JOIN groups ON users_groups.group_id = groups.id
            WHERE users.username = $1 OR users.email_address = $1",
                &[&username_or_email],
            )
            .await
            .expect("Error searching for user");
        if rows.is_empty() {
            println!("Couldn't find user '{}' in database", username_or_email);
            return None;
        }

        let row = &rows[0];
        let id: i64 = row.get("id");
        let username: String = row.get("username");
        let name: String = row.get("name");
        let email_address: String = row.get("email_address");
        let password_hash: String = row.get("password_hash");
        let added: DateTime<Utc> = row.get("added");
        let avatar_url: Option<String> = row.get("avatar_url");
        let bio: Option<String> = row.get("bio");
        println!("Found user {}", username);

        let groups: Vec<GroupMembershipViewModel> = rows
            .iter()
            .filter_map(|row| match row.get("group_path") {
                Some(group_path) => {
                    let membership: GroupMembership = row.get("group_membership");
                    Some(GroupMembershipViewModel {
                        group_path,
                        membership,
                    })
                }
                _ => None,
            })
            .collect();
        let avatar_url = get_user_avatar_url(&avatar_url, &email_address);

        Some(UserViewModel {
            id,
            password_hash,
            username,
            name,
            email_address,
            added,
            avatar_url,
            bio,
            groups,
        })
    }

    /// Patch user.
    pub async fn patch_user(&self, username: &str, patch: UserPatch) -> Result<UserViewModel> {
        let mut conn = self.pool.get().await?;
        let txn = conn.transaction().await?;

        let mut user = get_user_by_username(username, &txn)
            .await?
            .ok_or(anyhow!("User not found"))?;

        let mut args_map = HashMap::new();
        match patch.email_address {
            Some(email_address) => {
                args_map.insert("email_address", email_address.clone());
                user.email_address = email_address;
            }
            _ => {}
        };
        match patch.name {
            Some(name) => {
                args_map.insert("name", name.clone());
                user.name = name;
            }
            _ => {}
        };
        match patch.bio {
            Some(bio) => {
                args_map.insert("bio", bio.clone());
                user.bio = Some(bio);
            }
            _ => {}
        };
        match patch.avatar_url {
            Some(avatar_url) => {
                args_map.insert("avatar_url", avatar_url.clone());
                user.avatar_url = avatar_url;
            }
            _ => {}
        };

        let mut args_str = String::new();
        let mut index: i64 = 2;
        let username = String::from(username);
        let mut args: Vec<&(dyn ToSql + Sync)> = vec![&username as &(dyn ToSql + Sync)];
        for (key, _) in &args_map {
            if !args_str.is_empty() {
                args_str += ", ";
            }
            args_str += &format!("{} = ${}", key, index);
            index += 1;

            args.push(&args_map[key]);
        }

        let query_str = format!("UPDATE users SET {} WHERE username = $1", args_str);
        println!("Patching user - SQL: '{}', args: {:?}", query_str, args);
        txn.query(query_str.as_str(), args.as_slice()).await?;
        txn.commit().await?;

        Ok(user)
    }

    /// Update a user's password.
    pub async fn update_user_password(
        &self,
        username: &str,
        payload: PasswordUpdate,
    ) -> Result<()> {
        let mut conn = self.pool.get().await?;
        let txn = conn.transaction().await?;

        let row = match txn
            .query_one(
                "SELECT password_hash FROM users WHERE username = $1",
                &[&username],
            )
            .await
        {
            Ok(row) => row,
            Err(err) => {
                println!("An error occurred while getting user password: {}", err);
                return Err(anyhow!("Unrecognized username or password"));
            }
        };
        let password_hash: String = row.get("password_hash");
        if !bcrypt::verify(&payload.old_password, &password_hash)
            .expect("Failed to verify password")
        {
            println!(
                "Provided old password is not a match: '{}'",
                payload.old_password
            );
            return Err(anyhow!("Unrecognized username or password"));
        }

        let new_password_hash = bcrypt::hash(&payload.new_password, bcrypt::DEFAULT_COST)?;
        txn.query("UPDATE users SET password_hash=$1", &[&new_password_hash])
            .await?;
        txn.commit().await?;

        println!("Password successfully updated");

        Ok(())
    }

    pub async fn add_user(&self, user: NewUser) -> UserViewModel {
        let conn = self
            .pool
            .get()
            .await
            .expect("Failed to obtain DB connection");
        let rows = conn.query(
            "INSERT INTO users (username, name, email_address, password_hash, added, avatar_url, bio)
            VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING id", &[&user.username, &user.name,
                &user.email_address, &user.password_hash, &user.added, &user.avatar_url, &user.bio]
        ).await.expect("Error inserting user");
        let id = rows[0].get("id");

        let avatar_url = get_user_avatar_url(&user.avatar_url, &user.email_address);

        UserViewModel {
            id,
            username: user.username,
            password_hash: user.password_hash,
            name: user.name,
            email_address: user.email_address,
            added: user.added,
            avatar_url,
            bio: user.bio,
            groups: vec![],
        }
    }

    /// Get a user's groups.
    pub async fn get_user_groups(&self, username: &str) -> Vec<GroupViewModel> {
        let conn = self
            .pool
            .get()
            .await
            .expect("Failed to obtain DB connection");
        let rows = conn
            .query(
                "SELECT groups.* FROM users
            INNER JOIN users_groups ON users.id = users_groups.user_id
            INNER JOIN groups ON users_groups.group_id = groups.id
            WHERE users.username = $1
            ",
                &[&username],
            )
            .await
            .expect("Failed to load user's groups");
        rows.iter()
            .map(|row| GroupViewModel {
                path: row.get("path"),
                name: row.get("name"),
                description: row.get("description"),
                added: row.get("added"),
                avatar_url: row.get("avatar_url"),
                members: vec![],
                upcoming: vec![],
                past: vec![],
                gitlab_group_path: None,
            })
            .collect()
    }

    /// Get a user's upcoming meets.
    pub async fn get_user_upcoming_meets(&self, conf: &Conf, username: &str) -> Vec<MeetViewModel> {
        let conn = self
            .pool
            .get()
            .await
            .expect("Failed to obtain DB connection");
        let now = Utc::now();
        // Find upcoming meets that the user is hosting or attending, joined with each meet's
        // respective hosts and attendees
        let rows = &conn
            .query(
                "SELECT meets.*,
                    hosts.username AS host_username, hosts.name AS host_name,
                    hosts.avatar_url AS host_avatar_url, hosts.email_address as host_email,
                    attendees.username AS attendee_username, attendees.name AS attendee_name,
                    attendees.avatar_url AS attendee_avatar_url, attendees.email_address AS attendee_email,
                    groups.path AS group_path, groups.name AS group_name
                FROM meets
                LEFT JOIN meets_hosts ON meets_hosts.meet_id = meets.id
                LEFT JOIN meets_attendees ON meets_attendees.meet_id = meets.id
                LEFT JOIN users AS attendees ON attendees.id = meets_attendees.user_id
                LEFT JOIN users AS hosts ON hosts.id = meets_hosts.user_id
                INNER JOIN groups ON groups.id = meets.group_id
                WHERE meets.ends >= $2::timestamptz AND (
                    meets.id in (
                        SELECT meets_attendees.meet_id
                        FROM users
                        INNER JOIN meets_attendees
                        ON meets_attendees.user_id = users.id
                        WHERE users.username = $1
                    ) OR meets.id in (
                        SELECT meets_hosts.meet_id
                        FROM users
                        INNER JOIN meets_hosts
                        ON meets_hosts.user_id = users.id
                        WHERE users.username = $1
                    )
                ) AND meets.canceled IS NULL
                ORDER BY meets.starts
            ",
                &[&username, &now],
            )
            .await
            .expect("Failed to query for user's upcoming meets");

        // Register hosts and attendees per meet
        let mut hosts_map: HashMap<String, Vec<HostViewModel>> = HashMap::new();
        let mut attendees_map: HashMap<String, Vec<AttendeeViewModel>> = HashMap::new();
        for row in rows {
            let uuid: String = row.get("uuid");

            let host_username: Option<String> = row.get("host_username");
            match host_username {
                Some(username) => {
                    let email_address: String = row.get("host_email");
                    let avatar_url: Option<String> = row.get("host_avatar_url");
                    let avatar_url = get_user_avatar_url(&avatar_url, &email_address);
                    let hosts = hosts_map.entry(uuid.clone()).or_insert(vec![]);
                    hosts.push(HostViewModel {
                        username,
                        email_address,
                        name: row.get("host_name"),
                        avatar_url: avatar_url,
                    });
                }
                _ => {}
            };

            let attendee_username: Option<String> = row.get("attendee_username");
            match attendee_username {
                Some(username) => {
                    let email_address: String = row.get("attendee_email");
                    let avatar_url: Option<String> = row.get("attendee_avatar_url");
                    let avatar_url = get_user_avatar_url(&avatar_url, &email_address);
                    let attendees = attendees_map.entry(uuid.clone()).or_insert(vec![]);
                    attendees.push(AttendeeViewModel {
                        username,
                        email_address,
                        name: row.get("attendee_name"),
                        avatar_url: avatar_url,
                    });
                }
                _ => {}
            };
        }

        // Collect meets
        let mut meets_seen: HashSet<String> = HashSet::new();
        let meets: Vec<MeetViewModel> = rows
            .iter()
            .filter_map(|row| {
                let uuid: String = row.get("uuid");
                if meets_seen.contains(&uuid) {
                    return None;
                }
                meets_seen.insert(uuid.clone());

                let location_address: String = row.get("location_address");
                let location_map_url = google::get_map_url(conf, &location_address);
                let location_name: String = row.get("location_name");
                let location_place_id: String = row.get("location_place_id");
                let is_online: bool = row.get("is_online");
                let location = if !is_online {
                    Some(LocationDetailsViewModel {
                        place_id: location_place_id,
                        name: location_name,
                        address: location_address,
                        map_url: location_map_url,
                    })
                } else {
                    None
                };
                let canceled: Option<DateTime<Utc>> = row.get("canceled");
                Some(MeetViewModel {
                    uuid: uuid.clone(),
                    title: row.get("title"),
                    starts: row.get("starts"),
                    ends: row.get("ends"),
                    description: row.get("description"),
                    hosts: vec![],
                    attendee_limit: row.get("attendee_limit"),
                    added: row.get("added"),
                    group_path: row.get("group_path"),
                    group_name: row.get("group_name"),
                    location,
                    attendees: vec![],
                    canceled,
                    is_online,
                    external_hosts: vec![],
                    external_attendees: vec![],
                })
            })
            .collect();
        meets
    }

    /// Get a user's past meets.
    pub async fn get_user_past_meets(&self, conf: &Conf, username: &str) -> Vec<MeetViewModel> {
        let conn = self
            .pool
            .get()
            .await
            .expect("Failed to obtain DB connection");
        let now = Utc::now();
        let rows = &conn
            .query(
                "SELECT meets.*,
                    hosts.username AS host_username, hosts.name AS host_name,
                    hosts.avatar_url AS host_avatar_url, hosts.email_address as host_email,
                    attendees.username AS attendee_username, attendees.name AS attendee_name,
                    attendees.avatar_url AS attendee_avatar_url, attendees.email_address AS attendee_email,
                    groups.path AS group_path, groups.name AS group_name
                FROM meets
                LEFT JOIN meets_hosts ON meets_hosts.meet_id = meets.id
                LEFT JOIN meets_attendees ON meets_attendees.meet_id = meets.id
                LEFT JOIN users AS attendees ON attendees.id = meets_attendees.user_id
                LEFT JOIN users AS hosts ON hosts.id = meets_hosts.user_id
                INNER JOIN groups ON groups.id = meets.group_id
                WHERE meets.ends < $2::timestamptz AND (
                    meets.id in (
                        SELECT meets_attendees.meet_id
                        FROM users
                        INNER JOIN meets_attendees
                        ON meets_attendees.user_id = users.id
                        WHERE users.username = $1
                    ) OR meets.id in (
                        SELECT meets_hosts.meet_id
                        FROM users
                        INNER JOIN meets_hosts
                        ON meets_hosts.user_id = users.id
                        WHERE users.username = $1
                    )
                ) AND meets.canceled IS NULL
                ORDER BY meets.starts DESC
            ",
                &[&username, &now],
            )
            .await
            .expect("Failed to query for user's upcoming meets");

        // Register hosts and attendees per meet
        let mut hosts_map: HashMap<String, Vec<HostViewModel>> = HashMap::new();
        let mut attendees_map: HashMap<String, Vec<AttendeeViewModel>> = HashMap::new();
        for row in rows {
            let uuid: String = row.get("uuid");

            let host_username: Option<String> = row.get("host_username");
            match host_username {
                Some(username) => {
                    let email_address: String = row.get("host_email");
                    let avatar_url: Option<String> = row.get("host_avatar_url");
                    let avatar_url = get_user_avatar_url(&avatar_url, &email_address);
                    let hosts = hosts_map.entry(uuid.clone()).or_insert(vec![]);
                    hosts.push(HostViewModel {
                        username,
                        email_address,
                        name: row.get("host_name"),
                        avatar_url: avatar_url,
                    });
                }
                _ => {}
            };

            let attendee_username: Option<String> = row.get("attendee_username");
            match attendee_username {
                Some(username) => {
                    let email_address: String = row.get("attendee_email");
                    let avatar_url: Option<String> = row.get("attendee_avatar_url");
                    let avatar_url = get_user_avatar_url(&avatar_url, &email_address);
                    let attendees = attendees_map.entry(uuid.clone()).or_insert(vec![]);
                    attendees.push(AttendeeViewModel {
                        username,
                        email_address,
                        name: row.get("attendee_name"),
                        avatar_url: avatar_url,
                    });
                }
                _ => {}
            };
        }

        // Collect meets
        let mut meets_seen: HashSet<String> = HashSet::new();
        let meets: Vec<MeetViewModel> = rows
            .iter()
            .filter_map(|row| {
                let uuid: String = row.get("uuid");
                if meets_seen.contains(&uuid) {
                    return None;
                }
                meets_seen.insert(uuid.clone());

                let location_place_id: String = row.get("location_place_id");
                let location_name: String = row.get("location_name");
                let location_address: String = row.get("location_address");
                let location_map_url = google::get_map_url(conf, &location_address);
                let canceled: Option<DateTime<Utc>> = row.get("canceled");
                let is_online: bool = row.get("is_online");
                let location = if !is_online {
                    Some(LocationDetailsViewModel {
                        place_id: location_place_id,
                        name: location_name,
                        address: location_address,
                        map_url: location_map_url,
                    })
                } else {
                    None
                };
                Some(MeetViewModel {
                    uuid,
                    title: row.get("title"),
                    starts: row.get("starts"),
                    ends: row.get("ends"),
                    description: row.get("description"),
                    hosts: vec![],
                    attendee_limit: row.get("attendee_limit"),
                    added: row.get("added"),
                    group_path: row.get("group_path"),
                    group_name: row.get("group_name"),
                    location,
                    attendees: vec![],
                    external_attendees: vec![],
                    external_hosts: vec![],
                    canceled,
                    is_online,
                })
            })
            .collect();
        meets
    }

    /// Get group view model by path.
    pub async fn get_group_view_model_by_path(&self, path: &str) -> Option<GroupViewModel> {
        let conn = self
            .pool
            .get()
            .await
            .expect("Failed to obtain DB connection");
        let rows = conn
            .query(
                "SELECT groups.*, users.username AS member_username, users.name AS member_name,
            users.avatar_url AS member_avatar_url, users.bio AS member_bio,
            users.email_address AS member_email_address, users_groups.membership
            FROM groups
            LEFT JOIN users_groups ON users_groups.group_id = groups.id
            LEFT JOIN users ON users.id = users_groups.user_id
            WHERE groups.path = $1",
                &[&path],
            )
            .await
            .expect("Failed to query for group");
        if rows.is_empty() {
            println!("Couldn't find group '{}'", path);
            return None;
        }

        let row = &rows[0];
        let name: String = row.get("name");
        let added: DateTime<Utc> = row.get("added");
        let avatar_url: Option<String> = row.get("avatar_url");
        let description: String = row.get("description");
        let gitlab_group_path: Option<String> = row.get("gitlab_group_path");

        println!("Found group '{}'", path);
        let members = rows
            .iter()
            .filter_map(|row| {
                let username_opt: Option<String> = row.get("member_username");
                if username_opt.is_none() {
                    return None;
                }

                let username: String = username_opt.unwrap();
                let name: String = row.get("member_name");
                let avatar_url: Option<String> = row.get("member_avatar_url");
                let email_address: String = row.get("member_email_address");
                let bio: Option<String> = row.get("member_bio");
                let membership: GroupMembership = row.get("membership");
                Some(GroupMemberViewModel {
                    username,
                    name,
                    email_address: email_address.clone(),
                    avatar_url: get_user_avatar_url(&avatar_url, &email_address),
                    bio,
                    membership,
                })
            })
            .collect();

        let now = Utc::now();
        let id: i64 = row.get("id");

        // Query for upcoming meets belonging to the group, as well as their attendees.
        let rows = conn
            .query(
                "SELECT *, users.username as attendee_username
                FROM meets
                LEFT JOIN meets_attendees ON meets_attendees.meet_id = meets.id
                LEFT JOIN users ON users.id = meets_attendees.user_id
                WHERE meets.group_id = $1 AND meets.ends >= $2::timestamptz AND MEETS.canceled IS NULL
                ORDER BY meets.starts
                ",
                &[&id, &now],
            )
            .await
            .expect("Failed to load upcoming meets for group");
        // Collect attendees per meet
        let mut attendees_map: HashMap<String, Vec<String>> = HashMap::new();
        for row in rows.iter() {
            let uuid: String = row.get("uuid");
            let attendee_username: Option<String> = row.get("attendee_username");
            match attendee_username {
                Some(username) => {
                    let attendees = attendees_map.entry(uuid.clone()).or_insert(vec![]);
                    attendees.push(username);
                }
                _ => {}
            };
        }
        let mut meets_seen: HashSet<String> = HashSet::new();
        let upcoming: Vec<GroupMeetViewModel> = rows
            .iter()
            .filter_map(|row| {
                let uuid: String = row.get("uuid");
                if meets_seen.contains(&uuid) {
                    return None;
                }
                meets_seen.insert(uuid.clone());

                let going = attendees_map.get(&uuid).cloned().unwrap_or(vec![]);
                Some(GroupMeetViewModel {
                    uuid,
                    title: row.get("title"),
                    location_place_id: row.get("location_place_id"),
                    location_name: row.get("location_name"),
                    location_address: row.get("location_address"),
                    starts: row.get("starts"),
                    ends: row.get("ends"),
                    attendee_limit: row.get("attendee_limit"),
                    going,
                })
            })
            .collect();

        // Query for past meets belonging to the group, as well as their attendees.
        let rows = conn
            .query(
                "SELECT *, users.username as attendee_username
                FROM meets
                LEFT JOIN meets_attendees ON meets_attendees.meet_id = meets.id
                LEFT JOIN users ON users.id = meets_attendees.user_id
                WHERE meets.group_id = $1 AND meets.ends < $2::timestamptz AND meets.canceled IS NULL
                ORDER BY meets.starts DESC
                ",
                &[&id, &now],
            )
            .await
            .expect("Failed to load past meets for group");
        // Collect attendees per meet
        let mut attendees_map: HashMap<String, Vec<String>> = HashMap::new();
        for row in rows.iter() {
            let uuid: String = row.get("uuid");
            let attendee_username: Option<String> = row.get("attendee_username");
            match attendee_username {
                Some(username) => {
                    let attendees = attendees_map.entry(uuid.clone()).or_insert(vec![]);
                    attendees.push(username);
                }
                _ => {}
            };
        }
        let mut meets_seen: HashSet<String> = HashSet::new();
        let past: Vec<GroupMeetViewModel> = rows
            .iter()
            .filter_map(|row| {
                let uuid: String = row.get("uuid");
                if meets_seen.contains(&uuid) {
                    return None;
                }
                meets_seen.insert(uuid.clone());

                let going = attendees_map.get(&uuid).cloned().unwrap_or(vec![]);
                Some(GroupMeetViewModel {
                    uuid,
                    title: row.get("title"),
                    location_place_id: row.get("location_place_id"),
                    location_name: row.get("location_name"),
                    location_address: row.get("location_address"),
                    starts: row.get("starts"),
                    ends: row.get("ends"),
                    attendee_limit: row.get("attendee_limit"),
                    going,
                })
            })
            .collect();

        Some(GroupViewModel {
            path: String::from(path),
            name,
            added,
            avatar_url,
            description,
            members,
            upcoming,
            past,
            gitlab_group_path,
        })
    }

    /// Search for groups.
    pub async fn search_for_groups(&self, _: &str) -> Vec<GroupViewModel> {
        let mut conn = self
            .pool
            .get()
            .await
            .expect("Failed to obtain DB connection");
        let txn = conn
            .transaction()
            .await
            .expect("Failed to start transaction");

        // TODO: Use query string
        let rows = txn
            .query(
                "SELECT groups.*, users.username AS member_username, users.name AS member_name,
            users.avatar_url AS member_avatar_url, users.bio AS member_bio,
            users.email_address AS member_email_address, users_groups.membership
            FROM groups
            LEFT JOIN users_groups ON users_groups.group_id = groups.id
            LEFT JOIN users ON users.id = users_groups.user_id",
                &[],
            )
            .await
            .expect("Failed to query for groups");
        let mut groups: HashMap<String, GroupViewModel> = HashMap::new();
        for row in rows {
            let path: String = row.get("path");
            let name: String = row.get("name");
            let added: DateTime<Utc> = row.get("added");
            let avatar_url: Option<String> = row.get("avatar_url");
            let description: String = row.get("description");
            println!("Found group '{}'", path);
            let group = groups.entry(path.clone()).or_insert(GroupViewModel {
                path: path,
                name,
                added,
                avatar_url,
                description,
                members: vec![],
                upcoming: vec![],
                past: vec![],
                gitlab_group_path: None,
            });

            // If row has user info, add member
            let username_opt: Option<String> = row.get("member_username");
            if username_opt.is_none() {
                continue;
            }

            let username: String = username_opt.unwrap();
            let name: String = row.get("member_name");
            let avatar_url: Option<String> = row.get("member_avatar_url");
            let email_address: String = row.get("member_email_address");
            let bio: Option<String> = row.get("member_bio");
            let membership: GroupMembership = row.get("membership");
            group.members.push(GroupMemberViewModel {
                username,
                name,
                email_address: email_address.clone(),
                avatar_url: get_user_avatar_url(&avatar_url, &email_address),
                bio,
                membership,
            });
        }

        groups.drain().map(|(_, v)| v).collect()
    }

    /// Get group by path.
    pub async fn get_group_by_path(&self, path: &str) -> Option<Group> {
        let conn = self
            .pool
            .get()
            .await
            .expect("Failed to obtain DB connection");
        let rows = conn
            .query("SELECT * FROM groups WHERE path = $1", &[&path])
            .await
            .expect("Failed to load group");
        if rows.is_empty() {
            println!("Couldn't find group '{}'", path);
            return None;
        }
        let row = &rows[0];

        let path: String = row.get("path");
        println!("Found group '{}'", path);
        Some(Group {
            id: row.get("id"),
            path,
            name: row.get("name"),
            added: row.get("added"),
            avatar_url: row.get("avatar_url"),
            description: row.get("description"),
        })
    }

    /// Add a group.
    pub async fn add_group(&self, group: NewGroup, user: &UserViewModel) -> Group {
        let conn = self
            .pool
            .get()
            .await
            .expect("Failed to obtain DB connection");

        // TODO: Enclose in transaction
        let rows = conn
            .query(
                "INSERT INTO groups (path, name, added, avatar_url, description) VALUES
            ($1, $2, $3, $4, $5) RETURNING id
            ",
                &[
                    &group.path,
                    &group.name,
                    &group.added,
                    &group.avatar_url,
                    &group.description,
                ],
            )
            .await
            .expect("Failed to insert group");
        let id: i64 = rows[0].get("id");
        println!("Inserted group with ID {}, path {}\n", id, group.path);

        let now = Utc::now();
        conn.query(
            "INSERT INTO users_groups (group_id, user_id, added, membership) VALUES ($1, $2, $3,
            'admin')",
            &[&id, &user.id, &now],
        )
        .await
        .expect("Failed to add user as group member");

        Group {
            id,
            path: group.path,
            name: group.name,
            added: group.added,
            avatar_url: group.avatar_url,
            description: group.description,
        }
    }

    pub async fn edit_group(&self, path: String, payload: EditGroupPayload) -> GroupViewModel {
        let mut conn = self
            .pool
            .get()
            .await
            .expect("Failed to obtain DB connection");
        let txn = conn
            .transaction()
            .await
            .expect("Failed to start transaction");

        let rows = txn
            .query("SELECT * FROM groups where path = $1", &[&path])
            .await
            .expect("Failed to query group");
        if rows.is_empty() {
            panic!("Failed to find group with path '{}'", path);
        }

        let row = &rows[0];
        let added: DateTime<Utc> = row.get("added");
        let avatar_url: Option<String> = row.get("avatar_url");

        txn.query(
            "UPDATE groups SET name = $2, description = $3, gitlab_group_path = $4
            WHERE path = $1 RETURNING id
            ",
            &[
                &path,
                &payload.name,
                &payload.description,
                &payload.gitlab_group_path,
            ],
        )
        .await
        .expect("Failed to edit group");

        txn.commit().await.expect("Failed to commit transaction");
        println!("Successfully edited group '{}'", path);

        GroupViewModel {
            path,
            name: payload.name,
            description: payload.description,
            added,
            avatar_url,
            members: vec![],
            upcoming: vec![],
            past: vec![],
            gitlab_group_path: payload.gitlab_group_path,
        }
    }

    pub async fn delete_group(&self, path: String) {
        let conn = self
            .pool
            .get()
            .await
            .expect("Failed to obtain DB connection");

        conn.query("DELETE FROM groups WHERE path = $1", &[&path])
            .await
            .expect("Failed to delete group");
        println!("Successfully deleted group '{}'", path);
    }

    pub async fn add_meet(&self, payload: AddMeetPayload) -> Meet {
        let conn = self
            .pool
            .get()
            .await
            .expect("Failed to obtain DB connection");

        let group = self
            .get_group_by_path(&payload.group_path)
            .await
            .expect("Get group");
        let uuid = Uuid::new_v4().to_string();
        let ends = payload
            .ends
            .unwrap_or(payload.starts.add(Duration::hours(3)));
        let now = Utc::now();
        let empty_location = MeetLocation {
            place_id: String::new(),
            name: String::new(),
            address: String::new(),
        };
        let location = if !payload.is_online {
            // TODO: Convert error into HTTP response
            payload
                .location
                .as_ref()
                .expect("Location must be provided")
        } else {
            &empty_location
        };
        let rows = conn
            .query(
                "INSERT INTO meets (uuid, group_id, title, location_place_id, location_name,
            location_address, starts, ends, description, attendee_limit, added, is_online) VALUES
            ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12)
            RETURNING id
            ",
                &[
                    &uuid,
                    &group.id,
                    &payload.title,
                    &location.place_id,
                    &location.name,
                    &location.address,
                    &payload.starts,
                    &ends,
                    &payload.description,
                    &payload.attendee_limit,
                    &now,
                    &payload.is_online,
                ],
            )
            .await
            .expect("Failed to insert meet");
        let row = &rows[0];
        let meet_id: i64 = row.get("id");

        let now = Utc::now();

        for username in payload.hosts {
            let rows = conn
                .query("SELECT * FROM users WHERE username = $1", &[&username])
                .await
                .expect("Failed to load host");
            if rows.is_empty() {
                panic!("Couldn't find user '{}'", username);
            }
            let row = &rows[0];

            let user_id: i64 = row.get("id");
            conn.query(
                "INSERT INTO meets_hosts (meet_id, user_id, added) VALUES ($1, $2, $3)",
                &[&meet_id, &user_id, &now],
            )
            .await
            .expect("Failed to add meet host");
        }

        for name in payload.hosts_external {
            conn.query(
                "INSERT INTO meets_hosts_external (meet_id, name, added) VALUES ($1, $2, $3)",
                &[&meet_id, &name, &now],
            )
            .await
            .expect("Failed to add external meet host");
        }
        for name in payload.attendees_external {
            conn.query(
                "INSERT INTO meets_attendees_external (meet_id, name, added) VALUES ($1, $2, $3)",
                &[&meet_id, &name, &now],
            )
            .await
            .expect("Failed to add external meet attendee");
        }

        Meet {
            id: meet_id,
            uuid,
            title: payload.title,
            starts: payload.starts,
            ends,
            description: payload.description,
            attendee_limit: payload.attendee_limit,
            added: now,
            group_id: group.id,
            location: payload.location,
            is_online: payload.is_online,
        }
    }

    pub async fn edit_meet(
        &self,
        conf: &Conf,
        uuid: String,
        payload: EditMeetPayload,
    ) -> MeetViewModel {
        let mut conn = self
            .pool
            .get()
            .await
            .expect("Failed to obtain DB connection");

        let txn = conn
            .transaction()
            .await
            .expect("Failed to start transaction");
        let rows = txn
            .query(
                "SELECT meets.*, groups.path AS group_path, groups.name AS group_name
                FROM meets
                INNER JOIN groups ON groups.id = meets.group_id
                WHERE meets.uuid = $1",
                &[&uuid],
            )
            .await
            .expect("Failed to load meet");
        if rows.is_empty() {
            // TODO: Return 404
            panic!("Couldn't find meet '{}'", uuid);
        }
        let row = &rows[0];

        println!("Found meet '{}'", uuid);

        let meet_id: i64 = row.get("id");
        let group_path: String = row.get("group_path");
        let group_name: String = row.get("group_name");
        let added: DateTime<Utc> = row.get("added");
        let canceled: Option<DateTime<Utc>> = row.get("canceled");

        let empty_location = MeetLocation {
            place_id: String::new(),
            name: String::new(),
            address: String::new(),
        };
        let location = if !payload.is_online {
            // TODO: Convert error into HTTP response
            payload
                .location
                .as_ref()
                .expect("Location must be provided")
        } else {
            &empty_location
        };

        let ends = payload
            .ends
            .unwrap_or(payload.starts.add(Duration::hours(3)));
        txn.query(
            "UPDATE meets SET title = $2, location_place_id = $3, location_name = $4,
            location_address = $5, starts = $6, ends = $7, description = $8, attendee_limit = $9,
            is_online = $10
            WHERE uuid = $1
            RETURNING id
            ",
            &[
                &uuid,
                &payload.title,
                &location.place_id,
                &location.name,
                &location.address,
                &payload.starts,
                &ends,
                &payload.description,
                &payload.attendee_limit,
                &payload.is_online,
            ],
        )
        .await
        .expect("Failed to edit meet");

        txn.query("DELETE FROM meets_hosts WHERE meet_id = $1", &[&meet_id])
            .await
            .expect("Failed to prune meet hosts");

        let now = Utc::now();
        for username in payload.hosts {
            let rows = txn
                .query("SELECT * FROM users WHERE username = $1", &[&username])
                .await
                .expect(&format!("Failed to load host {}", username));
            if rows.is_empty() {
                panic!("Couldn't find user '{}'", username);
            }
            let user_id: i64 = rows[0].get("id");

            txn.query(
                "INSERT INTO meets_hosts (meet_id, user_id, added) VALUES ($1, $2, $3)",
                &[&meet_id, &user_id, &now],
            )
            .await
            .expect("Failed to add meet host");
        }

        // Query hosts
        let rows = txn
            .query(
                "SELECT users.* FROM meets_hosts
            INNER JOIN users ON users.id = meets_hosts.user_id
            WHERE meets_hosts.meet_id = $1",
                &[&meet_id],
            )
            .await
            .expect("Failed to query for hosts");
        let hosts: Vec<HostViewModel> = rows
            .iter()
            .map(|row| {
                let username: String = row.get("username");
                let name: String = row.get("name");
                let avatar_url: Option<String> = row.get("avatar_url");
                let email_address: String = row.get("email_address");
                let avatar_url = get_user_avatar_url(&avatar_url, &email_address);
                HostViewModel {
                    username,
                    email_address,
                    name,
                    avatar_url,
                }
            })
            .collect();

        // Query attendees
        let rows = txn
            .query(
                "SELECT users.* FROM meets_attendees
            INNER JOIN users ON users.id = meets_attendees.user_id
            WHERE meets_attendees.meet_id = $1",
                &[&meet_id],
            )
            .await
            .expect("Failed to query for attendees");
        let attendees: Vec<AttendeeViewModel> = rows
            .iter()
            .map(|row| {
                let username: String = row.get("username");
                let name: String = row.get("name");
                let avatar_url: Option<String> = row.get("avatar_url");
                let email_address: String = row.get("email_address");
                let avatar_url = get_user_avatar_url(&avatar_url, &email_address);
                AttendeeViewModel {
                    username,
                    email_address,
                    name,
                    avatar_url,
                }
            })
            .collect();

        txn.commit().await.expect("Failed to commit transaction");
        println!("Successfully edited meet {}", uuid);

        let location_vm = if !payload.is_online {
            let map_url = google::get_map_url(conf, &location.address);
            Some(LocationDetailsViewModel {
                place_id: location.place_id.clone(),
                name: location.name.clone(),
                address: location.address.clone(),
                map_url,
            })
        } else {
            None
        };

        MeetViewModel {
            uuid,
            title: payload.title,
            starts: payload.starts,
            ends,
            description: payload.description,
            hosts,
            attendee_limit: payload.attendee_limit,
            added,
            group_path,
            group_name,
            location: location_vm,
            attendees,
            external_attendees: vec![],
            external_hosts: vec![],
            canceled,
            is_online: payload.is_online,
        }
    }

    pub async fn delete_meet(&self, uuid: &str) -> Option<MeetViewModel> {
        let mut conn = self
            .pool
            .get()
            .await
            .expect("Failed to obtain DB connection");

        let txn = conn
            .transaction()
            .await
            .expect("Failed to start transaction");

        let rows = txn
            .query(
                "SELECT meets.*, groups.path AS group_path, groups.name AS group_name,
                organizers.username AS organizer_username, organizers.name AS organizer_name,
                organizers.email_address AS organizer_email,
                attendees.username AS attendee_username, attendees.name AS attendee_name,
                attendees.email_address AS attendee_email
                FROM meets
                LEFT JOIN meets_hosts ON meets_hosts.meet_id = meets.id
                LEFT JOIN users AS organizers ON organizers.id = meets_hosts.user_id
                LEFT JOIN meets_attendees ON meets_attendees.meet_id = meets.id
                LEFT JOIN users AS attendees ON attendees.id = meets_attendees.user_id
                INNER JOIN groups ON groups.id = meets.group_id
                WHERE meets.uuid = $1
                ",
                &[&uuid],
            )
            .await
            .expect("Failed to load meet");
        if rows.is_empty() {
            println!("Couldn't find meet '{}'", uuid);
            return None;
        }

        let row = &rows[0];
        let title: String = row.get("title");
        let starts: DateTime<Utc> = row.get("starts");
        let ends: DateTime<Utc> = row.get("ends");
        let description: String = row.get("description");
        let attendee_limit: Option<i32> = row.get("attendee_limit");
        let added: DateTime<Utc> = row.get("added");
        let group_path: String = row.get("group_path");
        let group_name: String = row.get("group_name");
        let location_name: String = row.get("location_name");
        let location_address: String = row.get("location_address");
        let location_place_id: String = row.get("location_place_id");
        let canceled: Option<DateTime<Utc>> = row.get("canceled");
        let is_online: bool = row.get("is_online");
        let location = if !is_online {
            Some(LocationDetailsViewModel {
                place_id: location_place_id,
                name: location_name,
                address: location_address,
                map_url: String::new(),
            })
        } else {
            None
        };
        let mut meet = MeetViewModel {
            uuid: String::from(uuid),
            title,
            starts,
            ends,
            description,
            attendee_limit,
            added,
            group_path,
            group_name,
            location,
            hosts: vec![],
            attendees: vec![],
            external_attendees: vec![],
            external_hosts: vec![],
            canceled,
            is_online,
        };

        for row in rows {
            let attendee_username: Option<String> = row.get("attendee_username");
            match attendee_username {
                Some(username) => {
                    meet.attendees.push(AttendeeViewModel {
                        username,
                        email_address: row.get("attendee_email"),
                        name: row.get("attendee_name"),
                        avatar_url: String::from(""),
                    });
                }
                _ => {}
            };
            let organizer_username: Option<String> = row.get("organizer_username");
            match organizer_username {
                Some(username) => {
                    meet.hosts.push(HostViewModel {
                        username,
                        email_address: row.get("organizer_email"),
                        name: row.get("organizer_name"),
                        avatar_url: String::from(""),
                    });
                }
                _ => {}
            };
        }

        txn.query(
            "DELETE FROM meets
                WHERE uuid = $1",
            &[&uuid],
        )
        .await
        .expect("Failed to delete meet");
        txn.query(
            "DELETE FROM meets_attendees USING meets
                WHERE meets_attendees.meet_id = meets.id AND meets.uuid = $1",
            &[&uuid],
        )
        .await
        .expect("Failed to delete meet attendees");
        txn.query(
            "DELETE FROM meets_hosts USING meets
                WHERE meets_hosts.meet_id = meets.id AND meets.uuid = $1",
            &[&uuid],
        )
        .await
        .expect("Failed to delete meet hosts");

        txn.commit().await.expect("Failed to commit transaction");
        println!("Successfully deleted meet {}", uuid);

        Some(meet)
    }

    /// Set whether a meet is canceled or not.
    pub async fn set_meet_canceled(&self, uuid: &str, canceled: bool) -> Option<MeetViewModel> {
        let mut conn = self
            .pool
            .get()
            .await
            .expect("Failed to obtain DB connection");

        let txn = conn
            .transaction()
            .await
            .expect("Failed to start transaction");

        let rows = txn
            .query(
                "SELECT meets.*, groups.path AS group_path, groups.name AS group_name,
                organizers.username AS organizer_username, organizers.name AS organizer_name,
                organizers.email_address AS organizer_email,
                attendees.username AS attendee_username, attendees.name AS attendee_name,
                attendees.email_address AS attendee_email
                FROM meets
                LEFT JOIN meets_hosts ON meets_hosts.meet_id = meets.id
                LEFT JOIN users AS organizers ON organizers.id = meets_hosts.user_id
                LEFT JOIN meets_attendees ON meets_attendees.meet_id = meets.id
                LEFT JOIN users AS attendees ON attendees.id = meets_attendees.user_id
                INNER JOIN groups ON groups.id = meets.group_id
                WHERE meets.uuid = $1
                ",
                &[&uuid],
            )
            .await
            .expect("Failed to load meet");
        if rows.is_empty() {
            println!("Couldn't find meet '{}'", uuid);
            return None;
        }

        let row = &rows[0];
        let title: String = row.get("title");
        let starts: DateTime<Utc> = row.get("starts");
        let ends: DateTime<Utc> = row.get("ends");
        let description: String = row.get("description");
        let attendee_limit: Option<i32> = row.get("attendee_limit");
        let added: DateTime<Utc> = row.get("added");
        let group_path: String = row.get("group_path");
        let group_name: String = row.get("group_name");
        let location_name: String = row.get("location_name");
        let location_address: String = row.get("location_address");
        let location_place_id: String = row.get("location_place_id");
        let orig_canceled: Option<DateTime<Utc>> = row.get("canceled");
        let canceled_time = if canceled {
            if orig_canceled.is_some() {
                println!("Not changing meet's canceled state, since it's already canceled");
                return None;
            }
            Some(Utc::now())
        } else {
            if orig_canceled.is_none() {
                println!("Not changing meet's canceled state, since it's already not canceled");
                return None;
            }
            None
        };
        let is_online: bool = row.get("is_online");
        let location = if !is_online {
            Some(LocationDetailsViewModel {
                place_id: location_place_id,
                name: location_name,
                address: location_address,
                map_url: String::new(),
            })
        } else {
            None
        };

        let mut meet = MeetViewModel {
            uuid: String::from(uuid),
            title,
            starts,
            ends,
            description,
            attendee_limit,
            added,
            group_path,
            group_name,
            location,
            hosts: vec![],
            attendees: vec![],
            external_attendees: vec![],
            external_hosts: vec![],
            canceled: canceled_time,
            is_online,
        };

        for row in rows {
            let attendee_username: Option<String> = row.get("attendee_username");
            match attendee_username {
                Some(username) => {
                    meet.attendees.push(AttendeeViewModel {
                        username,
                        email_address: row.get("attendee_email"),
                        name: row.get("attendee_name"),
                        avatar_url: String::from(""),
                    });
                }
                _ => {}
            };
            let organizer_username: Option<String> = row.get("organizer_username");
            match organizer_username {
                Some(username) => {
                    meet.hosts.push(HostViewModel {
                        username,
                        email_address: row.get("organizer_email"),
                        name: row.get("organizer_name"),
                        avatar_url: String::from(""),
                    });
                }
                _ => {}
            };
        }

        txn.query(
            "UPDATE meets SET canceled = $2 WHERE uuid = $1",
            &[&uuid, &canceled_time],
        )
        .await
        .expect("Failed to set 'canceled' value of meet");

        txn.commit().await.expect("Failed to commit transaction");
        if canceled {
            println!("Successfully canceled meet {}", uuid);
        } else {
            println!("Successfully un-canceled meet {}", uuid);
        }

        Some(meet)
    }

    pub async fn get_meet_by_uuid(&self, conf: &Conf, uuid: &str) -> Option<MeetViewModel> {
        let conn = self
            .pool
            .get()
            .await
            .expect("Failed to obtain DB connection");

        let rows = conn
            .query(
                "SELECT meets.*, groups.path as group_path, groups.name as group_name
                FROM meets
            INNER JOIN groups ON groups.id = meets.group_id
            WHERE meets.uuid = $1
            ",
                &[&uuid],
            )
            .await
            .expect("Failed to load meet");
        if rows.is_empty() {
            println!("Couldn't find meet '{}'", uuid);
            return None;
        }
        let row = &rows[0];

        println!("Found meet '{}'", uuid);

        let meet_id: i64 = row.get("id");
        let title: String = row.get("title");
        let starts: DateTime<Utc> = row.get("starts");
        let ends: DateTime<Utc> = row.get("ends");
        let description: String = row.get("description");
        let attendee_limit: Option<i32> = row.get("attendee_limit");
        let added: DateTime<Utc> = row.get("added");
        let group_path: String = row.get("group_path");
        let group_name: String = row.get("group_name");
        let location_name: String = row.get("location_name");
        let location_address: String = row.get("location_address");
        let location_place_id: String = row.get("location_place_id");
        let canceled: Option<DateTime<Utc>> = row.get("canceled");

        let location_map_url = google::get_map_url(conf, &location_address);

        let rows = conn
            .query(
                "SELECT users.* FROM meets
            INNER JOIN meets_attendees ON meets_attendees.meet_id = meets.id
            INNER JOIN users ON users.id = meets_attendees.user_id
            WHERE meets.id = $1
            ",
                &[&meet_id],
            )
            .await
            .expect("Failed to load attendees");
        let attendees: Vec<AttendeeViewModel> = rows
            .iter()
            .map(|row| {
                let avatar_url: Option<String> = row.get("avatar_url");
                let email_address: String = row.get("email_address");
                let avatar_url = get_user_avatar_url(&avatar_url, &email_address);
                AttendeeViewModel {
                    username: row.get("username"),
                    email_address,
                    name: row.get("name"),
                    avatar_url,
                }
            })
            .collect();

        let rows = conn
            .query(
                "SELECT users.* FROM meets
            INNER JOIN meets_hosts ON meets_hosts.meet_id = meets.id
            INNER JOIN users ON users.id = meets_hosts.user_id
            WHERE meets.id = $1
            ",
                &[&meet_id],
            )
            .await
            .expect("Failed to load meet hosts");
        let hosts: Vec<HostViewModel> = rows
            .iter()
            .map(|row| {
                let avatar_url: Option<String> = row.get("avatar_url");
                let email_address: String = row.get("email_address");
                let avatar_url = get_user_avatar_url(&avatar_url, &email_address);
                let username: String = row.get("username");
                let name: String = row.get("name");
                HostViewModel {
                    username,
                    email_address,
                    name,
                    avatar_url,
                }
            })
            .collect();

        let rows = conn
            .query(
                "SELECT meets_attendees_external.* FROM meets
            INNER JOIN meets_attendees_external ON meets_attendees_external.meet_id = meets.id
            WHERE meets.id = $1
            ",
                &[&meet_id],
            )
            .await
            .expect("Failed to load external attendees");
        let external_attendees: Vec<ExternalAttendeeViewModel> = rows
            .iter()
            .map(|row| ExternalAttendeeViewModel {
                name: row.get("name"),
            })
            .collect();

        let rows = conn
            .query(
                "SELECT meets_hosts_external.* FROM meets
            INNER JOIN meets_hosts_external ON meets_hosts_external.meet_id = meets.id
            WHERE meets.id = $1
            ",
                &[&meet_id],
            )
            .await
            .expect("Failed to load external meet hosts");
        let external_hosts: Vec<ExternalHostViewModel> = rows
            .iter()
            .map(|row| {
                let name: String = row.get("name");
                ExternalHostViewModel { name }
            })
            .collect();

        let is_online: bool = row.get("is_online");
        let location = if !is_online {
            Some(LocationDetailsViewModel {
                place_id: location_place_id,
                name: location_name,
                address: location_address,
                map_url: location_map_url,
            })
        } else {
            None
        };
        Some(MeetViewModel {
            uuid: String::from(uuid),
            title,
            starts,
            ends,
            description,
            hosts,
            attendee_limit,
            added,
            group_path,
            group_name,
            location,
            attendees,
            canceled,
            is_online,
            external_attendees,
            external_hosts,
        })
    }

    pub async fn search_for_meets(
        &self,
        conf: &Conf,
        username: Option<&String>,
    ) -> Vec<MeetViewModel> {
        let mut conn = self
            .pool
            .get()
            .await
            .expect("Failed to obtain DB connection");
        let txn = conn
            .transaction()
            .await
            .expect("Failed to start transaction");

        let now = Utc::now();

        let rows = match username {
            Some(username) => txn
                .query(
                    "SELECT meets.*, groups.path AS group_path, groups.name AS group_name
                        FROM users
                        INNER JOIN users_groups ON users_groups.user_id = users.id
                        INNER JOIN groups ON groups.id = users_groups.group_id
                        INNER JOIN meets ON meets.group_id = groups.id
                        WHERE users.username = $1 AND meets.canceled IS NULL AND
                        meets.ends >= $2::timestamptz
                        ORDER BY meets.starts
                        ",
                    &[&username, &now],
                )
                .await
                .expect("Failed to load meet"),
            None => txn
                .query(
                    "SELECT meets.*, groups.path AS group_path, groups.name AS group_name
                        FROM meets
                        INNER JOIN groups ON groups.id = meets.group_id
                        WHERE meets.canceled IS NULL AND meets.ends >= $1::timestamptz
                        ORDER BY meets.starts
                        ",
                    &[&now],
                )
                .await
                .expect("Failed to load meets"),
        };
        let results = rows
            .iter()
            .map(|row| map_row_to_view_model(conf, row, &txn));
        join_all(results).await
    }

    pub async fn add_meet_attendee(
        &self,
        uuid: &str,
        username: &str,
    ) -> (GroupViewModel, MeetViewModel, UserViewModel) {
        let mut conn = self
            .pool
            .get()
            .await
            .expect("Failed to obtain DB connection");
        let txn = conn
            .transaction()
            .await
            .expect("Failed to start transaction");

        let rows = txn
            .query(
                "SELECT meets.*, groups.path AS group_path
                FROM meets
                INNER JOIN groups ON groups.id = meets.group_id
                WHERE meets.uuid = $1 AND meets.canceled IS NULL",
                &[&uuid],
            )
            .await
            .expect("Failed to load meet");
        if rows.is_empty() {
            panic!("Couldn't find meet '{}'", uuid);
        }
        let row = &rows[0];
        let meet_id: i64 = row.get("id");
        let title: String = row.get("title");
        let starts: DateTime<Utc> = row.get("starts");
        let ends: DateTime<Utc> = row.get("ends");
        let description: String = row.get("description");
        let attendee_limit: Option<i32> = row.get("attendee_limit");
        let added: DateTime<Utc> = row.get("added");
        let location_name: String = row.get("location_name");
        let location_address: String = row.get("location_address");
        let location_place_id: String = row.get("location_place_id");
        let group_path: String = row.get("group_path");
        let canceled: Option<DateTime<Utc>> = row.get("canceled");
        let is_online: bool = row.get("is_online");
        let location = if !is_online {
            Some(LocationDetailsViewModel {
                place_id: location_place_id,
                name: location_name,
                address: location_address,
                map_url: String::new(),
            })
        } else {
            None
        };
        let meet = MeetViewModel {
            uuid: String::from(uuid),
            title,
            starts,
            ends,
            description,
            hosts: vec![],
            attendee_limit,
            added,
            group_path: group_path.clone(),
            group_name: String::from(""),
            location,
            attendees: vec![],
            external_attendees: vec![],
            external_hosts: vec![],
            canceled,
            is_online,
        };

        let rows = txn
            .query(
                "SELECT groups.*, users.username AS member_username, users.name AS member_name,
            users.bio AS member_bio, users.email_address as member_email,
            users_groups.membership AS membership
            FROM groups
            INNER JOIN users_groups ON users_groups.group_id = groups.id
            INNER JOIN users ON users.id = users_groups.user_id
            WHERE path = $1",
                &[&group_path],
            )
            .await
            .expect("Failed to load group");
        let row = &rows[0];
        let name: String = row.get("name");
        let added: DateTime<Utc> = row.get("added");
        let avatar_url: Option<String> = row.get("avatar_url");
        let description: String = row.get("description");
        let members = rows
            .iter()
            .map(|row| {
                let username: String = row.get("member_username");
                let name: String = row.get("member_name");
                let email_address: String = row.get("member_email");
                let bio: Option<String> = row.get("member_bio");
                let membership: GroupMembership = row.get("membership");
                GroupMemberViewModel {
                    username,
                    name,
                    email_address,
                    avatar_url: String::from(""),
                    bio,
                    membership,
                }
            })
            .collect();

        let group = GroupViewModel {
            path: group_path,
            name,
            added,
            avatar_url,
            description,
            members,
            upcoming: vec![],
            past: vec![],
            gitlab_group_path: None,
        };

        let rows = txn
            .query("SELECT * FROM users WHERE username = $1", &[&username])
            .await
            .expect("Failed to load user");
        if rows.is_empty() {
            panic!("Couldn't find user '{}'", username);
        }
        let row = &rows[0];
        let user_id: i64 = row.get("id");
        let username: String = row.get("username");
        let password_hash: String = row.get("password_hash");
        let name: String = row.get("name");
        let email_address: String = row.get("email_address");
        let added: DateTime<Utc> = row.get("added");
        let avatar_url: Option<String> = row.get("avatar_url");
        let bio: Option<String> = row.get("bio");
        let avatar_url = get_user_avatar_url(&avatar_url, &email_address);
        let user = UserViewModel {
            id: user_id,
            username,
            password_hash,
            name,
            email_address,
            added,
            avatar_url,
            bio,
            groups: vec![],
        };

        let now = Utc::now();

        txn.query(
            "INSERT INTO meets_attendees (meet_id, user_id, added) VALUES ($1, $2, $3)",
            &[&meet_id, &user_id, &now],
        )
        .await
        .expect("Failed to add meet attendee");

        txn.commit().await.expect("Failed to commit transaction");
        println!("Successfully registered meet attendance");

        (group, meet, user)
    }

    pub async fn remove_meet_attendee(
        &self,
        uuid: &str,
        username: &str,
    ) -> (GroupViewModel, MeetViewModel, UserViewModel) {
        let mut conn = self
            .pool
            .get()
            .await
            .expect("Failed to obtain DB connection");
        let txn = conn
            .transaction()
            .await
            .expect("Failed to start transaction");

        let rows = txn
            .query(
                "SELECT meets.*, groups.path AS group_path
                FROM meets
                INNER JOIN groups ON groups.id = meets.group_id
                WHERE uuid = $1",
                &[&uuid],
            )
            .await
            .expect("Failed to load meet");
        if rows.is_empty() {
            panic!("Couldn't find meet '{}'", uuid);
        }
        let row = &rows[0];
        let meet_id: i64 = row.get("id");
        let title: String = row.get("title");
        let starts: DateTime<Utc> = row.get("starts");
        let ends: DateTime<Utc> = row.get("ends");
        let description: String = row.get("description");
        let attendee_limit: Option<i32> = row.get("attendee_limit");
        let added: DateTime<Utc> = row.get("added");
        let location_name: String = row.get("location_name");
        let location_address: String = row.get("location_address");
        let location_place_id: String = row.get("location_place_id");
        let group_path: String = row.get("group_path");
        let canceled: Option<DateTime<Utc>> = row.get("canceled");
        let is_online: bool = row.get("is_online");
        let location = if !is_online {
            Some(LocationDetailsViewModel {
                place_id: location_place_id,
                name: location_name,
                address: location_address,
                map_url: String::new(),
            })
        } else {
            None
        };
        let meet = MeetViewModel {
            uuid: String::from(uuid),
            title,
            starts,
            ends,
            description,
            hosts: vec![],
            attendee_limit,
            added,
            group_path: group_path.clone(),
            group_name: String::from(""),
            location,
            is_online,
            attendees: vec![],
            external_attendees: vec![],
            external_hosts: vec![],
            canceled,
        };

        let rows = txn
            .query(
                "SELECT groups.*, users.username AS member_username, users.name AS member_name,
            users.bio AS member_bio, users.email_address as member_email,
            users_groups.membership AS membership
            FROM groups
            INNER JOIN users_groups ON users_groups.group_id = groups.id
            INNER JOIN users ON users.id = users_groups.user_id
            WHERE path = $1",
                &[&group_path],
            )
            .await
            .expect("Failed to load group");
        let row = &rows[0];
        let name: String = row.get("name");
        let added: DateTime<Utc> = row.get("added");
        let avatar_url: Option<String> = row.get("avatar_url");
        let description: String = row.get("description");
        let members = rows
            .iter()
            .map(|row| {
                let username: String = row.get("member_username");
                let name: String = row.get("member_name");
                let email_address: String = row.get("member_email");
                let bio: Option<String> = row.get("member_bio");
                let membership: GroupMembership = row.get("membership");
                GroupMemberViewModel {
                    username,
                    name,
                    email_address,
                    avatar_url: String::from(""),
                    bio,
                    membership,
                }
            })
            .collect();

        let group = GroupViewModel {
            path: group_path,
            name,
            added,
            avatar_url,
            description,
            members,
            upcoming: vec![],
            past: vec![],
            gitlab_group_path: None,
        };

        let rows = txn
            .query("SELECT * FROM users WHERE username = $1", &[&username])
            .await
            .expect("Failed to load user");
        if rows.is_empty() {
            panic!("Couldn't find user '{}'", username);
        }
        let row = &rows[0];
        let user_id: i64 = row.get("id");
        let username: String = row.get("username");
        let password_hash: String = row.get("password_hash");
        let name: String = row.get("name");
        let email_address: String = row.get("email_address");
        let added: DateTime<Utc> = row.get("added");
        let avatar_url: Option<String> = row.get("avatar_url");
        let bio: Option<String> = row.get("bio");
        let avatar_url = get_user_avatar_url(&avatar_url, &email_address);
        let user = UserViewModel {
            id: user_id,
            username,
            password_hash,
            name,
            email_address,
            added,
            avatar_url,
            bio,
            groups: vec![],
        };

        txn.query(
            "DELETE FROM meets_attendees WHERE meet_id = $1 and user_id = $2",
            &[&meet_id, &user_id],
        )
        .await
        .expect("Failed to de-register meet attendance");

        txn.commit().await.expect("Failed to commit transaction");
        println!("Successfully de-registered meet attendance");

        (group, meet, user)
    }

    pub async fn add_group_member(
        &self,
        path: &str,
        username: &str,
    ) -> (GroupViewModel, UserViewModel) {
        let mut conn = self
            .pool
            .get()
            .await
            .expect("Failed to obtain DB connection");
        let txn = conn
            .transaction()
            .await
            .expect("Failed to start transaction");

        let rows = txn
            .query(
                "SELECT groups.*, users.username AS member_username, users.name AS member_name,
            users.bio AS member_bio, users.email_address AS member_email,
            users_groups.membership AS membership
            FROM groups
            INNER JOIN users_groups ON users_groups.group_id = groups.id
            INNER JOIN users ON users.id = users_groups.user_id
            WHERE path = $1",
                &[&path],
            )
            .await
            .expect("Failed to load group");
        let row = &rows[0];
        let group_id: i64 = row.get("id");
        let name: String = row.get("name");
        let added: DateTime<Utc> = row.get("added");
        let avatar_url: Option<String> = row.get("avatar_url");
        let description: String = row.get("description");
        let members = rows
            .iter()
            .map(|row| {
                let username: String = row.get("member_username");
                let name: String = row.get("member_name");
                let email_address: String = row.get("member_email");
                let bio: Option<String> = row.get("member_bio");
                let membership: GroupMembership = row.get("membership");
                GroupMemberViewModel {
                    username,
                    name,
                    email_address,
                    avatar_url: String::from(""),
                    bio,
                    membership,
                }
            })
            .collect();

        let group = GroupViewModel {
            path: String::from(path),
            name,
            added,
            avatar_url,
            description,
            members,
            upcoming: vec![],
            past: vec![],
            gitlab_group_path: None,
        };

        let rows = txn
            .query("SELECT * FROM users WHERE username = $1", &[&username])
            .await
            .expect("Failed to load user");
        if rows.is_empty() {
            panic!("Couldn't find user '{}'", username);
        }
        let row = &rows[0];
        let user_id: i64 = row.get("id");
        let username: String = row.get("username");
        let password_hash: String = row.get("password_hash");
        let name: String = row.get("name");
        let email_address: String = row.get("email_address");
        let added: DateTime<Utc> = row.get("added");
        let avatar_url: Option<String> = row.get("avatar_url");
        let bio: Option<String> = row.get("bio");
        let avatar_url = get_user_avatar_url(&avatar_url, &email_address);
        let user = UserViewModel {
            id: user_id,
            username,
            password_hash,
            name,
            email_address,
            added,
            avatar_url,
            bio,
            groups: vec![],
        };

        let now = Utc::now();

        txn.query(
            "INSERT INTO users_groups (group_id, user_id, added, membership)
            VALUES ($1, $2, $3, 'member')",
            &[&group_id, &user_id, &now],
        )
        .await
        .expect("Failed to add group member");

        txn.commit().await.expect("Failed to commit transaction");
        println!("Successfully added group member");

        (group, user)
    }

    pub async fn remove_group_member(
        &self,
        path: &str,
        username: &str,
    ) -> (GroupViewModel, UserViewModel) {
        let mut conn = self
            .pool
            .get()
            .await
            .expect("Failed to obtain DB connection");
        let txn = conn
            .transaction()
            .await
            .expect("Failed to start transaction");

        let rows = txn
            .query(
                "SELECT groups.*, users.username AS member_username, users.name AS member_name,
            users.bio AS member_bio, users.email_address AS member_email,
            users_groups.membership AS membership
            FROM groups
            INNER JOIN users_groups ON users_groups.group_id = groups.id
            INNER JOIN users ON users.id = users_groups.user_id
            WHERE path = $1",
                &[&path],
            )
            .await
            .expect("Failed to load group");
        let row = &rows[0];
        let group_id: i64 = row.get("id");
        let name: String = row.get("name");
        let added: DateTime<Utc> = row.get("added");
        let avatar_url: Option<String> = row.get("avatar_url");
        let description: String = row.get("description");
        let members = rows
            .iter()
            .map(|row| {
                let username: String = row.get("member_username");
                let name: String = row.get("member_name");
                let email_address: String = row.get("member_email");
                let bio: Option<String> = row.get("member_bio");
                let membership: GroupMembership = row.get("membership");
                GroupMemberViewModel {
                    username,
                    name,
                    email_address,
                    avatar_url: String::from(""),
                    bio,
                    membership,
                }
            })
            .collect();

        let group = GroupViewModel {
            path: String::from(path),
            name,
            added,
            avatar_url,
            description,
            members,
            upcoming: vec![],
            past: vec![],
            gitlab_group_path: None,
        };

        let rows = txn
            .query("SELECT * FROM users WHERE username = $1", &[&username])
            .await
            .expect("Failed to load user");
        if rows.is_empty() {
            panic!("Couldn't find user '{}'", username);
        }
        let row = &rows[0];
        let user_id: i64 = row.get("id");
        let username: String = row.get("username");
        let password_hash: String = row.get("password_hash");
        let name: String = row.get("name");
        let email_address: String = row.get("email_address");
        let added: DateTime<Utc> = row.get("added");
        let avatar_url: Option<String> = row.get("avatar_url");
        let bio: Option<String> = row.get("bio");
        let avatar_url = get_user_avatar_url(&avatar_url, &email_address);
        let user = UserViewModel {
            id: user_id,
            username,
            password_hash,
            name,
            email_address,
            added,
            avatar_url,
            bio,
            groups: vec![],
        };

        txn.query(
            "DELETE FROM users_groups
            WHERE group_id = $1 AND user_id = $2",
            &[&group_id, &user_id],
        )
        .await
        .expect("Failed to remove group member");

        txn.commit().await.expect("Failed to commit transaction");
        println!("Successfully removed group member");

        (group, user)
    }
}

async fn map_row_to_view_model(
    conf: &Conf,
    row: &tokio_postgres::Row,
    txn: &tokio_postgres::Transaction<'_>,
) -> MeetViewModel {
    let meet_id: i64 = row.get("id");
    let uuid: String = row.get("uuid");
    let title: String = row.get("title");
    let starts: DateTime<Utc> = row.get("starts");
    let ends: DateTime<Utc> = row.get("ends");
    let description: String = row.get("description");
    let attendee_limit: Option<i32> = row.get("attendee_limit");
    let added: DateTime<Utc> = row.get("added");
    let group_path: String = row.get("group_path");
    let group_name: String = row.get("group_name");
    let location_name: String = row.get("location_name");
    let location_address: String = row.get("location_address");
    let location_place_id: String = row.get("location_place_id");
    let canceled: Option<DateTime<Utc>> = row.get("canceled");

    let location_map_url = google::get_map_url(conf, &location_address);

    let rows = txn
        .query(
            "SELECT users.*
            FROM meets
            INNER JOIN meets_attendees ON meets_attendees.meet_id = meets.id
            INNER JOIN users ON users.id = meets_attendees.user_id
            WHERE meets.id = $1
            ",
            &[&meet_id],
        )
        .await
        .expect("Failed to load attendees");
    let attendees: Vec<AttendeeViewModel> = rows
        .iter()
        .map(|row| {
            let avatar_url: Option<String> = row.get("avatar_url");
            let email_address: String = row.get("email_address");
            let avatar_url = get_user_avatar_url(&avatar_url, &email_address);
            AttendeeViewModel {
                username: row.get("username"),
                email_address,
                name: row.get("name"),
                avatar_url,
            }
        })
        .collect();

    let rows = txn
        .query(
            "SELECT users.* FROM meets
            INNER JOIN meets_hosts ON meets_hosts.meet_id = meets.id
            INNER JOIN users ON users.id = meets_hosts.user_id
            WHERE meets.id = $1
            ",
            &[&meet_id],
        )
        .await
        .expect("Failed to load meet hosts");
    let hosts: Vec<HostViewModel> = rows
        .iter()
        .map(|row| {
            let avatar_url: Option<String> = row.get("avatar_url");
            let email_address: String = row.get("email_address");
            let avatar_url = get_user_avatar_url(&avatar_url, &email_address);
            let username: String = row.get("username");
            let name: String = row.get("name");
            HostViewModel {
                username,
                email_address,
                name,
                avatar_url,
            }
        })
        .collect();
    let is_online: bool = row.get("is_online");
    let location = if !is_online {
        Some(LocationDetailsViewModel {
            place_id: location_place_id,
            name: location_name,
            address: location_address,
            map_url: location_map_url,
        })
    } else {
        None
    };
    MeetViewModel {
        uuid: String::from(uuid),
        title,
        starts,
        ends,
        description,
        hosts,
        attendee_limit,
        added,
        group_path,
        group_name,
        location,
        attendees,
        external_attendees: vec![],
        external_hosts: vec![],
        canceled,
        is_online,
    }
}
