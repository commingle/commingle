use futures::future;
use hyper::Body;
use paseto::v2::local::{decrypt_paseto, local_paseto};
use regex::Regex;
use serde::{Deserialize, Serialize};
use std::convert::Infallible;
use warp::filters::BoxedFilter;
use warp::http::header::SET_COOKIE;
use warp::reject;
use warp::{filters, reply, Filter};

use context::Context;
use database::Database;
use viewmodels::UserViewModel;

#[derive(Serialize, Deserialize)]
struct Session {
    // TODO: Convert to username
    id: i64,
}

/// Log user in by setting cookie.
pub fn log_user_in(user: &UserViewModel) -> Result<reply::Response, warp::Rejection> {
    // TODO: Use a configured password
    let mut key_mut = Vec::from("YELLOW SUBMARINE, BLACK WIZARDRY".as_bytes());
    let state = Session {
        id: user.id.clone() as i64,
    };
    let serialized_state = serde_json::to_string(&state).expect("Failed to serialize state");
    let payload =
        local_paseto(&serialized_state, None, &mut key_mut).expect("Failed to encrypt payload");
    let max_age = chrono::Duration::days(30).num_seconds();
    // TODO: If not on localhost, set to "Secure;"
    let secure = "";
    let cookie = format!(
        "sid={}; Max-Age={}; SameSite=Strict; HttpOnly; Path=/;{}",
        payload, max_age, secure
    );

    let user_json = serde_json::to_string(&user).expect("Failed to encode user");
    let resp = warp::http::Response::builder()
        .header(SET_COOKIE, cookie)
        .header("Content-Type", "application/json")
        .body(Body::from(user_json))
        .expect("Failed to build response");
    Ok(resp)
}

/// Log user out by unsetting cookie.
pub fn log_user_out() -> Result<reply::Response, warp::Rejection> {
    // TODO: If not on localhost, set to "Secure;"
    let secure = "";
    let cookie = format!(
        "sid=; Max-Age=0; SameSite=Strict; HttpOnly; Path=/;{}",
        secure
    );
    let resp = warp::http::Response::builder()
        .header(SET_COOKIE, cookie)
        .body(Body::from(""))
        .expect("Failed to build response");
    Ok(resp)
}

/// Filter for trying to get user credentials from request.
/// TODO: Clear invalid cookies. In case we have changed our format, we have to get rid of old,
/// stale, cookies
pub async fn try_auth(
    token: Option<String>,
    ctx: Context,
) -> Result<Option<UserViewModel>, Infallible> {
    let session: Option<Session> = token.and_then(move |token| {
        let mut key = Vec::from("YELLOW SUBMARINE, BLACK WIZARDRY".as_bytes());
        match decrypt_paseto(&token, None, &mut key) {
            Ok(json) => match serde_json::from_str::<Session>(&json) {
                Ok(state) => {
                    println!("Decoded session state: {}", state.id);
                    Some(state)
                }
                Err(err) => {
                    println!("Failed to decode state from auth cookie: {}", err);
                    None
                }
            },
            Err(err) => {
                println!("Failed to decrypt session token: {}", err);
                None
            }
        }
    });
    match session {
        None => Ok(None),
        Some(session) => {
            let user = ctx.database.get_user(session.id).await;
            Ok(user.clone())
        }
    }
}

async fn lookup_user(token: String, database: Database) -> Result<UserViewModel, Infallible> {
    println!("Obtained auth token {}", token);
    let mut key = Vec::from("YELLOW SUBMARINE, BLACK WIZARDRY".as_bytes());
    let view_model: UserViewModel = match decrypt_paseto(&token, None, &mut key) {
        Ok(json) => match serde_json::from_str::<Session>(&json) {
            Ok(state) => {
                println!("Decoded session state: {}", state.id);
                // TODO: Turn into rejection
                // TODO: Use UUID instead of sequential ID
                database
                    .get_user(state.id)
                    .await
                    .expect(&format!("Non-existent user '{}'", state.id))
            }
            Err(err) => {
                println!("Failed to decode state from auth cookie: {}", err);
                // TODO: Return rejection
                panic!("Invalid auth cookie");
            }
        },
        Err(err) => {
            println!("Failed to decrypt session token: {}", err);
            // TODO: Return rejection
            panic!("Invalid auth cookie");
        }
    };

    Ok(view_model)
}

/// Filter for getting user credentials from request.
/// TODO: Clear invalid cookies. In case we have changed our format, we have to get rid of old,
/// stale, cookies
pub fn auth(
    ctx: Context,
) -> impl Filter<Extract = (UserViewModel,), Error = warp::Rejection> + Clone {
    let re_header = Regex::new("^Bearer (.+)$").expect("Compile regex");
    let cookie_filter: BoxedFilter<(String,)> = filters::cookie::cookie("sid").boxed();
    let header_filter: BoxedFilter<(String,)> = warp::header("Authorization")
        .and_then(move |header: String| {
            let caps = match re_header.captures(&header) {
                Some(caps) => caps,
                None => {
                    // TODO: Corresponding rejection type
                    return future::ready(Err(reject::reject()));
                }
            };
            let token = match caps.get(1) {
                Some(cap) => String::from(cap.as_str()),
                None => {
                    // TODO: Corresponding rejection type
                    return future::ready(Err(reject::reject()));
                }
            };
            println!("Got bearer token '{}'", token);
            future::ready(Ok(token))
        })
        .boxed();
    cookie_filter
        .or(header_filter)
        .unify()
        .and_then(move |token: String| lookup_user(token, ctx.database.clone()))
}
