use chrono::Utc;
use futures::future::join_all;
use hyper::Body;
use regex::Regex;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use warp::filters::BoxedFilter;
use warp::http::header::{HeaderValue, CONTENT_TYPE};
use warp::http::status::StatusCode;
use warp::{Filter, Reply};

mod email;

use crate::email::send_email;
use auth::*;
use context::Context;
use database::*;
use viewmodels::*;

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Signup {
    pub username: String,
    pub name: String,
    pub email_address: String,
    pub password: String,
    pub bio: Option<String>,
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Login {
    pub username_or_email: String,
    pub password: String,
}

pub fn add_routes(
    filter: BoxedFilter<(impl Reply + 'static,)>,
    ctx: &Context,
) -> BoxedFilter<(impl Reply + 'static,)> {
    let json_body_limit = 1024 * 16;

    let ctx_clone = ctx.clone();
    let ctx_filter = warp::any().map(move || ctx_clone.clone());

    // TODO: Figure out why the request is re-routed to the homepage route after this executes.
    let api_not_found = warp::path("api")
        .and(warp::any())
        .and_then(any_api_not_found);
    let api_login = warp::post()
        .and(warp::path("api"))
        .and(warp::path("login"))
        .and(warp::path::end())
        .and(warp::body::content_length_limit(json_body_limit))
        .and(warp::body::json())
        .and(ctx_filter.clone())
        .and_then(post_api_login);
    let api_logout = warp::post()
        .and(warp::path("api"))
        .and(warp::path("logout"))
        .and(warp::path::end())
        .and_then(post_api_logout);
    let api_signup = warp::post()
        .and(warp::path("api"))
        .and(warp::path("accounts"))
        .and(warp::path::end())
        .and(warp::body::content_length_limit(json_body_limit))
        .and(warp::body::json())
        .and(ctx_filter.clone())
        .and_then(post_api_signup);
    let api_get_user = warp::get()
        .and(warp::path("api"))
        .and(warp::path("users"))
        .and(warp::path::param::<String>())
        .and(warp::path::end())
        .and(ctx_filter.clone())
        .and_then(get_api_user);
    let api_patch_user = warp::patch()
        .and(warp::path("api"))
        .and(warp::path("users"))
        .and(warp::path::param::<String>())
        .and(warp::path::end())
        .and(auth(ctx.clone()))
        .and(warp::body::content_length_limit(json_body_limit))
        .and(warp::body::json())
        .and(ctx_filter.clone())
        .and_then(patch_api_user);
    let api_put_user_password = warp::put()
        .and(warp::path("api"))
        .and(warp::path("users"))
        .and(warp::path::param::<String>())
        .and(warp::path("password"))
        .and(warp::path::end())
        .and(auth(ctx.clone()))
        .and(warp::body::content_length_limit(json_body_limit))
        .and(warp::body::json())
        .and(ctx_filter.clone())
        .and_then(put_api_user_password);
    let api_get_user_groups = warp::get()
        .and(warp::path("api"))
        .and(warp::path("users"))
        .and(warp::path::param::<String>())
        .and(warp::path("groups"))
        .and(warp::path::end())
        .and(ctx_filter.clone())
        .and_then(get_api_user_groups);
    let api_get_user_upcoming_meets = warp::get()
        .and(warp::path("api"))
        .and(warp::path("users"))
        .and(warp::path::param::<String>())
        .and(warp::path("upcoming-meets"))
        .and(warp::path::end())
        .and(ctx_filter.clone())
        .and_then(get_api_user_upcoming_meets);
    let api_get_user_past_meets = warp::get()
        .and(warp::path("api"))
        .and(warp::path("users"))
        .and(warp::path::param::<String>())
        .and(warp::path("past-meets"))
        .and(warp::path::end())
        .and(ctx_filter.clone())
        .and_then(get_api_user_past_meets);
    let api_get_groups = warp::get()
        .and(warp::path("api"))
        .and(warp::path("groups"))
        .and(warp::path::end())
        .and(warp::query::query::<HashMap<String, String>>())
        .and(ctx_filter.clone())
        .and_then(get_api_groups);
    let api_get_group = warp::get()
        .and(warp::path("api"))
        .and(warp::path("groups"))
        .and(warp::path::param::<String>())
        .and(warp::path::end())
        .and(ctx_filter.clone())
        .and_then(get_api_group);
    let api_post_group = warp::post()
        .and(warp::path("api"))
        .and(warp::path("groups"))
        .and(warp::path::end())
        .and(auth(ctx.clone()))
        .and(warp::body::content_length_limit(json_body_limit))
        .and(warp::body::json())
        .and(ctx_filter.clone())
        .and_then(post_api_group);
    let api_put_group = warp::put()
        .and(warp::path("api"))
        .and(warp::path("groups"))
        .and(warp::path::param::<String>())
        .and(warp::path::end())
        .and(auth(ctx.clone()))
        .and(warp::body::content_length_limit(json_body_limit))
        .and(warp::body::json())
        .and(ctx_filter.clone())
        .and_then(put_api_group);
    let api_delete_group = warp::delete()
        .and(warp::path("api"))
        .and(warp::path("groups"))
        .and(warp::path::param::<String>())
        .and(warp::path::end())
        .and(auth(ctx.clone()))
        .and(ctx_filter.clone())
        .and_then(delete_api_group);
    let api_get_place_predictions = warp::get()
        .and(warp::path("api"))
        .and(warp::path("place-predictions"))
        .and(warp::path::end())
        .and(warp::query::query::<HashMap<String, String>>())
        .and(ctx_filter.clone())
        .and_then(get_api_place_predictions);
    let api_get_place_details = warp::get()
        .and(warp::path("api"))
        .and(warp::path("place-details"))
        .and(warp::path::end())
        .and(warp::query::query::<HashMap<String, String>>())
        .and(ctx_filter.clone())
        .and_then(get_api_place_details);
    let api_get_meet = warp::get()
        .and(warp::path("api"))
        .and(warp::path("meets"))
        .and(warp::path::param::<String>())
        .and(warp::path::end())
        .and(ctx_filter.clone())
        .and_then(get_api_meet);
    let api_get_meets = warp::get()
        .and(warp::path("api"))
        .and(warp::path("meets"))
        .and(warp::path::end())
        .and(warp::query::query::<HashMap<String, String>>())
        .and(ctx_filter.clone())
        .and_then(get_api_meets);
    let api_post_meet = warp::post()
        .and(warp::path("api"))
        .and(warp::path("meets"))
        .and(warp::path::end())
        .and(auth(ctx.clone()))
        .and(warp::body::content_length_limit(json_body_limit))
        .and(warp::body::json())
        .and(ctx_filter.clone())
        .and_then(post_api_meet);
    let api_put_meet = warp::put()
        .and(warp::path("api"))
        .and(warp::path("meets"))
        .and(warp::path::param::<String>())
        .and(warp::path::end())
        .and(auth(ctx.clone()))
        .and(warp::body::content_length_limit(json_body_limit))
        .and(warp::body::json())
        .and(ctx_filter.clone())
        .and_then(put_api_meet);
    let api_put_meet_canceled = warp::put()
        .and(warp::path("api"))
        .and(warp::path("meets"))
        .and(warp::path::param::<String>())
        .and(warp::path("canceled"))
        .and(warp::path::end())
        .and(auth(ctx.clone()))
        .and(warp::body::content_length_limit(json_body_limit))
        .and(warp::body::json())
        .and(ctx_filter.clone())
        .and_then(put_api_meet_canceled);
    let api_delete_meet = warp::delete()
        .and(warp::path("api"))
        .and(warp::path("meets"))
        .and(warp::path::param::<String>())
        .and(warp::path::end())
        .and(auth(ctx.clone()))
        .and(ctx_filter.clone())
        .and_then(delete_api_meet);
    let api_post_meet_attendee = warp::post()
        .and(warp::path("api"))
        .and(warp::path("meets"))
        .and(warp::path::param::<String>())
        .and(warp::path("attendees"))
        .and(warp::path::end())
        .and(auth(ctx.clone()))
        .and(warp::body::content_length_limit(json_body_limit))
        .and(warp::body::json())
        .and(ctx_filter.clone())
        .and_then(post_api_meet_attendee);
    let api_delete_meet_attendee = warp::delete()
        .and(warp::path("api"))
        .and(warp::path("meets"))
        .and(warp::path::param::<String>())
        .and(warp::path("attendees"))
        .and(warp::path::param::<String>())
        .and(warp::path::end())
        .and(auth(ctx.clone()))
        .and(ctx_filter.clone())
        .and_then(delete_api_meet_attendee);
    let api_post_group_member = warp::post()
        .and(warp::path("api"))
        .and(warp::path("groups"))
        .and(warp::path::param::<String>())
        .and(warp::path("members"))
        .and(warp::path::end())
        .and(auth(ctx.clone()))
        .and(warp::body::content_length_limit(json_body_limit))
        .and(warp::body::json())
        .and(ctx_filter.clone())
        .and_then(post_api_group_member);
    let api_delete_group_member = warp::delete()
        .and(warp::path("api"))
        .and(warp::path("groups"))
        .and(warp::path::param::<String>())
        .and(warp::path("members"))
        .and(warp::path::param::<String>())
        .and(warp::path::end())
        .and(auth(ctx.clone()))
        .and(ctx_filter.clone())
        .and_then(delete_api_group_member);

    filter
        .or(api_login)
        .or(api_logout)
        .or(api_signup)
        .or(api_get_user)
        .or(api_patch_user)
        .or(api_put_user_password)
        .or(api_get_user_groups)
        .or(api_get_user_upcoming_meets)
        .or(api_get_user_past_meets)
        .or(api_get_group)
        .or(api_get_groups)
        .or(api_post_group)
        .or(api_put_group)
        .or(api_delete_group)
        .or(api_get_place_predictions)
        .or(api_get_place_details)
        .or(api_post_meet)
        .or(api_put_meet)
        .or(api_put_meet_canceled)
        .or(api_delete_meet)
        .or(api_get_meet)
        .or(api_get_meets)
        .or(api_post_meet_attendee)
        .or(api_delete_meet_attendee)
        .or(api_post_group_member)
        .or(api_delete_group_member)
        .or(api_not_found)
        .boxed()
}

pub async fn any_api_not_found() -> Result<::hyper::StatusCode, warp::Rejection> {
    println!("Received request for non-existent API route");
    Err(warp::reject::not_found())
}

pub async fn post_api_signup(
    signup: Signup,
    ctx: Context,
) -> Result<impl warp::Reply, warp::Rejection> {
    println!(
        "Received signup request, username: {}, email: {}",
        signup.username, signup.email_address,
    );
    let password_hash = bcrypt::hash(signup.password, bcrypt::DEFAULT_COST).expect("hash password");

    let user = NewUser::new(
        signup.username,
        signup.name,
        signup.email_address,
        password_hash,
        signup.bio,
    );
    let added_user = ctx.database.add_user(user).await;
    Ok(log_user_in(&added_user).expect("Failed to log user in"))
}

pub async fn post_api_login(
    login: Login,
    ctx: Context,
) -> Result<impl warp::Reply, warp::Rejection> {
    println!(
        "Received login request, username/email: {}",
        login.username_or_email
    );
    match ctx
        .database
        .get_user_by_username_or_email(&login.username_or_email)
        .await
    {
        Some(user) => {
            println!("Found user in database");
            if bcrypt::verify(&login.password, &user.password_hash)
                .expect("Failed to verify password")
            {
                println!("User logged in");
                Ok(log_user_in(&user).expect("Failed to log user in"))
            } else {
                println!("User not authenticated");
                let resp = hyper::http::Response::builder()
                    .status(StatusCode::UNAUTHORIZED)
                    .header(CONTENT_TYPE, HeaderValue::from_static("application/json"))
                    .body(Body::from(
                        r#"{"message": "Unrecognized username or password"}"#,
                    ))
                    .expect("Failed to build response");
                Ok(resp)
            }
        }
        None => {
            println!("Couldn't find user in database");
            let resp = hyper::http::Response::builder()
                .status(StatusCode::UNAUTHORIZED)
                .header(CONTENT_TYPE, HeaderValue::from_static("application/json"))
                .body(Body::from(
                    r#"{"message": "Unrecognized username or password"}"#,
                ))
                .expect("Failed to build response");
            Ok(resp)
        }
    }
}

pub async fn post_api_logout() -> Result<impl warp::Reply, warp::Rejection> {
    println!("Received logout request");
    log_user_out()
}

pub async fn get_api_user(
    username: String,
    ctx: Context,
) -> Result<impl warp::Reply, warp::Rejection> {
    println!("Received request to get user with username: {}", username);
    match ctx.database.get_user_by_username(&username).await {
        Some(user) => Ok(warp::reply::json(&user)),
        None => Err(warp::reject::not_found()),
    }
}

pub async fn patch_api_user(
    username: String,
    user: UserViewModel,
    payload: UserPatch,
    ctx: Context,
) -> Result<impl warp::Reply, warp::Rejection> {
    println!(
        "Received request to patch user with username: '{}'",
        username
    );
    if username != user.username {
        panic!("You can only update your own user data");
    }

    let user = ctx
        .database
        .patch_user(&username, payload)
        .await
        .expect("Failed to patch user");
    Ok(warp::reply::json(&user))
}

pub async fn put_api_user_password(
    username: String,
    user: UserViewModel,
    payload: PasswordUpdate,
    ctx: Context,
) -> Result<impl warp::Reply, warp::Rejection> {
    if username != user.username {
        panic!("You can only update your own password");
    }

    println!(
        "Received request to update password of user with username: '{}'",
        username
    );
    ctx.database
        .update_user_password(&username, payload)
        .await
        .expect("Failed to update user password");
    Ok(warp::reply())
}

/// Handler for API route to get groups of a user.
pub async fn get_api_user_groups(
    username: String,
    ctx: Context,
) -> Result<impl warp::Reply, warp::Rejection> {
    println!("Received request to get groups of user '{}'", username);
    let groups = ctx.database.get_user_groups(&username).await;
    println!("User has {} group(s)", groups.len());
    Ok(warp::reply::json(&groups))
}

/// Handler for API route to get upcoming meets of a user.
pub async fn get_api_user_upcoming_meets(
    username: String,
    ctx: Context,
) -> Result<impl warp::Reply, warp::Rejection> {
    println!(
        "Received request to get upcoming meets of user '{}'",
        username
    );
    let meets = ctx
        .database
        .get_user_upcoming_meets(&ctx.conf, &username)
        .await;
    println!("User has {} upcoming meet(s)", meets.len());
    Ok(warp::reply::json(&meets))
}

/// Handler for API route to get past meets of a user.
pub async fn get_api_user_past_meets(
    username: String,
    ctx: Context,
) -> Result<impl warp::Reply, warp::Rejection> {
    println!("Received request to get past meets of user '{}'", username);
    let meets = ctx.database.get_user_past_meets(&ctx.conf, &username).await;
    println!("User has {} past meet(s)", meets.len());
    Ok(warp::reply::json(&meets))
}

/// Handler for API route to search for groups.
pub async fn get_api_groups(
    query_params: HashMap<String, String>,
    ctx: Context,
) -> Result<impl warp::Reply, warp::Rejection> {
    let dflt = &String::from("");
    let query = query_params.get("query").unwrap_or(&dflt);
    println!("Received request to search for groups, query '{}'", query);
    let groups = ctx.database.search_for_groups(query).await;
    Ok(warp::reply::json(&groups))
}

/// Handler for API route to get group.
pub async fn get_api_group(
    path: String,
    ctx: Context,
) -> Result<impl warp::Reply, warp::Rejection> {
    println!("Received request to get group '{}'", path);
    match ctx.database.get_group_view_model_by_path(&path).await {
        Some(group) => Ok(warp::reply::json(&group)),
        None => Err(warp::reject::not_found()),
    }
}

/// A payload sent with a request to add a group.
#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct GroupPayload {
    pub path: String,
    pub name: String,
    pub description: String,
    pub avatar_url: Option<String>,
}

/// Handler for API route to add a group.
pub async fn post_api_group(
    user: UserViewModel,
    payload: GroupPayload,
    ctx: Context,
) -> Result<impl warp::Reply, warp::Rejection> {
    println!("Received request to add a group: {:?}", payload);
    let re_path = Regex::new("^[a-z0-9]+((-?[a-z0-9])*[a-z0-9])?$").expect("Compile regex");
    if !re_path.is_match(&payload.path) {
        panic!("Path is on invalid format: '{}'", payload.path);
    }
    if payload.path.len() > 255 {
        panic!("Path cannot be longer than 255 characters")
    }

    let new_group = NewGroup {
        path: payload.path,
        name: payload.name,
        description: payload.description,
        avatar_url: payload.avatar_url,
        added: Utc::now(),
    };
    let group = ctx.database.add_group(new_group, &user).await;
    Ok(warp::reply::json(&group))
}

/// Handler for API route to edit a group.
pub async fn put_api_group(
    path: String,
    _: UserViewModel,
    payload: EditGroupPayload,
    ctx: Context,
) -> Result<impl warp::Reply, warp::Rejection> {
    println!("Received request to edit group {}: {:?}", path, payload);
    let re_path = Regex::new("^[a-z0-9]+((-?[a-z0-9])*[a-z0-9])?$").expect("Compile regex");
    if !re_path.is_match(&path) {
        panic!("Path is on invalid format: '{}'", path);
    }
    if path.len() > 255 {
        panic!("Path cannot be longer than 255 characters")
    }

    let group = ctx.database.edit_group(path, payload).await;
    Ok(warp::reply::json(&group))
}

/// Handler for API route to delete a group.
pub async fn delete_api_group(
    path: String,
    _: UserViewModel,
    ctx: Context,
) -> Result<impl warp::Reply, warp::Rejection> {
    println!("Received request to delete group {}", path);
    let re_path = Regex::new("^[a-z0-9]+((-?[a-z0-9])*[a-z0-9])?$").expect("Compile regex");
    if !re_path.is_match(&path) {
        panic!("Path is on invalid format: '{}'", path);
    }
    if path.len() > 255 {
        panic!("Path cannot be longer than 255 characters")
    }

    let group = ctx.database.delete_group(path).await;
    Ok(warp::reply::json(&group))
}

#[derive(Debug, Serialize, Deserialize)]
struct StructuredFormatting {
    pub main_text: String,
    pub secondary_text: String,
}

#[derive(Debug, Serialize, Deserialize)]
struct PlacePrediction {
    description: String,
    place_id: String,
    structured_formatting: StructuredFormatting,
}

#[derive(Debug, Serialize, Deserialize)]
struct PlacePredictionResult {
    pub predictions: Vec<PlacePrediction>,
}

#[derive(Debug, Serialize, Deserialize)]
struct PlaceDetails {
    pub formatted_address: String,
    pub name: String,
}

#[derive(Debug, Serialize, Deserialize)]
struct PlaceDetailsResult {
    pub result: PlaceDetails,
}

// Handler for API route to get place predictions.
pub async fn get_api_place_predictions(
    query_params: HashMap<String, String>,
    ctx: Context,
) -> Result<impl warp::Reply, warp::Rejection> {
    // TODO: Translate into rejection.
    let query = query_params
        .get("query")
        .expect("Missing query parameter 'query'");
    println!(
        "Received request to get Google Maps place predictions: '{}'",
        query
    );
    let url: reqwest::Url = reqwest::Url::parse_with_params(
        format!("{}/maps/api/place/autocomplete/json", ctx.conf.maps_api_url).as_str(),
        &[("key", &ctx.conf.maps_api_key), ("input", &query)],
    )
    .expect("Failed to parse URL");
    let predictions: Vec<PlacePrediction> = match reqwest::get(url)
        .await
        // TODO: Translate into rejection
        .expect("Failed to get Google Maps location predictions")
        .json::<PlacePredictionResult>()
        .await
    {
        Ok(rslt) => rslt.predictions,
        Err(_) => vec![],
    };
    println!(
        "Successfully got {} Google Maps place prediction(s)",
        predictions.len(),
    );
    Ok(warp::reply::json(&predictions))
}

/// Handler for API route to get place details/signed Google Maps static API URL corresponding
/// to a certain place ID/address description.
pub async fn get_api_place_details(
    query_params: HashMap<String, String>,
    ctx: Context,
) -> Result<impl warp::Reply, warp::Rejection> {
    // TODO: Turn into rejection
    let place_id = query_params
        .get("placeId")
        .expect("Query parameter 'placeId' missing");
    println!(
        "Received request to get Google Maps image URI - placeId: {}",
        place_id
    );

    let client = reqwest::Client::new();
    let details: PlaceDetails = client
        .get(format!("{}/maps/api/place/details/json", ctx.conf.maps_api_url).as_str())
        .query(&[
            ("key", &ctx.conf.maps_api_key),
            ("place_id", place_id),
            ("fields", &String::from("formatted_address,name")),
        ])
        .send()
        .await
        .expect("Failed to get Google Maps place details")
        .json::<PlaceDetailsResult>()
        .await
        // TODO: Translate into rejection
        .expect("Failed to deserialize response from Google Maps")
        .result;

    let map_url = google::get_map_url(&ctx.conf, &details.formatted_address);

    let view_model = LocationDetailsViewModel {
        place_id: place_id.clone(),
        address: details.formatted_address,
        name: details.name,
        map_url,
    };
    Ok(warp::reply::json(&view_model))
}

/// Handler for API route to add a meet.
pub async fn post_api_meet(
    _: UserViewModel,
    payload: AddMeetPayload,
    ctx: Context,
) -> Result<impl warp::Reply, warp::Rejection> {
    println!("Received request to add a meet: {:?}", payload);
    let meet = ctx.database.add_meet(payload).await;
    Ok(warp::reply::json(&meet))
}

/// Handler for API route to get a meet.
pub async fn get_api_meet(uuid: String, ctx: Context) -> Result<impl warp::Reply, warp::Rejection> {
    println!("Received request to get meet '{}'", uuid);
    match ctx.database.get_meet_by_uuid(&ctx.conf, &uuid).await {
        Some(meet) => Ok(warp::reply::json(&meet)),
        None => Err(warp::reject::not_found()),
    }
}

/// Handler for API route to search for meets.
pub async fn get_api_meets(
    query_params: HashMap<String, String>,
    ctx: Context,
) -> Result<impl warp::Reply, warp::Rejection> {
    let username: Option<&String> = query_params.get("username");
    println!("Received request to search for meets");
    let meets = ctx.database.search_for_meets(&ctx.conf, username).await;
    Ok(warp::reply::json(&meets))
}

/// Handler for API route to edit a meet.
pub async fn put_api_meet(
    uuid: String,
    _: UserViewModel,
    payload: EditMeetPayload,
    ctx: Context,
) -> Result<impl warp::Reply, warp::Rejection> {
    println!("Received request to edit meet {}: {:?}", uuid, payload);
    let meet = ctx.database.edit_meet(&ctx.conf, uuid, payload).await;

    let mut futures = vec![];

    // Send email to meet organizers
    for organizer in &meet.hosts {
        println!(
            "Sending email to meet organizer ({}) about meet '{}' having been updated",
            organizer.email_address, meet.title
        );
        futures.push(send_email(
            &ctx,
            format!("Meet updated: {}", meet.title),
            format!(
                "<p>The meet <em>{title}</em> has been updated.</p>",
                title = meet.title
            ),
            organizer.email_address.clone(),
            organizer.name.clone(),
        ));
    }

    // Send email to meet attendees
    for attendee in &meet.attendees {
        println!(
            "Sending email to meet attendee ({}) about meet '{}' having been updated",
            attendee.email_address, meet.title
        );
        futures.push(send_email(
            &ctx,
            format!("Meet updated: {}", meet.title),
            format!(
                "<p>The meet <em>{title}</em> has been updated.</p>",
                title = meet.title
            ),
            attendee.email_address.clone(),
            attendee.name.clone(),
        ));
    }

    for result in join_all(futures).await {
        result.expect("Failed to send emails");
    }

    Ok(warp::reply::json(&meet))
}

/// A payload sent with a request to (un-)cancel a meet.
#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct CancelMeetPayload {
    pub canceled: bool,
}

/// Handler for API route to update whether a meet is canceled.
pub async fn put_api_meet_canceled(
    uuid: String,
    user: UserViewModel,
    payload: CancelMeetPayload,
    ctx: Context,
) -> Result<impl warp::Reply, warp::Rejection> {
    println!(
        "Received request to update whether meet is canceled: {} (canceled: {})",
        uuid, payload.canceled
    );
    match ctx
        .database
        .set_meet_canceled(&uuid, payload.canceled)
        .await
    {
        Some(meet) => {
            let mut futures = vec![];

            for organizer in meet.hosts {
                println!(
                    "Sending email to organizer ({}) about canceling meet '{}'",
                    organizer.email_address, meet.title
                );
                futures.push(send_email(
                    &ctx,
                    format!("Meet canceled: {}", meet.title),
                    format!(
                        r#"<p>
                          The meet <em>{title}</em> has been canceled by <a href="https://commingle.xyz/u/{username}">{name}</a>.
                        </p>"#,
                        title = meet.title,
                        name = user.name,
                        username = user.username,
                    ),
                    organizer.email_address.clone(),
                    organizer.name.clone(),
                ));
            }

            for attendee in meet.attendees {
                println!(
                    "Sending email to attendee ({}) about canceling meet '{}'",
                    attendee.email_address, meet.title
                );
                futures.push(send_email(
                    &ctx,
                    format!("Meet canceled: {}", meet.title),
                    format!(
                        r#"<p>
                          The meet <em>{title}</em> has been canceled.
                        </p>"#,
                        title = meet.title
                    ),
                    attendee.email_address.clone(),
                    attendee.name.clone(),
                ));
            }

            for result in join_all(futures).await {
                result.expect("Failed to send emails");
            }
        }
        _ => {}
    }
    Ok(warp::reply())
}

/// Handler for API route to delete a meet.
pub async fn delete_api_meet(
    uuid: String,
    _: UserViewModel,
    ctx: Context,
) -> Result<impl warp::Reply, warp::Rejection> {
    println!("Received request to delete meet {}", uuid);
    match ctx.database.delete_meet(&uuid).await {
        Some(meet) => {
            let mut futures = vec![];

            for organizer in meet.hosts {
                println!(
                    "Sending email to organizer ({}) about deleting meet '{}'",
                    organizer.email_address, meet.title
                );
                futures.push(send_email(
                    &ctx,
                    format!("Meet deleted: {}", meet.title),
                    format!(
                        "<p>The meet <em>{title}</em> has been deleted.</p>",
                        title = meet.title
                    ),
                    organizer.email_address.clone(),
                    organizer.name.clone(),
                ));
            }

            for attendee in meet.attendees {
                println!(
                    "Sending email to attendee ({}) about deleting meet '{}'",
                    attendee.email_address, meet.title
                );
                futures.push(send_email(
                    &ctx,
                    format!("Meet deleted: {}", meet.title),
                    format!(
                        "<p>The meet <em>{title}</em> has been deleted.</p>",
                        title = meet.title
                    ),
                    attendee.email_address.clone(),
                    attendee.name.clone(),
                ));
            }

            for result in join_all(futures).await {
                result.expect("Failed to send emails");
            }
        }
        _ => {}
    };

    Ok(warp::reply())
}

/// A payload sent with a request to add a meet attendee.
#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AttendeePayload {}

/// Handler for API route to add an attendee to a meet.
pub async fn post_api_meet_attendee(
    uuid: String,
    session: UserViewModel,
    _: AttendeePayload,
    ctx: Context,
) -> Result<impl warp::Reply, warp::Rejection> {
    println!(
        "Received request to add attendee '{}' to meet '{}'",
        session.username, uuid
    );

    let (group, meet, user) = ctx
        .database
        .add_meet_attendee(&uuid, &session.username)
        .await;

    let mut futures = vec![];

    println!(
        "Sending email to user ({}) about registering their attendance at '{}'",
        user.email_address, meet.title
    );
    futures.push(send_email(
        &ctx,
        format!("Signed up for meet: {}", meet.title),
        format!(
            "<p>You've signed up for the meet <em>{title}</em>. See you there!</p>",
            title = meet.title
        ),
        user.email_address.clone(),
        user.name.clone(),
    ));

    for member in group.members {
        if member.membership != GroupMembership::Admin {
            continue;
        }

        println!(
            "Sending email to admin ({}) about registering a new attendant to '{}'",
            member.email_address, meet.title
        );
        futures.push(send_email(
            &ctx,
            format!("A user has signed up for your meet: {}", meet.title),
            format!(
                "<p>The user <em>{user}</em> has signed up for your meet <em>{title}</em>.</p>",
                user = user.name,
                title = meet.title
            ),
            member.email_address.clone(),
            member.name.clone(),
        ));
    }

    for result in join_all(futures).await {
        result.expect("Failed to send emails");
    }

    Ok(warp::reply())
}

/// Handler for API route to remove an attendee from a meet.
pub async fn delete_api_meet_attendee(
    uuid: String,
    username: String,
    session: UserViewModel,
    ctx: Context,
) -> Result<impl warp::Reply, warp::Rejection> {
    println!(
        "Received request to remove attendee '{}' from meet '{}'",
        username, uuid
    );
    if username != session.username {
        // TODO: Turn into rejection
        panic!("Logged in user differs from attendee");
    }

    let (group, meet, user) = ctx.database.remove_meet_attendee(&uuid, &username).await;

    let mut futures = vec![];

    println!(
        "Sending email to user ({}) about rescinding their attendance at '{}'",
        user.email_address, meet.title
    );
    // TODO: Render styled email HTML with Maud
    futures.push(send_email(
        &ctx,
        format!("Signup rescinded for meet: {}", meet.title),
        format!(
            "<p>You've rescinded your signup for the meet <em>{title}</em>.</p>",
            title = meet.title,
        ),
        user.email_address.clone(),
        user.name.clone(),
    ));

    for member in group.members {
        if member.membership != GroupMembership::Admin {
            continue;
        }

        println!(
            "Sending email to admin ({}) about an attendee having rescinded their signup to '{}'",
            member.email_address, meet.title
        );
        futures.push(send_email(
            &ctx,
            format!(
                "A user has rescinded their signup for your meet: {}",
                meet.title
            ),
            format!(
                "<p>The user <em>{user}</em> has rescinded their signup for your meet <em>{title}</em>.</p>",
                user = user.name,
                title = meet.title
            ),
            member.email_address.clone(),
            member.name.clone(),
        ));
    }

    for result in join_all(futures).await {
        result.expect("Failed to send email");
    }

    Ok(warp::reply())
}

/// A payload sent with a request to add a group member.
#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AddMemberPayload {
    username: String,
}

/// Handler for API route to add a group member.
pub async fn post_api_group_member(
    uuid: String,
    session: UserViewModel,
    payload: AddMemberPayload,
    ctx: Context,
) -> Result<impl warp::Reply, warp::Rejection> {
    println!(
        "Received request to add member '{}' to group '{}'",
        payload.username, uuid
    );
    if payload.username != session.username {
        panic!("Joining a group on another's behalf is not allowed");
    }

    let (group, user) = ctx
        .database
        .add_group_member(&uuid, &session.username)
        .await;

    let mut futures = vec![];

    println!(
        "Sending email to user ({}) about their joining '{}'",
        user.email_address, group.path
    );
    futures.push(send_email(
        &ctx,
        format!("Joined group: {}", group.name),
        format!(
            "<p>You're now a member of the group <em>{name}</em>.</p>",
            name = group.name
        ),
        user.email_address.clone(),
        user.name.clone(),
    ));

    for member in group.members {
        if member.membership != GroupMembership::Admin {
            continue;
        }

        println!(
            "Sending email to admin ({}) about registering a new group memberr to '{}'",
            member.email_address, group.name
        );
        futures.push(send_email(
            &ctx,
            format!("A new user has joined your group: {}", group.name),
            format!(
                "<p>The user <em>{user}</em> joined your group <em>{name}</em>.</p>",
                user = user.name,
                name = group.name
            ),
            member.email_address.clone(),
            member.name.clone(),
        ));
    }

    for result in join_all(futures).await {
        result.expect("Failed to send emails");
    }

    Ok(warp::reply())
}

/// Handler for API route to remove a group member.
pub async fn delete_api_group_member(
    uuid: String,
    username: String,
    session: UserViewModel,
    ctx: Context,
) -> Result<impl warp::Reply, warp::Rejection> {
    println!(
        "Received request to remove member '{}' from group '{}'",
        username, uuid
    );
    if username != session.username {
        panic!("Leaving a group on another's behalf is not allowed")
    }

    let (group, user) = ctx.database.remove_group_member(&uuid, &username).await;

    let mut futures = vec![];

    println!(
        "Sending email to user ({}) about their leaving group '{}'",
        user.email_address, group.path
    );
    futures.push(send_email(
        &ctx,
        format!("Left group: {}", group.name),
        format!(
            "<p>You've left the group <em>{name}</em>.</p>",
            name = group.name
        ),
        user.email_address.clone(),
        user.name.clone(),
    ));

    for member in group.members {
        if member.membership != GroupMembership::Admin {
            continue;
        }

        println!(
            "Sending email to admin ({}) about losing a group member '{}'",
            member.email_address, group.name
        );
        futures.push(send_email(
            &ctx,
            format!("A user has left your group: {}", group.name),
            format!(
                "<p>The user <em>{user}</em> has left your group <em>{name}</em>.</p>",
                user = user.name,
                name = group.name
            ),
            member.email_address.clone(),
            member.name.clone(),
        ));
    }

    for result in join_all(futures).await {
        result.expect("Failed to send emails");
    }

    Ok(warp::reply())
}
