use anyhow::Result;
use serde_json::json;

use context::Context;

/// Send an email to a certain recipient.
pub async fn send_email(
    ctx: &Context,
    subject: String,
    message: String,
    to_email: String,
    to_name: String,
) -> Result<()> {
    let req_body = json!({
        "key": &ctx.conf.mandrill_api_key,
        "message": {
            "subject": subject,
            "html": message,
            // TODO: Change to commingle.xyz once the domain is verified with Mandrill
            "from_email": "contact@experimental.berlin",
            "from_name": "Commingle",
            "to": [
                {
                    "email": to_email,
                    "name": to_name,
                    "type": "to",
                }
            ],
            "headers": {
                "Reply-To": "<no-reply>@commingle.xyz",
            },
        },
    });
    let client = reqwest::Client::new();
    client
        .post(&ctx.conf.mandrill_api_url)
        .body(req_body.to_string())
        .send()
        .await?
        .error_for_status()?;

    Ok(())
}
