// Conf holds app configuration.
#[derive(Clone)]
pub struct Conf {
    pub maps_api_url: String,
    pub maps_api_key: String,
    pub maps_signing_key: Vec<u8>,
    pub mandrill_api_url: String,
    pub mandrill_api_key: String,
}
