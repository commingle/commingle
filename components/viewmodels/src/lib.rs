use chrono::{DateTime, Utc};
use postgres_types::FromSql;
use serde::{Deserialize, Serialize};

#[derive(Debug, FromSql, Serialize, Deserialize, PartialEq, Clone)]
#[postgres(name = "group_membership")]
pub enum GroupMembership {
    #[postgres(name = "member")]
    Member,
    #[postgres(name = "admin")]
    Admin,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct GroupMemberViewModel {
    pub username: String,
    pub name: String,
    pub email_address: String,
    pub avatar_url: String,
    pub bio: Option<String>,
    pub membership: GroupMembership,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct GroupViewModel {
    pub path: String,
    pub name: String,
    pub added: DateTime<Utc>,
    pub avatar_url: Option<String>,
    pub description: String,
    pub members: Vec<GroupMemberViewModel>,
    pub upcoming: Vec<GroupMeetViewModel>,
    pub past: Vec<GroupMeetViewModel>,
    pub gitlab_group_path: Option<String>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct GroupMeetViewModel {
    pub uuid: String,
    pub title: String,
    pub location_place_id: String,
    pub location_name: String,
    pub location_address: String,
    pub starts: DateTime<Utc>,
    pub ends: DateTime<Utc>,
    pub attendee_limit: Option<i32>,
    pub going: Vec<String>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AttendeeViewModel {
    pub username: String,
    pub email_address: String,
    pub name: String,
    pub avatar_url: String,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct HostViewModel {
    pub username: String,
    pub email_address: String,
    pub name: String,
    pub avatar_url: String,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ExternalAttendeeViewModel {
    pub name: String,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ExternalHostViewModel {
    pub name: String,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct MeetViewModel {
    pub uuid: String,
    pub title: String,
    pub starts: DateTime<Utc>,
    pub ends: DateTime<Utc>,
    pub description: String,
    pub hosts: Vec<HostViewModel>,
    pub attendee_limit: Option<i32>,
    pub added: DateTime<Utc>,
    pub group_path: String,
    pub group_name: String,
    pub location: Option<LocationDetailsViewModel>,
    pub attendees: Vec<AttendeeViewModel>,
    pub external_attendees: Vec<ExternalAttendeeViewModel>,
    pub external_hosts: Vec<ExternalHostViewModel>,
    pub canceled: Option<DateTime<Utc>>,
    pub is_online: bool,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct LocationDetailsViewModel {
    pub place_id: String,
    pub name: String,
    pub address: String,
    pub map_url: String,
}

#[derive(Debug, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct GroupMembershipViewModel {
    pub group_path: String,
    pub membership: GroupMembership,
}

#[derive(Debug, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct UserViewModel {
    #[serde(skip_serializing)]
    pub id: i64,
    pub username: String,
    #[serde(skip_serializing)]
    pub password_hash: String,
    pub name: String,
    pub email_address: String,
    pub added: DateTime<Utc>,
    pub avatar_url: String,
    pub bio: Option<String>,
    pub groups: Vec<GroupMembershipViewModel>,
}
