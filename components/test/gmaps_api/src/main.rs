#![feature(async_closure)]

use clap::App;
use serde::Serialize;
use std::collections::HashMap;
use std::env;
use warp::Filter;

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let version = env::var("CARGO_PKG_VERSION").expect("Failed to get $CARGO_PKG_VERSION");
    App::new("Commingle")
        .version(version.as_str())
        .about("Fake Google Maps API server")
        .get_matches();

    let api_get_predictions = warp::get()
        .and(warp::path("maps"))
        .and(warp::path("api"))
        .and(warp::path("place"))
        .and(warp::path("autocomplete"))
        .and(warp::path("json"))
        .and(warp::path::end())
        .and(warp::query::query::<HashMap<String, String>>())
        .and_then(get_place_predictions);

    let api_get_details = warp::get()
        .and(warp::path("maps"))
        .and(warp::path("api"))
        .and(warp::path("place"))
        .and(warp::path("details"))
        .and(warp::path("json"))
        .and(warp::path::end())
        .and(warp::query::query::<HashMap<String, String>>())
        .and_then(get_place_details);

    let routes = api_get_predictions.or(api_get_details);

    let (addr, fut) = warp::serve(routes).bind_ephemeral(([127, 0, 0, 1], 0));
    let port = addr.port();
    println!("* Listening on port {}", port);
    fut.await;

    Ok(())
}

#[derive(Debug, Serialize)]
struct StructuredFormatting {
    pub main_text: String,
    pub secondary_text: String,
}

#[derive(Debug, Serialize)]
struct PlacePrediction {
    description: String,
    place_id: String,
    structured_formatting: StructuredFormatting,
}

#[derive(Debug, Serialize)]
struct PlacePredictionResult {
    pub predictions: Vec<PlacePrediction>,
}

async fn get_place_predictions(
    query_params: HashMap<String, String>,
) -> Result<impl warp::Reply, warp::Rejection> {
    // TODO: Translate into rejection.
    let query = query_params
        .get("input")
        .expect("Missing query parameter 'input'");
    println!("Getting place predictions - query: '{}'", query);
    let rslt = PlacePredictionResult {
        predictions: vec![PlacePrediction {
            description: String::from("Test"),
            place_id: String::from("test"),
            structured_formatting: StructuredFormatting {
                main_text: String::from("Test"),
                secondary_text: String::from("Test, Test Street, Test City, Test Country"),
            },
        }],
    };
    Ok(warp::reply::json(&rslt))
}

#[derive(Debug, Serialize)]
struct PlaceDetails {
    pub formatted_address: String,
    pub name: String,
}

#[derive(Debug, Serialize)]
struct PlaceDetailsResult {
    pub result: PlaceDetails,
}

async fn get_place_details(
    query_params: HashMap<String, String>,
) -> Result<impl warp::Reply, warp::Rejection> {
    // TODO: Translate into rejection.
    let place_id = query_params
        .get("place_id")
        .expect("Missing query parameter 'place_id'");
    let fields = query_params
        .get("fields")
        .expect("Missing query parameter 'fields'");
    println!(
        "Getting place details - place_id: '{}', fields: '{}'",
        place_id, fields
    );
    let rslt = PlaceDetailsResult {
        result: PlaceDetails {
            formatted_address: String::from("Test Street, Test City, Test Country"),
            name: String::from("Test"),
        },
    };
    Ok(warp::reply::json(&rslt))
}
