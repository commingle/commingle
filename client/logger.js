const pino = require('pino')

const appConfig = JSON.parse(document.getElementById('app-config').getAttribute('data-json')) ||
  {
    logLevel: 'debug',
  }

const logger = pino({
  name: 'root',
  level: appConfig.logLevel,
})

module.exports = logger
