const h = require('@arve.knudsen/hyperscript')
const concat = require('ramda/src/concat')
const sheetify = require('sheetify')

const logger = require('./logger').child({module: 'insertErrorNotificationPane',})
const sheet = sheetify('./insertErrorNotificationPane.styl')

module.exports = (title, children, closeCallback) => {
  if (!Array.isArray(children)) {
    children = [children,]
  }
  const pane = h(`.${sheet}`, [
    h('#error-notification-pane', concat(concat([h('strong', title), ' ',], children), [
      h('span#close-error-notification-pane', {
        onclick: () => {
          logger.debug(`Closing error notification pane`)
          document.body.removeChild(pane)
          if (closeCallback != null) {
            closeCallback()
          }
        },
      }, '✖'),
    ])),
  ])
  window.document.body.insertBefore(pane, window.document.body.firstChild)
}
