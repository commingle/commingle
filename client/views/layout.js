const h = require('@arve.knudsen/hyperscript')
const sheetify = require('sheetify')
const t = require('tcomb')

const logger = require('../logger').child({module: 'views/layout',})
const Menu = require('./menu')
const Footer = require('./footer')

sheetify('./layout.styl')

const link2MenuAttrs = {
  '/': {
    name: 'home',
  },
  '/groups': {
    name: 'groups',
  },
}

module.exports = (viewElement, state, emit) => {
  t.Object(viewElement, ['viewElement',])
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])

  logger.debug(`Rendering`)

  logger.debug(`User is logged in: ${state.loggedInUser != null}`)
  const pathname = `${!state.href.startsWith('/') ? '/' : ''}${state.href}`
  return h('#app-container', [
    Menu(state, emit, link2MenuAttrs, pathname),
    h('main#content.bg-white', [
      viewElement,
    ]),
    Footer(state),
  ])
}
