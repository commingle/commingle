const h = require('@arve.knudsen/hyperscript')
const sheetify = require('sheetify')
const t = require('tcomb')
const {isNullOrBlank,} = require('@arve.knudsen/stringutils')
const assert = require('assert')
const includes = require('ramda/src/includes')
const bowser = require('bowser')
const {DateTime,} = require('luxon')
const mergeRight = require('ramda/src/mergeRight')
const debounce = require('debounce')
const isEmpty = require('ramda/src/isEmpty')
const addIndex = require('ramda/src/addIndex')
const map = require('ramda/src/map')
const partial = require('ramda/src/partial')
const trim = require('ramda/src/trim')
const filter = require('ramda/src/filter')
const reject = require('ramda/src/reject')
const propEq = require('ramda/src/propEq')
const any = require('ramda/src/any')

const insertErrorNotificationPane = require('../insertErrorNotificationPane')
const {markdownEditorWidget,} = require('../markdownEditorWidget')
const modalComponent = require('../components/modal')
const loadingComponent = require('../components/loading')(['newMeetView', 'loading',])
const dateTimePickerWidget = require('../dateTimePickerWidget')
const focusedElement = require('../focusedElement')
const createIconElem = require('../createIconElem')
const ajax = require('../ajax')
const logger = require('../logger').child({module: 'views/editMeet',})
const {getDateTimeStr,} = require('../addAndEditMeetUtils')
const {isGroupAdmin,} = require('../groups')

const sheet = sheetify('./editMeet.styl')

const checkForm = debounce((state, emit) => {
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])
  logger.debug(`Checking whether to enable submission...`)
  /*

  const markdownElem = document.getElementById('wmd-input-markdown')
  assert.notEqual(markdownElem, null, `No element with ID wmd-input-markdown`)
  const inputName2Data = pipe(
    toPairs,
    map(([key, value,]) => {
      if (value == null) {
        value = ''
      }
      t.String(value, ['state', 'input', key,])
      return [key, trim(value),]
    }),
    fromPairs
  )(state.input)
  inputName2Data.description = markdownElem.value
  */

  /*
  logger.debug(`Storing add meet form locally:`, cachedData)
  window.localStorage.setItem('addMeetData', JSON.stringify(cachedData))
  */

  /*
  const notRequired = [
    'endDate', 'endTime',
  ]
  logger.debug(`Validating form input and determining whether to enable submission`,
    inputName2Data)
  state.isSubmissionEnabled = all(([name, data,]) => {
    t.String(name, ['inputName2Data',])
    t.String(data, ['inputName2Data', name,])
    return includes(name, notRequired) || !isNullOrBlank(data)
  }, toPairs(inputName2Data))

  if (isEmpty(state.hosts)) {
    state.isSubmissionEnabled = false
  }
  if (state.location == null) {
    state.isSubmissionEnabled = false
  }
  if (state.showAttendeeLimit && state.attendeeLimit == null) {
    state.isSubmissionEnabled = false
  }

  logger.debug(`Have markdown: ${!isNullOrBlank(inputName2Data.description)}`)
  logger.debug(`Enabled submission: ${state.isSubmissionEnabled}`, {
    state,
  })
  */
  emit.triggerRender()
}, 300)

const resetState = (state) => {
  t.Object(state, ['state',])
  const {meet,} = state
  t.Object(meet, ['state', 'meet',])

  const starts = DateTime.fromISO(meet.starts)
  const ends = DateTime.fromISO(meet.ends)
  state.input = {
    title: meet.title,
    startsDate: !bowser.safari ? starts.toFormat('yyyy-MM-dd') : starts.toFormat('dd/MM/yyyy'),
    startsTime: starts.toFormat('HH:mm'),
    endsDate: !bowser.safari ? ends.toFormat('yyyy-MM-dd') : ends.toFormat('dd/MM/yyyy'),
    endsTime: ends.toFormat('HH:mm'),
    description: meet.description,
    locationQuery: '',
    hostQuery: '',
    attendeeLimit: meet.attendeeLimit,
  }
  state.shouldShowAttendeeLimit = meet.attendeeLimit != null
  state.hosts = meet.hosts
  state.filteredHosts = []
  state.locationPredictions = []
  state.location = !meet.isOnline ? {
    name: meet.location.name,
    address: meet.location.address,
    mapImageUrl: meet.location.mapUrl,
    placeId: meet.location.placeId,
  } : null
  state.isOnline = meet.isOnline
  state.isSubmissionEnabled = true
  state.isCancelEnabled = true
  state.errorMessages = {}
  state.shouldShowCancelConfirmationDialog = false
}

const renderInput = (name, placeholder, state, emit, opts={}) => {
  t.String(name, ['name',])
  t.String(placeholder, ['placeholder',])
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])
  const focus = opts.focus
  delete opts.focus

  const inputValue = state.input[name]
  t.String(inputValue, ['state', 'input', name,])
  const errorMsg = state.errorMessages[name]
  const renderer = focus ? focusedElement : h
  return [
    renderer(`input#input-${name.toLowerCase()}.edit-meet-input`, mergeRight({
      placeholder,
      value: inputValue,
      required: true,
      oninput: (event) => {
        state.input[name] = event.target.value
        checkForm(state, emit)
      },
    }, opts)),
    errorMsg != null ? h('.error-message', errorMsg) : null,
  ]
}

const filterLocations = debounce((state, emit, query) => {
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])
  t.String(query, ['query',])

  if (isNullOrBlank(query)) {
    state.locationPredictions = []
    emit.triggerRender()
    return
  }

  logger.debug(`Querying for location: '${query}'...`)
  ajax.getJson(`/api/place-predictions?query=${query}`).then(async (predictions) => {
    logger.debug(`Received location predictions:`, predictions)
    if (state.input.locationQuery === query) {
      state.locationPredictions = predictions
      emit.triggerRender()
    } else {
      logger.debug(`Ignoring received location predictions since the query changed in the meantime`)
    }
  })
}, 300)

const renderLocationMap = (state, emit) => {
  t.Object(state, ['state',])
  t.Object(state.location, ['state', 'location',])
  t.Function(emit, ['emit',])
  logger.debug({location: state.location,}, `Rendering map`)
  const mapUri = encodeURI(`https://google.com/maps/search/?api=1&query=${
      state.location.description}&query_place_id=${state.location.placeId}`)
  return h(`a#meet-location-map`, {
    href: mapUri,
    target: `_blank`,
    rel: `noreferrer noopener`,
  }, h(`img`, {src: state.location.mapImageUrl,}))
}

const renderLocation = (state, emit) => {
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])

  if (state.isLoadingLocation) {
    return loadingComponent.render(state, emit)
  }

  return h(`#location-group.typeahead-group`, [
    h(`#edit-is-online`, [
      h('label', 'Online'),
      h(`input`, {
        type: 'checkbox',
        checked: state.isOnline,
        onchange: (event) => {
          const isOnline = event.target.checked
          logger.debug(`Meet is online: ${isOnline}`)
          state.isOnline = isOnline
          checkForm(state, emit)
        },
      }),
    ]),
    !state.isOnline ? renderLocationEditor(state, emit) : null,
  ])
}

const renderLocationEditor = (state, emit) => {
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])
  if (state.location != null) {
    logger.debug({location: state.location,}, `A location is selected, rendering it`)
    return h(`#meet-location`, [
      h(`#meet-location-address-container`, [
        h(`#meet-location-meta`, [
          h(`#meet-location-name`, state.location.name),
          h(`#meet-location-address`, state.location.address),
        ]),
        h(`button#meet-location-change-button`, {
          onclick: (event) => {
            logger.debug(`Button to change location was clicked`)
            event.preventDefault()

            state.location = null
            checkForm(state, emit)
          },
        }, `Change`),
      ]),
      renderLocationMap(state, emit),
    ])
  }

  const shouldShowDropdown = !isEmpty(state.locationPredictions) &&
    state.shouldShowLocationSelectionDropdown
  return [
    h(`input#input-location.edit-meet-input`, {
      placeholder: `Location`,
      required: true,
      value: state.input.locationQuery,
      onblur: () => {
        logger.debug(`Location query input was blurred`)
        logger.debug(`Hiding location query dropdown`)
        state.hoveredLocationIndex = null
        state.shouldShowLocationSelectionDropdown = false
        emit.triggerRender()
      },
      onfocus: () => {
        logger.debug(`Location query input was focused`)
        logger.debug(`Showing location query dropdown`)
        state.hoveredLocationIndex = null
        state.shouldShowLocationSelectionDropdown = true
        emit.triggerRender()
      },
      oninput: (event) => {
        const query = event.target.value
        state.input.locationQuery = query
        filterLocations(state, emit, query)
      },
    }),
    h(`ul.typeahead-dropdown${!shouldShowDropdown ? '.hidden': ''}`, addIndex(map)((prediction, i) => {
      const suffix = i === state.hoveredLocationIndex ? '.active' : ''
      return h(`li${suffix}`, {
        onmouseenter: () => {
          logger.debug(`Entering dropdown item ${i}`)
          state.hoveredLocationIndex = i
          emit.triggerRender()
        },
        onmouseleave: () => {
          logger.debug(`Exiting dropdown item ${i}`)
          state.hoveredLocationIndex = null
          emit.triggerRender()
        },
        onmousedown: () => {
          logger.debug({prediction,}, `Dropdown item ${i} was clicked`)
          state.hoveredLocationIndex = null
          state.shouldShowLocationSelectionDropdown = false
          state.locationPredictions = []
          state.input.locationQuery = ''
          checkForm(state, emit)

          logger.debug(`Getting map URL from server...`)
          state.isLoadingLocation = true
          emit.triggerRender()
          ajax.getJson(
            `/api/place-details?description=${prediction.description}&placeId=${prediction.place_id}`)
            .then(async (details) => {
              logger.debug({details,}, `Received place details`)
              if (!state.isLoadingLocation) {
                logger.debug(`Ignoring received place details, due to stale query`)
                return
              }

              state.location = {
                placeId: prediction.place_id,
                description: prediction.description,
                name: details.name,
                address: details.address,
                mapImageUrl: details.mapUrl,
              }
              state.isLoadingLocation = false
              emit.triggerRender()
            })
        },
      }, prediction.description)
    }, state.locationPredictions || [])),
  ]
}

let descriptionEditorWidget
const renderDescriptionEditor = (state, emit) => {
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])
  if (descriptionEditorWidget == null) {
    descriptionEditorWidget = markdownEditorWidget(state.input.description)
  }
  return h('fieldset#edit-meet-description-fieldset', [
    h('legend', 'Description'),
    h('#description-editor', [
      descriptionEditorWidget(state, emit, partial(checkForm, [state, emit,])),
    ]),
  ])
}

const filterHostSelection = debounce((state, emit, query) => {
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])
  t.String(query, ['query',])
  const {meet, group,} = state
  t.Object(meet, ['state', 'meet',])
  t.Object(group, ['state', 'group',])
  query = query.toLowerCase()
  logger.debug({group: group,}, `Filtering host selection: '${query}'`)
  let hosts
  if (isNullOrBlank(query)) {
    hosts = []
  } else {
    hosts = filter((member) => {
      return (includes(query, member.username.toLowerCase()) || includes(query,
        member.name.toLowerCase())) && !any(propEq('username', member.username), state.hosts)
    }, group.members)
  }
  logger.debug({hosts,}, `Got host selection`)
  state.filteredHosts = hosts
  emit.triggerRender()
}, 300)

const renderHosts = (state, emit) => {
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])
  const shouldShowDropdown = !isEmpty(state.filteredHosts) &&
    state.shouldShowHostSelectionDropdown
  return h('fieldset#edit-meet-hosts-fieldset', [
    h('legend', 'Hosts'),
    h(`.typeahead-group`, [
      h(`input#host-input.edit-meet-input`, {
        placeholder: `Add a host`,
        value: state.input.hostQuery,
        onblur: () => {
          logger.debug(`Host selection input was blurred, hiding host selection dropdown`)
          state.hoveredHostIndex = null
          state.shouldShowHostSelectionDropdown = false
          emit.triggerRender()
        },
        onfocus: () => {
          logger.debug(`Host selection input was focused, showing host selection dropdown`)
          state.hoveredHostIndex = null
          state.shouldShowHostSelectionDropdown = true
          emit.triggerRender()
        },
        oninput: (event) => {
          state.input.hostQuery = event.target.value
          const query = trim(event.target.value)
          filterHostSelection(state, emit, query)
        },
      }),
      h(`ul.typeahead-dropdown${!shouldShowDropdown ? '.hidden': ''}`, addIndex(map)((host, i) => {
        const suffix = i === state.hoveredHostIndex ? '.active' : ''
        return h(`li${suffix}`, {
          onmouseenter: () => {
            logger.debug(`Entering dropdown item ${i}`)
            state.hoveredHostIndex = i
            emit.triggerRender()
          },
          onmouseleave: () => {
            logger.debug(`Exiting dropdown item ${i}`)
            state.hoveredHostIndex = null
            emit.triggerRender()
          },
          onmousedown: () => {
            logger.debug(`Dropdown item ${i} was clicked`)
            const host = state.filteredHosts[i]
            const hostName = host.name
            logger.debug(`The selected host is '${hostName}'`)
            const selectedHostId = host.username
            assert.ok(!isNullOrBlank(selectedHostId))
            state.hoveredHostIndex = null
            state.shouldShowHostSelectionDropdown = false
            state.filteredHosts = []
            state.selectedHostId = selectedHostId
            state.input.hostQuery = ''
            state.hosts.push(host)
            checkForm(state, emit)
          },
        }, host.name)
      }, state.filteredHosts)),
    ]),
    !isEmpty(state.hosts) ? h(`ul#hosts`, map((host) => {
      return h(`li.host`, [
        h(`.remove-host`, {
          onclick: () => {
            logger.debug(`Removing host '${host.name}'`)
            state.hosts = reject(propEq('username', host.username), state.hosts)
            checkForm(state, emit)
            logger.debug(`Triggered re-render`)
          },
        }, createIconElem('user-cancel2')),
        h(`img.host-avatar`, {src: host.avatarUrl,}),
        host.name,
      ])
    }, state.hosts)) : null,
  ])
}

const renderRightColumn = (state, emit) => {
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])
  const {shouldShowAttendeeLimit,} = state

  return h(`#right-column`, [
    h(`#edit-meet-pad`, [
      h(`h1`, `Edit Meet`),
      h(`form#edit-meet-form`, [
        renderInput('title', 'Title', state, emit, {
          focus: true,
        }),
        renderLocation(state, emit),
        h(`#edit-meet-start`, [
          h('label', 'Starts'),
          dateTimePickerWidget('input-start', (type, value) => {
            if (type === 'date') {
              logger.debug(`New input value for startsDate: ${value}`)
              state.input.startsDate = value
            } else {
              logger.debug(`New input value for startsTime: ${value}`)
              state.input.startsTime = value
            }
            checkForm(state, emit)
          }, {
            required: true, classes: ['edit-meet-input',],
            dateValue: state.input.startsDate, timeValue: state.input.startsTime,
          }),
        ]),
        h(`#edit-meet-end`, [
          h('label', 'Ends'),
          dateTimePickerWidget('input-end', (type, value) => {
            if (type === 'date') {
              logger.debug(`New input value for endsDate: ${value}`)
              state.input.endsDate = value
            } else {
              logger.debug(`New input value for endsTime: ${value}`)
              state.input.endsTime = value
            }
            checkForm(state, emit)
          }, {
            required: true, classes: ['edit-meet-input',],
            dateValue: state.input.endsDate, timeValue: state.input.endsTime,
          }),
        ]),
        renderDescriptionEditor(state, emit),
        renderHosts(state, emit),
        h(`fieldset#edit-meet-optional-settings-fieldset`, [
          h(`legend`, `Optional Settings`),
          h(`.optional-setting-container`, [
            h(`.optional-setting-heading`, [
              h(`input#input-attendee-limit.optional-setting-checkbox`, {
                type: 'checkbox',
                name: 'attendee-limit',
                checked: state.shouldShowAttendeeLimit,
                onchange: (event) => {
                  const isEnabled = event.target.checked
                  logger.debug(`Enabling showing of attendee limit setting: ${isEnabled}`)
                  state.shouldShowAttendeeLimit = isEnabled
                  if (!isEnabled) {
                    state.input.attendeeLimit = null
                  }
                  checkForm(state, emit)
                },
              }),
              h(`label#label-attendee-limit`,
                `Attendee Limit`),
            ]),
            h(`input.optional-setting${shouldShowAttendeeLimit ? '' : '.hidden'}`, {
              type: `number`,
              min: 1,
              max: 9999,
              value: state.input.attendeeLimit,
              oninput: (event) => {
                const limit = !isNullOrBlank(event.target.value) ? parseInt(event.target.value, 10) : null
                assert.ok(!isNaN(limit))
                logger.debug(`Attendee limit changed to ${limit}`)
                state.input.attendeeLimit = limit
                checkForm(state, emit)
              },
              placeholder: `Attendee limit`,
            }),
          ]),
        ]),
      ]),
      h(`hr`),
      h(`#form-buttons`, [
        h(`button#submit-edit-meet-button`, {
          type: 'submit',
          disabled: !state.isSubmissionEnabled,
          onclick: (event) => {
            event.preventDefault()
            logger.debug(`Submit button clicked`)
            submitEditMeet(state, emit)
          },
        }, `Save`),
        h(`button#cancel-edit-meet-button`, {
          disabled: !state.isCancelEnabled,
          onclick: (event) => {
            event.preventDefault()
            logger.debug(`Cancel button clicked`)
            state.shouldShowCancelConfirmationDialog = true
            emit.triggerRender()
          },
        }, `Cancel`),
      ]),
    ]),
  ])
}

module.exports = {
  name: 'editMeetView',
  route: '/m/:uuid/edit',
  loadData: async (state, emit) => {
    t.Object(state, ['state',])
    t.Function(emit, ['emit',])
    const {uuid,} = state.params
    assert.ok(!isNullOrBlank(uuid))
    logger.debug(`Loading meet ${uuid}...`)
    const meet = await ajax.getJson(`/api/meets/${uuid}`)
    state.meet = meet
    logger.debug(`Successfully loaded meet '${uuid}', loading its group...`)

    const group = await ajax.getJson(`/api/groups/${meet.groupPath}`)
    state.group = group
    logger.debug(`Successfully loaded group '${group.path}'`)
  },
  resetState,
  render: (state, emit) => {
    t.Object(state, ['state',])
    t.Object(state.global, ['state', 'global',])
    t.Function(emit, ['emit',])
    const {meet,} = state
    t.Object(meet, ['state', 'meet',])
    t.String(meet.groupPath, ['state', 'meet', 'groupPath',])
    const {loggedInUser,} = state.global

    const isPermitted = loggedInUser != null && (loggedInUser.isAdmin ||
      isGroupAdmin(loggedInUser, meet.groupPath))
    if (!isPermitted) {
      if (loggedInUser != null) {
        logger.debug(`The logged in user isn't authorized to edit the meet, returning to homepage`)
      }
      return 'redirectToHomepage'
    }

    logger.debug({meet,}, `Rendering form for editing meet ${meet.uuid}`)
    return h(`.${sheet}`, h('#edit-meet-view', [
      renderRightColumn(state, emit),
      state.shouldShowCancelConfirmationDialog ?
        renderCancelConfirmationDialog(state, emit) : null,
    ]))
  },
}

const renderCancelConfirmationDialog = (state, emit) => {
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])

  const closeModal = () => {
    state.shouldShowCancelConfirmationDialog = false
    emit.triggerRender()
  }

  logger.debug(`Rendering dialog to confirm canceling of meet editing`)
  return modalComponent({
    closeCallback: closeModal,
    headerLabel: 'Cancel Editing Meet?',
    content: [
      h('.modal-body-content', [
        h('p', `Are you sure you wish to cancel editing this meet?`),
      ]),
      h('.button-group', [
        focusedElement('button#modal-yes.pure-button.pure-button-primary', {
          onclick: () => {
            logger.debug(`Yes button clicked - redirecting to meet page`)
            emit.global('pushState', `/m/${state.meet.uuid}`)
          },
        }, 'Yes'),
        h('button#modal-no.pure-button', {
          onclick: () => {
            logger.debug(`No button clicked`)
            closeModal()
          },
        }, 'No'),
      ]),
    ],
  })
}

const submitEditMeet = (state, emit) => {
  t.Object(state, ['state',])
  t.Object(state.meet, ['state', 'meet',])
  t.Function(emit, ['emit',])

  logger.debug({state,}, `Submitting edited meet...`)
  const title = trim(state.input.title)
  const starts = getDateTimeStr(state.input.startsDate, state.input.startsTime, true)
  t.String(starts, ['starts',])
  const ends = getDateTimeStr(state.input.endsDate, state.input.endsTime, false)
  if (ends != null) {
    t.String(ends, ['ends',])
  }
  const description = trim(document.getElementById('wmd-input-markdown').value)
  assert.ok(!isNullOrBlank(title))
  assert.ok(!isNullOrBlank(description))
  const meetInput = {
    title,
    starts,
    ends,
    description,
    hosts: map((host) => {
      return host.username
    }, state.hosts),
    location: !state.isOnline ? state.location : null,
    isOnline: state.isOnline,
    attendeeLimit: state.input.attendeeLimit,
  }

  submitToServer(meetInput, state, emit)
}

const submitToServer = async (meetInput, state, emit) => {
  t.Object(meetInput, ['meetInput',])
  t.Object(state, ['state',])

  state.isSubmissionEnabled = false
  state.isCancelEnabled = false
  emit.triggerRender()

  logger.debug(`Submitting meet ${state.meet.uuid} to server:`, meetInput)
  let meet
  try {
    meet = await ajax.putJson(`/api/meets/${state.meet.uuid}`, meetInput)
  } catch (error) {
    logger.error({error,}, `Editing meet failed on server`)
    if (error.data != null) {
      t.Object(error.data, ['error', 'data',])
      state.isLoading = false
      state.errorMessages = error.data
    } else {
      insertErrorNotificationPane('Editing Meet Failed:', [error.message,])
    }

    state.isSubmissionEnabled = true
    state.isCancelEnabled = true
    emit.triggerRender()
    return
  }

  logger.debug({meet,}, `Meet was successfully edited`)
  // Reset the loading state for meet view, so that this meet gets reloaded and we're not
  // stuck with stale data
  delete state.global.components.meetView.loadingState
  state.meet = meet
  state.isSubmissionEnabled = true
  state.isCancelEnabled = true
  emit.global('pushState', `/m/${meet.uuid}`)
}
