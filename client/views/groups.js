const h = require('@arve.knudsen/hyperscript')
const sheetify = require('sheetify')
const t = require('tcomb')
const map = require('ramda/src/map')
const addIndex = require('ramda/src/addIndex')
const titleize = require('titleize')
const jdenticon = require('jdenticon')
const findIndex = require('ramda/src/findIndex')
const equals = require('ramda/src/equals')
const prop = require('ramda/src/prop')

const ajax = require('../ajax')
const logger = require('../logger').child({module: 'views/groups',})

const sheet = sheetify('./groups.styl')

// Render the logged in user's groups
const renderUsersGroups = (state, emit) => {
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])
  const groups = state.usersGroups || []
  if (groups.length === 0) {
    return h(`#users-groups.empty-groups-tab-content`, `You're not a member of any groups.`)
  }

  return h(`#users-groups`, map((group) => {
    const avatarElem = h(`.group-avatar-container`)
    avatarElem.innerHTML = jdenticon.toSvg(group.path, 80)
    return h(`.group-container`, h(`.group-inner`, [
      h(`a`, {href: `/g/${group.path}`,}, avatarElem),
      h(`.group-content`, [
        h(`a`, {href: `/g/${group.path}`,}, [
          h(`.group-name`, group.name),
          h(`.group-description`, group.description),
        ]),
      ]),
    ]))
  }, groups))
}

const renderExplore = (state, emit) => {
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])
  return h(`#explore-groups.empty-groups-tab-content`, `No content to show.`)
}

module.exports = {
  name: 'groupsView',
  route: '/groups',
  resetState: (state) => {
    t.Object(state, ['state',])
    t.Object(state.global, ['state', 'global',])
    const {loggedInUser,} = state.global
    if (loggedInUser != null) {
      state.activeTab = 'Your Groups'
    } else {
      state.activeTab = 'Explore'
    }
  },
  shouldReloadData: (state) => {
    t.Object(state, ['state',])
    t.Object(state.global, ['state', 'global',])
    logger.debug(`Determining whether we should reload the view's data`)
    const {loggedInUser,} = state.global
    if (loggedInUser == null) {
      logger.debug(`Not reloading the view's data, since the user isn't logged in`)
      return false
    }

    const shouldReload = !equals(map(prop('groupPath'), loggedInUser.groups), map(prop('path'), state.usersGroups || []))
    if (shouldReload) {
      logger.debug(`Reloading the view's data since the cached data is invalid`)
    } else {
      logger.debug(`Not reloading the view's data since the cached data is still valid`)
    }
    return shouldReload
  },
  loadData: async (state, emit) => {
    t.Object(state, ['state',])
    t.Object(state.global, ['state', 'global',])
    t.Function(emit, ['emit',])
    const {loggedInUser,} = state.global
    if (loggedInUser == null) {
      return
    }

    const {username,} = loggedInUser
    logger.debug(`Loading groups of user ${username}...`)
    const groups = await ajax.getJson(`/api/users/${username}/groups`)
    logger.debug(`Successfully loaded groups for user ${username}`)
    state.usersGroups = groups
  },
  render: (state, emit) => {
    t.Object(state, ['state',])
    t.Object(state.global, ['state', 'global',])
    t.Function(emit, ['emit',])
    const {loggedInUser,} = state.global
    const isLoggedIn = loggedInUser != null
    let tabs = ['Explore',]
    if (isLoggedIn) {
      tabs.unshift('Your Groups')
    }
    logger.debug(`Rendering groups page`, {isLoggedIn, tabs,})
    let renderer
    if (state.activeTab === `Your Groups`) {
      renderer = renderUsersGroups
    } else if (state.activeTab === `Explore`) {
      renderer = renderExplore
    } else {
      throw new Error(`Unrecognized tab '${state.activeTab}'`)
    }
    const activeIndex = findIndex(equals(state.activeTab), tabs)
    return h(`#groups-view.${sheet}`, [
      h(`#groups-toolbar`, [
        h(`h1#groups-heading`, `Groups`),
        isLoggedIn ? h(`a#new-group-button.button.primary-button`, {
          href: `/new-group`,
        }, [`New Group`,]) : null,
      ]),
      h(`hr`),
      // We need a container for the tab elements, so we can make it 100% wide and cover any elements
      // scrolling beneath it
      h(`nav#groups-tabs`, h(`ul`, addIndex(map)((name, i) => {
        logger.debug(`i: ${i}, name: ${name}`)
        t.Number(i, ['i',])
        t.String(name, ['name',])
        let sfx = ''
        if (i === activeIndex) {
          sfx = `.active`
        }
        const id = `${name.toLowerCase().replace(' ', '-')}-tab`
        return h(`li#${id}.tab${sfx}`, {
          onclick: () => {
            if (state.activeTab !== name) {
              logger.debug(`Switching to groups tab '${name}'`)
              state.activeTab = name
              emit.triggerRender()
            }
          },
        }, titleize(name))
      }, tabs))),
      h(`#groups-tab-content`, [
        renderer(state, emit),
      ]),
    ])
  },
}
