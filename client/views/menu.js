const h = require('@arve.knudsen/hyperscript')
const sheetify = require('sheetify')
const map = require('ramda/src/map')
const t = require('tcomb')
const Popper = require('popper.js')
const assert = require('assert')
const concat = require('ramda/src/concat')
const includes = require('ramda/src/includes')
const keys = require('ramda/src/keys')
const titleize = require('titleize')
const toPairs = require('ramda/src/toPairs')

const createIconElem = require('../createIconElem')
const {nbsp,} = require('../specialChars')
const cachedWidget = require('./cachedWidget')
const logger = require('../logger').child({module: 'views/menu',})

const sheet = sheetify('./menu.styl')

const ACCOUNT_MENU_DROPDOWN_ELEM_ID = `account-menu-dropdown`
const ACCOUNT_ICON_ELEM_ID = `account-icon`

let accountMenuDropdownPopper = null
const renderAccountMenuDropdown = cachedWidget((state, emit) => {
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])
  const globalState = state.global != null ? state.global : state
  t.Object(globalState.loggedInUser, ['globalState', 'loggedInUser',])
  const {loggedInUser,} = globalState
  return h(`#${ACCOUNT_MENU_DROPDOWN_ELEM_ID}`, [
    h('ul.list.pl0', concat([
      h('li#account-menu-dropdown-header', `Signed in as`, nbsp,
        h('strong', loggedInUser.username)),
      ],
      map(({uri, id, text,}) => {
        return h(`li#${id}-link`, h('a.account-menu-dropdown-item', {
          href: uri,
          onclick: () => {
            state.shouldShowAccountMenuDropdown = false
            emit.triggerRender()
          },
        }, text))
      }, [
        {uri: `/u/${loggedInUser.username}`, id: `profile`, text: `Your profile`,},
        {uri: `/settings`, id: `settings`, text: `Settings`,},
        {uri: `/logout`, id: `logout`, text: `Log out`,},
      ])
    )),
  ])
}, {
  onloadHandler: (element) => {
    logger.debug(`Account menu dropdown element loaded:`, element)
    const accountIconElem = document.getElementById('account-icon')
    assert.notEqual(accountIconElem, null)
    accountMenuDropdownPopper = new Popper(accountIconElem, element, {
      modifiers: {
        preventOverflow: {
          enabled: true,
          boundariesElement: 'viewport',
        },
      },
    })
  },
  updateHandler: (state) => {
    logger.debug(`Handling update`)
    if (accountMenuDropdownPopper != null) {
      logger.debug(`Updating account menu dropdown element after state change`)
      const elem = document.getElementById(ACCOUNT_MENU_DROPDOWN_ELEM_ID)
      const globalState = state.global != null ? state.global : state
      if (globalState.shouldShowAccountMenuDropdown) {
        logger.debug(`Showing account menu dropdown element`)
        elem.style.display = `inherit`
      } else {
        logger.debug(`Hiding account menu dropdown element`)
        elem.style.display = `none`
      }
      accountMenuDropdownPopper.update()
    }
  },
  callerId: `renderAccountMenuDropdown`,
})

let hasInstalledEventListener = false
const renderAccountSection = (state, emit, currentPath) => {
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])
  t.String(currentPath, ['currentPath',])
  const isLoggedIn = state.loggedInUser != null

  if (!hasInstalledEventListener) {
    document.addEventListener('click', (event) => {
      t.Object(event, ['event',])
      if (state.shouldShowAccountMenuDropdown) {
        if (event.target.closest(`#${ACCOUNT_ICON_ELEM_ID}`) == null) {
          logger.debug(`There was a click outside of the account icon/dropdown, closing it`)
          state.shouldShowAccountMenuDropdown = false
          emit.triggerRender()
        } else {
          logger.debug(`There was a click within the account icon/dropdown, not closing it`)
        }
      }
    })
    hasInstalledEventListener = true
  }

  let menuItems
  if (!isLoggedIn) {
    menuItems = map(({uri, id, text,}) => {
      t.String(uri, ['uri',])
      t.String(id, ['id',])
      t.String(text, ['text',])
      const suffix = uri === currentPath ? '.pure-menu-selected' : ''
      return h(`li#${id}-link.dib${suffix}`, [
        h('a.menu-link.pa2.no-underline.white', {attrs: {href: uri,},}, text),
      ])
    }, [
      {
        uri: '/signup',
        id: 'signup',
        text: 'Sign Up',
      },
      {
        uri: '/login',
        id: 'login',
        text: 'Log In',
      },
    ])
  } else {
    menuItems = [
      h(`li#${ACCOUNT_ICON_ELEM_ID}`, [
        createIconElem(`user3`, {
          classes: ['clickable',],
          eventListeners: {
            click: () => {
              logger.debug(`Account icon clicked`)

              const shouldShow = !state.shouldShowAccountMenuDropdown
              if (shouldShow) {
                logger.debug(`Showing account menu dropdown`)
              } else {
                logger.debug(`Hiding account menu dropdown`)
              }
              state.shouldShowAccountMenuDropdown = shouldShow
              emit.triggerRender()
            },
          },
        }),
        renderAccountMenuDropdown(state, emit),
      ]),
    ]
  }
  return h('#account-menu-section', [
    h('ul.list.pl0.ma0', menuItems),
  ])
}

// Render horizontal menu
module.exports = (state, emit, link2MenuAttrs, pathname) => {
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])
  t.Function(emit.triggerRender, ['emit', 'triggerRender',])
  t.Object(link2MenuAttrs, ['link2MenuAttrs',])
  t.String(pathname, ['pathname',])

  const currentPath = includes(pathname, concat(keys(link2MenuAttrs), ['/signup', '/login',])) ?
    pathname : '/'
  logger.debug(`Rendering, currentPath: '${currentPath}'`)
  return h(`.${sheet}`, [
    h('header#menu-header.w-100.ph5-ns', [
      h('nav#menu', [
        h('a.menu-heading.no-underline.white', {
          href: '/',
        }, 'Commingle'),
        h(`ul#pages-menu-section`, map(([path, attrs,]) => {
          t.Object(attrs, ['attrs',])
          const {name,} = attrs
          t.String(name, ['page', 'name',])
          t.String(path, ['path',])
          const suffix = path === currentPath ? '.selected' : ''
          return h(`li#menu-item-${name}.menu-item${suffix}`, h(`a`, {href: path,}, titleize(name)))
        }, toPairs(link2MenuAttrs))),
        renderAccountSection(state, emit, currentPath),
      ]),
    ]),
  ])
}
