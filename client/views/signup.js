const h = require('@arve.knudsen/hyperscript')
const sheetify = require('sheetify')
const logger = require('../logger').child({module: 'views/signup',})
const mergeRight = require('ramda/src/mergeRight')
const all = require('ramda/src/all')
const {isNullOrBlank,} = require('@arve.knudsen/stringutils')
const t = require('tcomb')
const assert = require('assert')
const pipe = require('ramda/src/pipe')
const fromPairs = require('ramda/src/fromPairs')
const toPairs = require('ramda/src/toPairs')
const map = require('ramda/src/map')
const isEmpty = require('ramda/src/isEmpty')
const trim = require('ramda/src/trim')
const forEach = require('ramda/src/forEach')
const values = require('ramda/src/values')

const loadingComponent = require('../components/loading')(['signupView', 'loading',])
const focusedElement = require('../focusedElement')
const sheet = sheetify('./signup.styl')
const ajax = require('../ajax')
const insertErrorNotificationPane = require('./insertErrorNotificationPane')

const renderInput = (name, placeholder, autocomplete, state, emit, opts={}) => {
  t.String(name, ['name',])
  t.String(placeholder, ['placeholder',])
  t.String(autocomplete, ['autocomplete',])
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])
  const focus = opts.focus
  delete opts.focus

  const inputValue = state.input[name]
  t.String(inputValue, ['state', 'input', name,])
  const errorMsg = state.errorMessages[name]
  const errClassStr = !isNullOrBlank(errorMsg) ? `.field-with-error` : ``
  const renderer = focus ? focusedElement : h
  return [
    renderer(`input#signup-input-${name.toLowerCase()}${errClassStr}.signup-input.b--moon-gray.db.w-100.pa2.mb2`,
      mergeRight({
      placeholder,
      autocomplete,
      required: true,
      value: inputValue,
      oninput: (event) => {
        state.input[name] = event.target.value

        const inputNames = ['username', 'name', 'email', 'password',]
        state.submissionEnabled = all((inputName) => {
          return !isNullOrBlank(state.input[inputName])
        }, inputNames)

        if (!isNullOrBlank(state.errorMessages[name])) {
          delete state.errorMessages[name]
        }

        emit.triggerRender()
      },
    }, opts)),
    errorMsg != null ? h('.error-message', errorMsg) : null,
  ]
}

const redirectAuthenticatedUser = (state, emit) => {
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])

  if (!isNullOrBlank(state.query.next)) {
    const nextUrl = decodeURIComponent(state.query.next)
    logger.debug(`Redirecting to ${nextUrl}`)
    window.location.href = nextUrl
  } else {
    logger.debug(`Redirecting to homepage`)
    emit.global('pushState', '/')
  }
}

const submitSignup = async (state, emit) => {
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])

  logger.debug(`Submitting user registration...`)
  let extra
  const {username, email, name, password,} = pipe(
    toPairs,
    map(([key, value,]) => {
      return [key, trim(value),]
    }),
    fromPairs
  )(state.input)
  assert.ok(!isNullOrBlank(password), `state.input.password cannot be null`)
  extra = {
    password,
  }
  assert.ok(!isNullOrBlank(username), `state.input.username cannot be null`)
  assert.ok(!isNullOrBlank(name), `state.input.name cannot be null`)
  assert.ok(!isNullOrBlank(email), `state.input.email cannot be null`)

  state.errorMessages = {}
  emit.triggerRender()

  let user
  try {
    user = await ajax.postJson('/api/accounts', mergeRight({
      username, name, emailAddress: email, provider: 'native',
    }, extra))
  } catch (err) {
    logger.debug(`User registration failed on server:`, {err,})
    if (isEmpty(err.details || {})) {
      logger.debug(`Error has no details, displaying it in notification pane`)
      insertErrorNotificationPane('Registration Failed:', [err.message,])
    } else {
      logger.debug(`Error has details, displaying them individually`)
      t.Object(err.details, ['err', 'details',])
      state.errorMessages = err.details
      emit.triggerRender()
    }
    return
  }

  logger.debug(`Successfully registered user on server`)
  state.global.loggedInUser = user

  logger.debug(`Clearing view caches, so views are reloaded wrt. user being logged in`)
  t.Object(state.global.components, ['state', 'global', 'components',])
  forEach((componentState) => {
    delete componentState.loadingState
  }, values(state.global.components))

  logger.debug(`Redirecting to homepage (from ${window.location})`)
  redirectAuthenticatedUser(state, emit)
}

const renderSignupForm = (state, emit) => {
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])
  logger.debug(`Rendering signup form`)
  return h('#signup-pad.w-100-ns.mw6-ns.center.pa4.ba.br2.b--moon-gray', [
    h('form#signup-form', {
      onsubmit: (event) => {
        logger.debug(`Form was submitted`)
        event.preventDefault()
        submitSignup(state, emit)
      },
    }, [
      h('fieldset', [
        renderInput('email', 'Email address', 'email', state, emit, {
          type: 'email', focus: true,
        }),
        renderInput('username', 'Username', 'username', state, emit),
        renderInput('name', 'Name', 'name', state, emit),
        renderInput('password', 'Password', 'new-password', state, emit,
          {type: 'password',}),
      ]),
    ]),
    h('.button-group', [
      h('button#submit-signup-button', {
        type: 'submit',
        disabled: !state.submissionEnabled,
        onclick: () => {
          logger.debug(`Submit button clicked`)
          submitSignup(state, emit)
        },
      }, 'Sign Up'),
    ]),
  ])
}

module.exports = {
  name: 'signupView',
  route: '/signup',
  resetState: (state) => {
    logger.debug(`Resetting state`)
    state.input = {
      name,
      username: '',
      email: '',
      password: '',
    }
    state.isLoading = false
    state.submissionEnabled = false
    state.errorMessages = {}
  },
  render: (state, emit) => {
    logger.debug(`Rendering`)

    if (state.global.loggedInUser == null) {
      let content
      if (state.isLoading) {
        logger.debug(`In loading state`)
        content = loadingComponent.render(state, emit)
      } else {
        content = renderSignupForm(state, emit)
      }

      return h(`.${sheet}`, [
        content,
      ])
    } else {
      if (!isNullOrBlank(state.query.next)) {
        const nextUrl = decodeURIComponent(state.query.next)
        logger.debug(`Redirecting to ${nextUrl}`)
        window.location.href = nextUrl
        return loadingComponent.render(state, emit)
      } else {
        logger.debug(`Redirecting to homepage`)
        return 'redirectToHomepage'
      }
    }
  },
  pageTitle: 'Sign Up',
}
