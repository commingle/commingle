const h = require('@arve.knudsen/hyperscript')
const sheetify = require('sheetify')
const t = require('tcomb')
const assert = require('assert')
const {isNullOrBlank,} = require('@arve.knudsen/stringutils')
const {DateTime,} = require('luxon')
const map = require('ramda/src/map')
const addIndex = require('ramda/src/addIndex')
const titleize = require('titleize')
const jdenticon = require('jdenticon')
const isEmpty = require('ramda/src/isEmpty')
const pipe = require('ramda/src/pipe')
const groupBy = require('ramda/src/groupBy')
const toPairs = require('ramda/src/toPairs')
const includes = require('ramda/src/includes')
const reject = require('ramda/src/reject')
const prop = require('ramda/src/prop')
const propEq = require('ramda/src/propEq')
const forEach = require('ramda/src/forEach')

const {isGroupAdmin,} = require('../groups')
const insertErrorNotificationPane = require('../insertErrorNotificationPane')
const focusedElement = require('../focusedElement')
const modalComponent = require('../components/modal')
const ajax = require('../ajax')
const logger = require('../logger').child({module: 'views/groupProfile',})
const createIconElem = require('../createIconElem')

const sheet = sheetify('./groupProfile.styl')

// Render upcoming meets.
const renderUpcoming = (state) => {
  t.Object(state, ['state',])
  const {group,} = state
  t.Object(group, ['state.group',])

  if (isEmpty(group.upcoming || [])) {
    return h(`#group-upcoming.empty-group-tab`, `${group.name} doesn't have any upcoming meets.`)
  }

  // TODO: Convert to infinite scroll
  return h(`#group-upcoming`, h(`ul`, pipe(
    groupBy((meet) => {
      logger.debug(`Grouping by ${DateTime.fromISO(meet.starts).toLocal().toISODate()}`)
      return DateTime.fromISO(meet.starts).toLocal().toISODate()
    }),
    toPairs,
    map(([dateStr, meets,]) => {
      const date = DateTime.fromISO(dateStr).toLocal()
      return h(`li.date-with-meets`, [
        h(`.meet-date`, date.toLocaleString(DateTime.DATE_FULL)),
        h(`ul.meets-for-date`, map((meet) => {
          const starts = DateTime.fromISO(meet.starts).toLocal()
          const ends = DateTime.fromISO(meet.ends).toLocal()
          const endStr = ends.diff(starts).days === 0 ? ends.toLocaleString(DateTime.TIME_SIMPLE) :
            ends.toLocaleString(DateTime.DATETIME_SHORT)
          const timeStr = `${starts.toLocaleString(DateTime.DATETIME_SHORT)} - ${endStr}`
          return h(`li.meet-container`, [
            h(`.meet-content`, [
              h(`a`, {href: `/m/${meet.uuid}`,}, [
                h(`.meet-title`, `${meet.title}`),
                h(`.meet-time`, timeStr),
                h(`.meet-location`, meet.location),
                h(`.meet-going`, isEmpty(meet.going) ? `Noone has signed up yet` :
                  `${meet.going.length} going`),
              ]),
            ]),
          ])
        }, meets)),
      ])
    })
  )(group.upcoming)))
}

// Render past meets.
const renderPast = (state) => {
  t.Object(state, ['state',])
  const {group,} = state
  t.Object(group, ['state.group',])

  if (isEmpty(group.past || [])) {
    return h(`#group-past.empty-group-tab`, `${group.name} doesn't have any past meets.`)
  }

  // TODO: Convert to infinite scroll
  return h(`#group-past`, h(`ul`, pipe(
    groupBy((meet) => {
      logger.debug(`Grouping by ${DateTime.fromISO(meet.starts).toLocal().toISODate()}`)
      return DateTime.fromISO(meet.starts).toLocal().toISODate()
    }),
    toPairs,
    map(([dateStr, meets,]) => {
      const date = DateTime.fromISO(dateStr).toLocal()
      return h(`li.date-with-meets`, [
        h(`.meet-date`, date.toLocaleString(DateTime.DATE_FULL)),
        h(`ul.meets-for-date`, map((meet) => {
          const starts = DateTime.fromISO(meet.starts).toLocal()
          const ends = DateTime.fromISO(meet.ends).toLocal()
          const endStr = ends.diff(starts).days === 0 ? ends.toLocaleString(DateTime.TIME_SIMPLE) :
            ends.toLocaleString(DateTime.DATETIME_SHORT)
          const timeStr = `${starts.toLocaleString(DateTime.DATETIME_SHORT)} - ${endStr}`
          return h(`li.meet-container`, [
            h(`.meet-content`, [
              h(`a`, {href: `/m/${meet.uuid}`,}, [
                h(`.meet-title`, `${meet.title}`),
                h(`.meet-time`, timeStr),
                h(`.meet-location`, meet.location),
                h(`.meet-going`, isEmpty(meet.going) ? `Noone went to this meet` :
                  `${meet.going.length} went`),
              ]),
            ]),
          ])
        }, meets)),
      ])
    })
  )(group.past)))
}

const renderMembers = (state) => {
  t.Object(state, ['state',])
  const {group,} = state
  t.Object(group, ['state.group',])
  if (isEmpty(group.members || [])) {
    return h(`#group-members.empty-group-tab`, `${group.name} doesn't have any members.`)
  }

  return h(`#group-members`, map((member) => {
    return h(`.group-member-container`, [
      h(`a`, {href: `/u/${member.username}`,}, h(`.group-member-avatar-container`,
        h(`img.group-member-avatar`, {src: member.avatarUrl,}))),
      h(`.group-member-content`, [
        h(`a`, {href: `/u/${member.username}`,}, [
          h(`.group-member-name`, `${member.name} (${member.username})`),
          h(`.group-member-bio`, member.bio),
        ]),
      ]),
    ])
  }, group.members))
}

const renderLeftColumn = (state, group) => {
  t.Object(state, ['state',])
  t.Object(state.global, ['state', 'global',])
  const {loggedInUser,} = state.global
  t.Object(group, ['group',])
  const creationDate = DateTime.fromISO(group.added).toLocal().toLocaleString(DateTime.DATE_FULL)
  const avatarElem = h(`#avatar-container`)
  avatarElem.innerHTML = jdenticon.toSvg(group.path, 228)
  const numMembers = group.members.length
  const membersSfx = numMembers !== 1 ? 's' : ''
  let membershipStr
  if (loggedInUser != null) {
    forEach((member) => {
      if (membershipStr != null || member.username !== loggedInUser.username) {
        return
      }

      if (member.membership.toLowerCase() === 'admin') {
        membershipStr = `You are an admin of this group`
      } else {
        membershipStr = `You are a member of this group`
      }
    }, group.members)

    if (membershipStr == null) {
      membershipStr = `You are not a member of this group`
    }
  }
  return h(`#left-column`, [
    avatarElem,
    h(`#group-names-container.bb.b--light-gray.pv3.mb0`, [
      h(`#group-name`, group.name),
      h(`#group-path.muted`, group.path),
    ]),
    h(`#group-metadata-container.b--light-gray.pv3.mb0.f6`, [
      h(`#group-description.group-metadata`, group.description),
      h(`#group-creation-date.group-metadata`, `Created ${creationDate}`),
      h(`#group-num-members.group-metadata`, `Has ${numMembers} member${membersSfx}`),
      membershipStr != null ? h(`#group-membership.group-metadata`, membershipStr) : null,
      renderSocialSection(group),
    ]),
  ])
}

const renderSocialSection = (group) => {
  if (isNullOrBlank(group.gitlabGroupPath)) {
    return null
  }

  return [
    h('#social-gitlab.group-metadata', [
      createIconElem('gitlab'),
      h(`a`, {
        href: `https://gitlab.com/${group.gitlabGroupPath}`,
        target: `_blank`,
        rel: `noreferrer noopener`,
      }, group.gitlabGroupPath),
    ]),
  ]
}

const renderRightColumn = (state, emit) => {
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])
  const {group,} = state
  t.Object(group, ['state.group',])
  let renderer
  let activeIndex
  if (state.activeTab === `upcoming`) {
    renderer = renderUpcoming
    activeIndex = 0
  } else if (state.activeTab === `past`) {
    renderer = renderPast
    activeIndex = 1
  } else if (state.activeTab === `members`) {
    renderer = renderMembers
    activeIndex = 2
  } else {
    throw new Error(`Unrecognized tab '${state.activeTab}'`)
  }

  return h(`#right-column`, [
    h(`#top-section`, [
      h(`#breadcrumbs`, [
        h(`a`, {href: `/g/${group.path}`,}, group.name),
        h(`span.arrow`, `>`),
        h(`span`, `Details`),
      ]),
      h(`hr`),
      h(`#top-section-inner`, [,
        h(`nav#group-info-tabs.bg-white`, h(`ul.pa0.ma0.list.flex.bg-white`, addIndex(map)((name, i) => {
          logger.debug(`i: ${i}, name: ${name}`)
          t.Number(i, ['i',])
          t.String(name, ['name',])
          let sfx = ''
          if (i === activeIndex) {
            sfx = `.active`
          }
          const id = `${name}-tab`
          return h(`li#${id}.tab${sfx}`, {
            onclick: () => {
              if (state.activeTab !== name) {
                logger.debug(`Switching to group ${name} tab`)
                state.activeTab = name
                emit.triggerRender()
              }
            },
          }, titleize(name))
        }, ['upcoming', 'past', 'members',]))),
        renderToolbar(state, emit),
      ]),
    ]),
    h(`#group-info-tab-content.ba.b--light-gray.bt-0`, [
      renderer(state),
    ]),
  ])
}

const renderToolbar = (state, emit) => {
  t.Object(state, ['state',])
  t.Object(state.global, ['state', 'global',])
  t.Function(emit, ['emit',])
  const {group,} = state
  t.Object(group, ['state.group',])
  const {loggedInUser,} = state.global
  const isAuthorizedToEdit = loggedInUser != null && (isGroupAdmin(loggedInUser, group.path) ||
    loggedInUser.isAdmin)

  let content
  if (isAuthorizedToEdit) {
    logger.debug(`Logged in user is authorized to edit group, rendering corresponding buttons`)
    content = [
      h(`a#edit-group-button.button`, {href: `/g/${group.path}/edit`,}, `Edit`),
      h(`a#delete-group-button.button`, {
        href: `#`,
        onclick: () => {
            logger.debug(`User clicked control for deleting group`)
            state.shouldShowDeleteGroupConfirmationDialog = true
            emit.triggerRender()
        },
      }, `Delete`),
      h(`a#new-meet-button.button.primary-button`, {href: `/g/${group.path}/new-meet`,}, `New Meet`),
    ]
  } else if (loggedInUser != null) {
    if (!includes(group.path, map(prop('groupPath'), loggedInUser.groups))) {
      logger.debug(
      `Logged in user is not a member of the group, rendering join button`)
      content = [
        h(`button#join-group-button.button`, {
          disabled: state.isChangingMembership,
          onclick: () => {
            logger.debug(`User clicked button to join group`)
            state.isChangingMembership = true
            emit.triggerRender()
            // Defer request until after above update has been rendered
            setTimeout(() => {
              ajax.postJson(`/api/groups/${group.path}/members`, {
                username: loggedInUser.username,
              })
                .then(() => {
                  logger.debug(`Successfully made API call to join group`)
                  loggedInUser.groups.push({
                    groupPath: group.path,
                    membership: 'Member',
                  })
                  state.group.members.push({
                    username: loggedInUser.username,
                    name: loggedInUser.name,
                    emailAddress: loggedInUser.emailAddress,
                    avatarUrl: loggedInUser.avatarUrl,
                    bio: loggedInUser.bio,
                    membership: 'Member',
                  })
                  state.isChangingMembership = false
                  emit.triggerRender()
                })
                .catch((err) => {
                  logger.error(`Failed to make API call to join group`, {err,})
                  state.isChangingMembership = false
                  emit.triggerRender()
                  // TODO: Show error
                })
            }, 0)
          },
        }, `Join`),
      ]
    } else {
      logger.debug(
      `Logged in user is a member of the group, rendering leave button`)
      content = [
        h(`button#leave-group-button.button`, {
          disabled: state.isChangingMembership,
          onclick: () => {
            logger.debug(`User clicked button to leave group`)
            state.isChangingMembership = true
            emit.triggerRender()
            // Defer request until after above update has been rendered
            setTimeout(() => {
              ajax.delete(`/api/groups/${group.path}/members/${loggedInUser.username}`)
                .then(() => {
                  logger.debug(`Successfully made API call to leave group`)
                  loggedInUser.groups = reject((membership) => {
                    return membership.groupPath === group.path
                  }, loggedInUser.groups)
                  state.group.members = reject((member) => {
                    return member.username === loggedInUser.username
                  }, state.group.members)
                  state.isChangingMembership = false
                  emit.triggerRender()
                })
                .catch((err) => {
                  logger.error(`Failed to make API call to leave group`, {err,})
                  state.isChangingMembership = false
                  emit.triggerRender()
                  // TODO: Show error
                })
            }, 0)
          },
        }, `Leave`),
      ]
    }
  }

  return h(`#group-toolbar`, content)
}

module.exports = {
  name: 'groupProfileView',
  route: '/g/:path',
  resetState: (state) => {
    state.activeTab = `upcoming`
    state.shouldShowDeleteGroupConfirmationDialog = false
    state.isChangingMembership = false
  },
  loadData: async (state, emit) => {
    t.Object(state, ['state',])
    t.Function(emit, ['emit',])
    const path = state.params.path
    assert.ok(!isNullOrBlank(path))
    logger.debug(`Loading group ${path}...`)
    const group = await ajax.getJson(`/api/groups/${path}`)
    logger.debug(`Successfully loaded group '${path}'`)
    state.group = group
  },
  render: (state, emit) => {
    t.Object(state, ['state',])
    t.Object(state.global, ['state', 'global',])
    t.Function(emit, ['emit',])
    const {group,} = state
    t.Object(group, ['state.group',])
    logger.debug(`Rendering profile page of group '${group.path}'`, {state,})
    return h(`.${sheet}`, [
      h(`#group-profile-view`, [
        renderLeftColumn(state, group),
        renderRightColumn(state, emit, group),
      ]),
      state.shouldShowDeleteGroupConfirmationDialog ?
        renderDeleteGroupConfirmationDialog(state, emit) : null,
    ])
  },
}

const deleteGroup = async (state, emit) => {
  t.Object(state, ['state',])
  t.Object(state.global, ['state', 'global',])
  t.Function(emit, ['emit',])
  logger.debug({group: state.group,}, `Submitting request to server to delete group`)
  await ajax.delete(`/api/groups/${state.group.path}`)
  const {loggedInUser,} = state.global
  if (loggedInUser != null) {
    loggedInUser.groups = reject(propEq('groupPath', state.group.path), loggedInUser.groups || [])
  }
  // Force reloading of the home view state
  delete state.global.components.homeView.loadingState
  emit.global('pushState', `/`)
}

const renderDeleteGroupConfirmationDialog = (state, emit) => {
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])

  const closeModal = () => {
    state.shouldShowDeleteGroupConfirmationDialog = false
    emit.triggerRender()
  }

  logger.debug(`Rendering dialog to confirm deletion of group`)
  return modalComponent({
    closeCallback: closeModal,
    headerLabel: 'Delete Group?',
    content: [
      h('.modal-body-content', [
        h('p', `Are you really sure you wish to delete this group?`),
      ]),
      h('.button-group', [
        focusedElement('button#modal-yes', {
          onclick: () => {
            logger.debug(`Yes button clicked - deleting group...`)
            deleteGroup(state, emit)
              .catch((error) => {
                logger.error({error,}, `Failed to delete group`)
                insertErrorNotificationPane(`Deleting group failed!`, `${error}.`)
              })
          },
        }, 'Yes'),
        h('button#modal-no.pure-button', {
          onclick: () => {
            logger.debug(`No button clicked`)
            closeModal()
          },
        }, 'No'),
      ]),
    ],
  })
}
