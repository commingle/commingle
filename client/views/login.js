const h = require('@arve.knudsen/hyperscript')
const sheetify = require('sheetify')
const t = require('tcomb')
const assert = require('assert')
const mergeRight = require('ramda/src/mergeRight')
const all = require('ramda/src/all')
const {isNullOrBlank,} = require('@arve.knudsen/stringutils')
const values = require('ramda/src/values')
const toPairs = require('ramda/src/toPairs')
const fromPairs = require('ramda/src/fromPairs')
const pipe = require('ramda/src/pipe')
const map = require('ramda/src/map')
const trim = require('ramda/src/trim')
const forEach = require('ramda/src/forEach')

const logger = require('../logger').child({module: 'views/login',})
const loadingComponent = require('../components/loading')(['loginView', 'loading',])
const ajax = require('../ajax')
const focusedElement = require('../focusedElement')

const sheet = sheetify('./login.styl')

const renderInput = (name, placeholder, autocomplete, state, emit, opts={}) => {
  const focus = opts.focus
  delete opts.focus
  const renderer = focus ? focusedElement : h
  const inputValue = state.input[name] || ''
  logger.debug(`Rendering input ${name}, value: '${inputValue}'`)
  return [
    renderer(`input#login-input-${name.toLowerCase()}.login-input.b--moon-gray.db.w-100.pa2.mb2`, mergeRight({
      placeholder,
      required: true,
      value: inputValue,
      autocomplete,
      oninput: (event) => {
        state.input[name] = event.target.value
        const submissionEnabled = all((value) => {
          return !isNullOrBlank(value)
        }, values(state.input))
        state.submissionEnabled = submissionEnabled
        state.formErrorMessage = null
        emit.triggerRender()
      },
    }, opts)),
  ]
}

const logUserIn = async (state, emit) => {
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])
  const {usernameOrEmail, password,} = pipe(
    toPairs,
    map(([key, value,]) => {
      return [key, trim(value),]
    }),
    fromPairs
  )(state.input)
  t.String(usernameOrEmail, ['usernameOrEmail',])
  t.String(password, ['password',])
  state.isLoading = true
  emit.triggerRender()

  let user
  try {
    user = await ajax.postJson('/api/login', {
      provider: 'native',
      usernameOrEmail,
      password,
    })
  } catch (error) {
    logger.debug(`User login failed, error:`, error.message)
    state.isLoading = false
    state.formErrorMessage = error.message
    emit.triggerRender()
    return
  }

  logger.debug(`User login succeeded`)
  assert.notEqual(user, null)
  logger.debug(`Redirecting to homepage (from ${window.location})`)
  state.global.loggedInUser = user

  logger.debug(`Clearing view caches, so views are reloaded wrt. user being logged in`)
  t.Object(state.global.components, ['state', 'global', 'components',])
  forEach((componentState) => {
    delete componentState.loadingState
  }, values(state.global.components))

  emit.triggerRender()
}

const renderNativeLogin = (state, emit) => {
  logger.debug(`Rendering native login form, state:`, state)
  if (state.formErrorMessage != null) {
    t.String(state.formErrorMessage, ['loginState', 'formErrorMessage',])
  }
  return [
    !isNullOrBlank(state.formErrorMessage) ? h('#login-form-error.error-message',
        state.formErrorMessage) : null,
    h('form#login-form', {
      onsubmit: (event) => {
        logger.debug(`Form was submitted`)
        event.preventDefault()
        emit('submitLogin')
      },
    }, h('fieldset', [
      renderInput('usernameOrEmail', 'Email or username', 'username', state, emit, {
        focus: true,
      }),
      renderInput('password', 'Password', 'current-password', state, emit, {type: 'password',}),
    ])),
    h('button#submit-login-button.mr2.ph3.pv2', {
      type: 'submit',
      disabled: !state.submissionEnabled,
      onclick: () => {
        logger.debug(`Login submit button clicked`)
        logUserIn(state, emit)
      },
    }, 'Log In'),
    h('a#forgot-password-link.smallish', {href: '/forgotPassword',}, 'Forgot your password?'),
  ]
}

const renderLoginPad = (state, emit) => {
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])

  const {errorNotification,} = state
  const queryStr = !isNullOrBlank(state.global.query.next) ? `?next=${
    encodeURIComponent(state.global.query.next)}` : ``
  return h('#login-pad.w-100-ns.mw6-ns.center.pa4.ba.br2.b--moon-gray', [
    errorNotification != null ? h('#login-error-notification', [
      h('#close-login-error-notification', {
        onclick: () => {
          state.errorNotification = null
          emit.triggerRender()
        },
      }, '✖'),
      h('span', errorNotification),
    ]) : null,
    renderNativeLogin(state, emit),
    h('hr.mv3.bg-moon-gray'),
    h('#direct-to-signup.flex.items-center', [
      h('.vertically-centered', `Don't have an account?`),
      h('a#redirect-link.ph3.pv2.no-underline.black.br2.ml-auto.ba.b--moon-gray', {href: `/signup${queryStr}`,}, 'Sign Up'),
    ]),
  ])
}

module.exports = {
  name: 'loginView',
  route: '/login',
  resetState: (state) => {
    logger.debug(`Resetting state`)
    state.input = {
      usernameOrEmail: '',
      password: '',
    }
    state.formErrorMessage = null
    state.isLoading = false
    state.submissionEnabled = false
    state.errorNotification = null
  },
  render: (state, emit) => {
    t.Object(state, ['state',])
    t.Object(state.global, ['state', 'global',])
    t.Function(emit, ['emit',])

    logger.debug(`Rendering`)
    if (state.global.loggedInUser == null) {
      const content = state.isLoading ? loadingComponent.render(state, emit) :
        renderLoginPad(state, emit)
      return h(`.${sheet}`, content)
    } else {
      if (!isNullOrBlank(state.query.next)) {
        const nextUrl = decodeURIComponent(state.query.next)
        logger.debug(`Redirecting to ${nextUrl}`)
        window.location.href = nextUrl
        return loadingComponent.render(state, emit)
      } else {
        logger.debug(`Redirecting to homepage`)
        return 'redirectToHomepage'
      }
    }
  },
}
