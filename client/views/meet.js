const h = require('@arve.knudsen/hyperscript')
const sheetify = require('sheetify')
const t = require('tcomb')
const {isNullOrBlank,} = require('@arve.knudsen/stringutils')
const assert = require('assert')
const isEmpty = require('ramda/src/isEmpty')
const MarkdownSanitizer = require('@arve.knudsen/pagedown/Markdown.Sanitizer')
const {DateTime,} = require('luxon')
const map = require('ramda/src/map')
const flatten = require('ramda/src/flatten')
const pipe = require('ramda/src/pipe')
const prepend = require('ramda/src/prepend')
const reject = require('ramda/src/reject')
const addIndex = require('ramda/src/addIndex')
const propEq = require('ramda/src/propEq')
const any = require('ramda/src/any')
const find = require('ramda/src/find')
const mergeRight = require('ramda/src/mergeRight')

const {isGroupAdmin,} = require('../groups')
const insertErrorNotificationPane = require('../insertErrorNotificationPane')
const focusedElement = require('../focusedElement')
const modalComponent = require('../components/modal')
const loadingComponent = require('../components/loading')(['views', 'meet', 'loading',])
const createIconElem = require('../createIconElem')
const ajax = require('../ajax')
const logger = require('../logger').child({module: 'views/meet',})

const sheet = sheetify('./meet.styl')

const domParser = new DOMParser()

const renderMarkdown = (markdown) => {
  t.String(markdown, ['markdown',])
  const converter = MarkdownSanitizer.getSanitizingConverter()
  const html = converter.makeHtml(markdown)
  const dom = domParser.parseFromString(`<div>${html}</div>`, 'text/html')
  if (dom == null) {
    logger.error(`Failed to parse HTML: %s`, html)
    throw new Error(`Failed to parse HTML`)
  }

  return dom.body.children[0]
}

const renderAttendanceControls = (state, isOrganizer, emit) => {
  t.Object(state, ['state',])
  t.Object(state.global, ['state', 'global',])
  t.Function(emit, ['emit',])
  const {meet,} = state
  t.Array(meet.attendees, ['state', 'meet', 'attendees',])
  const {loggedInUser,} = state.global

  if (meet.ends.diffNow().valueOf() < 0) {
    logger.debug(`Not rendering attendance controls control since meet has ended`, {
      ends: meet.ends,
    })
    return null
  }

  if (isOrganizer) {
    logger.debug(`The user is among the organizers - not rendering attendance controls`)
    return null
  }
  if (loggedInUser == null) {
    logger.debug(`User not logged in - not rendering attendance controls`)
    return null
  }
  if (state.isRegisteringAttendance) {
    logger.debug(`User attendance being registered, rendering loader`)
    return h(`#attendance-controls`, [
      loadingComponent.render(state, emit),
    ])
  }

  const isAttending = any(propEq('username', loggedInUser.username), meet.attendees)
  logger.debug({isAttending,}, `User logged in - rendering attendance controls`)

  return h(`#attendance-controls`, [
    h(`.button-group`, [
      h(`button#attend-button`, {
        onclick: () => {
          if (isAttending) {
            logger.debug(`User clicked button to no longer attend meet`)
            state.isRegisteringAttendance = true
            emit.triggerRender()
            ajax.delete(`/api/meets/${meet.uuid}/attendees/${loggedInUser.username}`)
              .then(() => {
                meet.attendees = reject(propEq('username', loggedInUser.username), meet.attendees)
                logger.debug(`Succesfully registered user as not attending meet`)
              })
              .finally(() => {
                state.isRegisteringAttendance = false
                emit.triggerRender()
              })
          } else {
            logger.debug(`User clicked button to attend meet`)
            state.isRegisteringAttendance = true
            emit.triggerRender()
            ajax.postJson(`/api/meets/${meet.uuid}/attendees`, {})
              .then(() => {
                meet.attendees.push(loggedInUser)
                logger.debug(`Succesfully registered user as attending meet`)
              })
              .finally(() => {
                state.isRegisteringAttendance = false
                emit.triggerRender()
              })
          }
        },
      }, [
        isAttending ? `Attending` : `Attend`,
      ]),
    ]),
  ])
}

const renderCancelControl = (state, emit) => {
  t.Object(state, ['state',])
  const {meet,} = state
  t.Object(meet, ['state', 'meet',])
  t.Function(emit, ['emit',])

  if (meet.ends.diffNow().valueOf() < 0) {
    logger.debug(`Not rendering cancel control since meet has ended`, {
      ends: meet.ends,
    })
    return null
  }

  return meet.canceled == null ? h(`a#meet-cancel-control`, {
    href: '#',
    onclick: () => {
      if (state.isCancelingMeet || state.isDeletingMeet) {
        logger.debug(
          `User clicked control for canceling meet while cancelation or deletion in ` +
          `progress - ignoring`)
        return
      }

      logger.debug(`User clicked control for canceling meet`)
      state.shouldShowCancelMeetConfirmationDialog = true
      emit.triggerRender()
    },
  }, `Cancel Meet`) : h(`a#meet-uncancel-control`, {
    href: '#',
    onclick: () => {
      if (state.isCancelingMeet || state.isDeletingMeet) {
        logger.debug(
          `User clicked control for un-canceling meet while cancelation or deletion in ` +
          `progress - ignoring`)
        return
      }

      logger.debug(`User clicked control for un-canceling meet`)
      state.shouldShowUnCancelMeetConfirmationDialog = true
      emit.triggerRender()
    },
  }, `Un-Cancel Meet`)
}

const renderRightColumn = (state, emit) => {
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])
  const {meet,} = state
  const {loggedInUser,} = state.global
  const startTime = meet.starts.toLocal()
  const endTime = meet.ends.toLocal()
  const endDateAndTimeStr = !endTime.hasSame(startTime, 'day') ? endTime.toLocaleString({
    year: 'numeric',
    month: 'long',
    day: 'numeric',
    hour: 'numeric',
    minute: '2-digit',
  }) : endTime.toLocaleString({
    hour: 'numeric',
    minute: '2-digit',
  })
  const dateAndTime = `${startTime.toLocaleString({
    year: 'numeric',
    month: 'long',
    day: 'numeric',
    hour: 'numeric',
    minute: '2-digit',
  })} - ${endDateAndTimeStr}`
  let hosts = meet.hosts
  if (isEmpty(hosts)) {
    hosts = map((host) => {
      return mergeRight(host, {
        isExternal: true,
      })
    }, meet.externalHosts)
    t.Array(hosts, ['meet.externalHosts',])
  } else {
    t.Array(hosts, ['meet.hosts',])
  }
  const isOrganizer = loggedInUser != null && any((organizer) => {
    return !organizer.isExternal ? organizer.username === loggedInUser.username : organizer.name ===
      loggedInUser.name
  }, hosts)
  if (isOrganizer) {
    // Put organizer corresponding to logged in user at the front
    const organizer = find((organizer) => {
      return !organizer.isExternal ? organizer.username === loggedInUser.username : organizer.name ===
        loggedInUser.name
    }, hosts)
    assert.notEqual(organizer, null)
    hosts = reject((organizer) => {
      return !organizer.isExternal ? organizer.username === loggedInUser.username : organizer.name ===
        loggedInUser.name
    }, hosts)
    hosts.unshift(organizer)
    assert.ok(hosts.length > 0)
  }
  const hostElems = pipe(
    addIndex(map)((organizer, i) => {
      t.Object(organizer, ['organizer',])
      let name = organizer.name
      if (loggedInUser != null) {
        const isOrganizer = !organizer.isExternal ? organizer.username === loggedInUser.username :
          organizer.name === loggedInUser.name
        if (isOrganizer) {
          name = 'you'
        }
      }

      if (i === 0) {
        if (!organizer.isExternal) {
          return h(`a`, {href: `/u/${organizer.username}`,}, name)
        }
        return name
      }

      if (i === hosts.length - 1) {
        return [` and `, name,]
      }
      return [`, `, name,]
    }),
    flatten,
    prepend(`Organized by `)
  )(hosts)
  const isAttending = loggedInUser != null && any(propEq('username', loggedInUser.username),
    meet.attendees)
  let attendees = loggedInUser == null ? meet.attendees : reject(
    propEq('username', loggedInUser.username), meet.attendees)
  logger.debug(`The attendees`, attendees)
  if (isEmpty(attendees)) {
    attendees = loggedInUser == null ? meet.externalAttendees : reject(
      propEq('name', loggedInUser.name), meet.externalAttendees)
  }
  const hasHappened = meet.ends.diffNow().valueOf() <= 0
  let attendanceStr
  if (!hasHappened) {
    if (!isEmpty(attendees)) {
      if (isAttending) {
        attendanceStr = attendees.length == 1 ? `You and one other person is attending` :
          `You and ${attendees.length} others are attending`
      } else {
        attendanceStr = attendees.length == 1 ? `1 person is attending` :
          `${attendees.length} people are attending`
      }
    } else {
      if (isAttending) {
        attendanceStr = `Only you have signed up so far`
      } else {
        attendanceStr = `Noone has signed up yet`
      }
    }
  } else {
    attendanceStr = `Noone went`
    if (!isEmpty(attendees)) {
      if (isAttending) {
        attendanceStr = attendees.length == 1 ? `You and one other person went` :
          `You and ${attendees.length} others went`
      } else {
        attendanceStr = attendees.length == 1 ? `1 person went` :
          `${attendees.length} people went`
      }
    } else {
      if (isAttending) {
        attendanceStr = `Only you went`
      } else {
        attendanceStr == `Noone went`
      }
    }
  }
  const isAuthorizedToEdit = loggedInUser != null && (isGroupAdmin(loggedInUser, meet.groupPath) ||
    loggedInUser.isAdmin)

  return h(`#right-column`, [
    h(`#breadcrumbs`, [
      h(`a`, {href: `/g/${meet.groupPath}`,}, meet.groupName),
      h(`span.arrow`, `>`),
      h(`a`, {href: `/m/${meet.uuid}`,}, meet.title),
    ]),
    h(`#meet-pad`, [
      h(`.meet-meta`, [
        h(`#meet-title-container`, [
          h(`#meet-title`, meet.title),
          h(`#meet-organizers`, hostElems),
          meet.canceled == null ? null : h(`#meet-canceled`, [
            h('em', `The meet was canceled on ${meet.canceled.toLocaleString(DateTime.DATE_FULL)}`),
          ]),
          renderAttendanceControls(state, isOrganizer, emit),
        ]),
        isAuthorizedToEdit ? [
          h('hr'),
          h(`#meet-edit`, [
            createIconElem('pencil', {classes: ['meta-icon',],}),
            h(`a#meet-edit-control`, {href: `/m/${meet.uuid}/edit`,}, `Edit Meet`),
            renderCancelControl(state, emit),
            h(`a#meet-delete-control`, {
              href: `#`,
              onclick: () => {
                logger.debug(`User clicked control for deleting meet`)
                state.shouldShowDeleteMeetConfirmationDialog = true
                emit.triggerRender()
              },
            }, `Delete Meet`),
          ]),
        ] : null,
        h('hr'),
        h(`#meet-time`, [
          createIconElem('clock', {classes: ['meta-icon',],}),
          dateAndTime,
        ]),
        h('hr'),
        !meet.isOnline ? renderLocation(state, emit) : renderOnline(state, emit),
        h('hr'),
        h(`#attendees`, [
          createIconElem('users', {classes: ['meta-icon',],}),
          attendanceStr,
        ]),
        h('hr'),
      ]),
      h(`#meet-description-container`, [
        h(`#meet-description`, renderMarkdown(meet.description)),
      ]),
    ]),
  ])
}

const renderOnline = (state, emit) => {
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])
  return h(`#meet-online`, [
    createIconElem('sphere', {classes: ['meta-icon',],}),
    `Online Meet`,
  ])
}

const renderLocation = (state, emit) => {
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])
  const {meet,} = state
  const mapUrl = encodeURI(`https://google.com/maps/search/?api=1&query=${
      meet.location.address}&query_place_id=${meet.location.placeId}`)
  return h(`#meet-location`, [
    h(`#meet-location-inner`, {
      onclick: () => {
        logger.debug(`Location clicked, toggling map`)
        state.shouldShowMap = !state.shouldShowMap
        emit.triggerRender()
      },
    }, [
      createIconElem('location', {classes: ['meta-icon',],}),
      h(`#location-name-and-address`, [
        h(`#location-name`, meet.location.name),
        h(`#location-address`, meet.location.address),
      ]),
    ]),
    h(`#location-map-wrapper${state.shouldShowMap ? '' : '.hidden'}`, h(`a`, {
      href: mapUrl,
      target: `_blank`,
      rel: `noreferrer noopener`,
    }, h(`img`, {
      src: meet.location.mapUrl,
    }))),
  ])
}

module.exports = {
  name: 'meetView',
  route: '/m/:uuid',
  resetState: (state) => {
    t.Object(state, ['state',])
    logger.debug(`Resetting state`)
    state.shouldShowDeleteMeetConfirmationDialog = false
    state.shouldShowCancelMeetConfirmationDialog = false
    state.shouldShowUnCancelMeetConfirmationDialog = false
    state.isDeletingMeet = false
    state.isCancelingMeet = false
  },
  loadData: async (state, emit) => {
    t.Object(state, ['state',])
    t.Function(emit, ['emit',])
    const {uuid,} = state.params
    assert.ok(!isNullOrBlank(uuid))
    logger.debug(`Loading meet ${uuid}...`)
    const meet = await ajax.getJson(`/api/meets/${uuid}`)
    meet.starts = DateTime.fromISO(meet.starts)
    assert.ok(meet.starts.isValid)
    meet.ends = DateTime.fromISO(meet.ends)
    assert.ok(meet.ends.isValid)
    if (meet.canceled != null) {
      meet.canceled = DateTime.fromISO(meet.canceled)
      assert.ok(meet.canceled.isValid)
    }
    logger.debug(`Successfully loaded meet '${uuid}'`)
    state.meet = meet
  },
  render: (state, emit) => {
    t.Object(state, ['state',])
    t.Object(state.global, ['state', 'global',])
    t.Function(emit, ['emit',])
    const {meet,} = state
    t.Object(meet, ['state', 'meet',])

    logger.debug({meet,}, `Rendering meet ${meet.uuid}`)
    return h(`.${sheet}`, [
      h('#meet-view', [
        renderRightColumn(state, emit),
      ]),
      state.shouldShowCancelMeetConfirmationDialog ?
        renderCancelMeetConfirmationDialog(state, emit) : null,
      state.shouldShowUnCancelMeetConfirmationDialog ?
        renderUnCancelMeetConfirmationDialog(state, emit) : null,
      state.shouldShowDeleteMeetConfirmationDialog ?
        renderDeleteMeetConfirmationDialog(state, emit) : null,
    ])
  },
}

const cancelMeet = async (state, emit) => {
  t.Object(state, ['state',])
  t.Object(state.global, ['state', 'global',])
  t.Function(emit, ['emit',])
  state.isCancelingMeet = true
  emit.triggerRender()

  logger.debug({meet: state.meet,}, `Submitting request to server to cancel meet`)
  try {
    await ajax.putJson(`/api/meets/${state.meet.uuid}/canceled`, {
      canceled: true,
    })
  } catch (err) {
    state.isCancelingMeet = false
    emit.triggerRender()
    throw err
  }

  state.isCancelingMeet = false
  state.meet.canceled = DateTime.local()
  // Prune client side cache
  delete state.global.components.homeView.loadingState
  delete state.global.components.groupProfileView.loadingState
  emit.triggerRender()
}

const unCancelMeet = async (state, emit) => {
  t.Object(state, ['state',])
  t.Object(state.global, ['state', 'global',])
  t.Function(emit, ['emit',])
  state.isCancelingMeet = true
  emit.triggerRender()

  logger.debug({meet: state.meet,}, `Submitting request to server to un-cancel meet`)
  try {
    await ajax.putJson(`/api/meets/${state.meet.uuid}/canceled`, {
      canceled: false,
    })
  } catch (err) {
    state.isCancelingMeet = false
    emit.triggerRender()
    throw err
  }

  state.isCancelingMeet = false
  state.meet.canceled = null
  // Prune client side cache
  delete state.global.components.homeView.loadingState
  delete state.global.components.groupProfileView.loadingState
  emit.triggerRender()
}

const deleteMeet = async (state, emit) => {
  t.Object(state, ['state',])
  t.Object(state.global, ['state', 'global',])
  t.Function(emit, ['emit',])
  state.isDeletingMeet = true
  emit.triggerRender()

  logger.debug({meet: state.meet,}, `Submitting request to server to delete meet`)
  try {
    await ajax.delete(`/api/meets/${state.meet.uuid}`)
  } catch (err) {
    state.isDeletingMeet = false
    emit.triggerRender()
    throw err
  }

  state.isDeletingMeet = false

  // Prune client side cache
  delete state.global.components.homeView.loadingState
  delete state.global.components.groupProfileView.loadingState

  emit.global('pushState', `/`)
}

const renderCancelMeetConfirmationDialog = (state, emit) => {
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])

  const closeModal = () => {
    state.shouldShowCancelMeetConfirmationDialog = false
    emit.triggerRender()
  }

  logger.debug(`Rendering dialog to confirm canceling of meet`)
  return modalComponent({
    closeCallback: closeModal,
    headerLabel: 'Cancel Meet?',
    content: [
      h('.modal-body-content', [
        h('p', `Are you sure you wish to cancel this meet?`),
      ]),
      h('.button-group', [
        focusedElement('button#modal-yes', {
          onclick: () => {
            logger.debug(`Yes button clicked - canceling meet...`)
            cancelMeet(state, emit)
              .catch((error) => {
                logger.error({error,}, `Failed to cancel meet`)
                insertErrorNotificationPane(`Canceling meet failed!`, `${error}.`)
              })
          },
        }, 'Yes'),
        h('button#modal-no.pure-button', {
          onclick: () => {
            logger.debug(`No button clicked`)
            closeModal()
          },
        }, 'No'),
      ]),
    ],
  })
}

const renderUnCancelMeetConfirmationDialog = (state, emit) => {
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])

  const closeModal = () => {
    state.shouldShowUnCancelMeetConfirmationDialog = false
    emit.triggerRender()
  }

  logger.debug(`Rendering dialog to confirm un-canceling of meet`)
  return modalComponent({
    closeCallback: closeModal,
    headerLabel: 'Un-Cancel Meet?',
    content: [
      h('.modal-body-content', [
        h('p', `Are you sure you wish to un-cancel this meet?`),
      ]),
      h('.button-group', [
        focusedElement('button#modal-yes', {
          onclick: () => {
            logger.debug(`Yes button clicked - un-canceling meet...`)
            unCancelMeet(state, emit)
              .catch((error) => {
                logger.error({error,}, `Failed to un-cancel meet`)
                insertErrorNotificationPane(`Un-canceling meet failed!`, `${error}.`)
              })
          },
        }, 'Yes'),
        h('button#modal-no.pure-button', {
          onclick: () => {
            logger.debug(`No button clicked`)
            closeModal()
          },
        }, 'No'),
      ]),
    ],
  })
}

const renderDeleteMeetConfirmationDialog = (state, emit) => {
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])

  const closeModal = () => {
    state.shouldShowDeleteMeetConfirmationDialog = false
    emit.triggerRender()
  }

  logger.debug(`Rendering dialog to confirm deletion of meet`)
  return modalComponent({
    closeCallback: closeModal,
    headerLabel: 'Delete Meet?',
    content: [
      h('.modal-body-content', [
        h('p', `Are you really sure you wish to delete this meet? This cannot be undone!`),
      ]),
      h('.button-group', [
        focusedElement('button#modal-yes', {
          onclick: () => {
            logger.debug(`Yes button clicked - deleting meet...`)
            deleteMeet(state, emit)
              .catch((error) => {
                logger.error({error,}, `Failed to delete meet`)
                insertErrorNotificationPane(`Deleting meet failed!`, `${error}.`)
              })
          },
        }, 'Yes'),
        h('button#modal-no.pure-button', {
          onclick: () => {
            logger.debug(`No button clicked`)
            closeModal()
          },
        }, 'No'),
      ]),
    ],
  })
}
