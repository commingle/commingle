const t = require('tcomb')
const {isNullOrBlank,} = require('@arve.knudsen/stringutils')
const forEach = require('ramda/src/forEach')
const values = require('ramda/src/values')

const loadingComponent = require('../components/loading')(['logoutView', 'loading',])
const ajax = require('../ajax')
const logger = require('../logger').child({module: 'views/logout',})

module.exports = {
  name: 'logoutView',
  route: '/logout',
  render: (state, emit) => {
    t.Object(state, ['state',])
    t.Object(state.global, ['state', 'global',])
    t.Function(emit, ['emit',])

    logger.debug(`Logging user out...`)
    state.isLoading = true
    ajax.postJson('/api/logout').then(() => {
      logger.debug(`User logout succeeded`)
      state.global.loggedInUser = null
      redirect(state, emit)
    }, (err) => {
      logger.error(`User logout failed`, {err,})
      redirect(state, emit)
    })

    return loadingComponent.render(state, emit)
  },
}

const redirect = (state, emit) => {
  state.isLoading = false

  logger.debug(`Clearing view caches, so views are reloaded wrt. user being logged out`)
  t.Object(state.global.components, ['state', 'global', 'components',])
  forEach((componentState) => {
    delete componentState.loadingState
  }, values(state.global.components))

  emit.triggerRender()

  if (!isNullOrBlank(state.query.next)) {
    const nextUrl = decodeURIComponent(state.query.next)
    logger.debug(`Redirecting to ${nextUrl}`)
    emit.global('pushState', nextUrl)
  } else {
    logger.debug(`Redirecting to homepage`)
    emit.global('pushState', '/')
  }
}
