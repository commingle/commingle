const h = require('@arve.knudsen/hyperscript')
const t = require('tcomb')
const sheetify = require('sheetify')
const titleize = require('titleize')
const map = require('ramda/src/map')
const assert = require('assert')
const pipe = require('ramda/src/pipe')
const toPairs = require('ramda/src/toPairs')
const fromPairs = require('ramda/src/fromPairs')
const filter = require('ramda/src/filter')
const isEmpty = require('ramda/src/isEmpty')
const forEach = require('ramda/src/forEach')

const ajax = require('../ajax')
const logger = require('../logger').child({module: 'views/settings',})

const sheet = sheetify('./settings.styl')

const getInputState = (state) => {
  t.Object(state, ['state',])
  t.Object(state.global, ['state', 'global',])
  const {loggedInUser,} = state.global
  t.Object(loggedInUser, ['state', 'global', 'loggedInUser',])

  return {
    name: loggedInUser.name,
    bio: loggedInUser.bio,
  }
}

module.exports = {
  name: 'settingsView',
  route: '/settings',
  requiresAuthentication: true,
  resetState: (state) => {
    t.Object(state, ['state',])
    state.activeTab = 'profile'
    state.isSubmissionEnabled = true
    state.input = getInputState(state)
    logger.debug(`Reset state`, {state,})
  },
  render: (state, emit) => {
    t.Object(state, ['state',])
    t.Object(state.global, ['state', 'global',])
    const {loggedInUser,} = state.global
    t.Object(loggedInUser, ['state', 'global', 'loggedInUser',])
    t.Function(emit, ['emit',])

    logger.debug(`Rendering settings page of user '${loggedInUser.username}'`, {state,})
    return h(`.${sheet}`, [
      renderLeftColumn(state, emit),
      renderRightColumn(state, emit),
    ])
  },
}

const renderLeftColumn = (state, emit) => {
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])
  return h('#left-column', h('#settings-tabs', h('ul', map((tab) => {
    return h(`li${tab === state.activeTab ? '.active' : ''}`, {
      onclick: () => {
        logger.debug(`User clicked tab '${tab}'`)
        state.activeTab = tab
        state.input = getInputState(state)
        emit.triggerRender()
      },
    }, titleize(tab))
  }, [
    'profile',
    'security',
  ]))))
}

const submit = async (state, emit) => {
  t.Object(state, ['state',])
  t.Object(state.global, ['state', 'global',])
  t.Function(emit, ['emit',])
  const {loggedInUser,} = state.global
  t.Object(loggedInUser, ['state', 'global', 'loggedInUser',])

  const patch = pipe(
    toPairs,
    filter(([key, value,]) => {
      t.String(key, [key,])
      if (value != null) {
        t.String(value, ['state', 'input', key,])
      } else {
        // Normalize
        value = null
      }
      let oldValue = loggedInUser[key]
      if (oldValue != null) {
        t.String(oldValue, ['state', 'global', 'loggedInUser', key,])
      } else {
        // Normalize
        oldValue = null
      }
      return value !== oldValue
    }),
    fromPairs
  )(state.input)

  if (isEmpty(patch)) {
    logger.debug(`Not sending request to patch user, since there are no updates`)
    return
  }


  state.isSubmissionEnabled = false
  emit.triggerRender()
  logger.debug(`Sending request to patch user '${loggedInUser.username}'`, {
    patch,
  })
  let user
  try {
    user = await ajax.patchJson(`/api/users/${loggedInUser.username}`, patch)
  } finally {
    state.isSubmissionEnabled = true
    emit.triggerRender()
  }

  logger.debug(`Successfully updated user`, {
    result: user,
  })
  pipe(
    toPairs,
    forEach(([key, value,]) => {
      loggedInUser[key] = value
    })
  )(user)
}

const renderProfile = (state, emit) => {
  t.Object(state, ['state',])
  t.Object(state.input, ['state', 'input',])
  t.Function(emit, ['emit',])
  return [
    h('h2', 'Public Profile'),
    h(`form#public-profile-form.settings-form`, [
      h(`#edit-name`, [
        h(`label`, `Name`),
        h(`input`, {
          type: 'text',
          placeholder: 'Your name',
          value: state.input.name,
          oninput: (event) => {
            logger.debug(`Name changed: '${event.target.name}'`)
            state.input.name = event.target.value
          },
        }),
      ]),
      h(`#edit-bio`, [
        h(`label`, `Bio`),
        h(`textarea`, {
          value: state.input.bio,
          oninput: (event) => {
            logger.debug(`Bio changed: '${event.target.value}'`)
            state.input.bio = event.target.value
          },
        }),
      ]),
    ]),
    h(`button#update-profile-button.button.primary-button`, {
      type: 'submit',
      disabled: !state.isSubmissionEnabled,
      onclick: (event) => {
        event.preventDefault()
        logger.debug(`Submitting public profile form`)
        submit(state, emit)
          .then(() => {
            logger.info(`Successfully submitted public profile form`)
          })
          .catch((err) => {
            // TODO: Display error
            logger.error(`Failed to submit public profile form`, {err,})
          })
      },
    }, `Update Profile`),
  ]
}

const submitPassword = async (state, emit) => {
  t.Object(state, ['state',])
  t.Object(state.global, ['state', 'global',])
  const {loggedInUser,} = state.global
  t.Object(loggedInUser, ['state', 'global', 'loggedInUser',])
  t.Function(emit, ['emit',])

  state.isSubmissionEnabled = false
  emit.triggerRender()
  logger.debug(`Sending request to update password of '${loggedInUser.username}'`)
  try {
    await ajax.putJson(`/api/users/${loggedInUser.username}/password`, {
      oldPassword: state.oldPassword,
      newPassword: state.newPassword,
    })
  } finally {
    state.isSubmissionEnabled = true
    emit.triggerRender()
  }

  logger.info(`Successfully updated user password`)
}

const evaluatePasswords = (state, emit) => {
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])

  if (state.oldPassword === '' || state.newPassword === '' || state.confirmNewPassword === '') {
    state.canUpdatePassword = false
    emit.triggerRender()
    return
  }

  if (state.newPassword !== state.confirmNewPassword) {
    // TODO: Inform user
    state.canUpdatePassword = false
    emit.triggerRender()
    return
  }

  state.canUpdatePassword = true
  emit.triggerRender()
}

const renderSecurity = (state, emit) => {
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])

  return [
    h('h2', 'Change Password'),
    h(`form#password-form.settings-form`, [
      h(`#old-password`, [
        h(`label`, `Old Password`),
        h(`input`, {
          type: 'password',
          placeholder: 'Old password',
          value: state.oldPassword,
          oninput: (event) => {
            logger.debug(`Old password changed: '${event.target.name}'`)
            state.oldPassword = event.target.value
            evaluatePasswords(state, emit)
          },
        }),
      ]),
      h(`#new-password`, [
        h(`label`, `New Password`),
        h(`input`, {
          type: 'password',
          placeholder: 'New password',
          value: state.newPassword,
          oninput: (event) => {
            logger.debug(`New password changed: '${event.target.value}'`)
            state.newPassword = event.target.value
            evaluatePasswords(state, emit)
          },
        }),
      ]),
      h(`#confirm-password`, [
        h(`label`, `Confirm New Password`),
        h(`input`, {
          type: 'password',
          placeholder: 'Confirm new password',
          value: state.confirmNewPassword,
          oninput: (event) => {
            logger.debug(`Confirm new password changed: '${event.target.value}'`)
            state.confirmNewPassword = event.target.value
            evaluatePasswords(state, emit)
          },
        }),
      ]),
    ]),
    h(`button#update-password-button.button.primary-button`, {
      type: 'submit',
      disabled: !state.isSubmissionEnabled || !state.canUpdatePassword,
      onclick: (event) => {
        event.preventDefault()

        assert.notStrictEqual(state.oldPassword, '')
        assert.notStrictEqual(state.newPassword, '')
        assert.strictEqual(state.newPassword, state.confirmNewPassword)

        logger.debug(`Submitting password form`)
        submitPassword(state, emit)
          .catch((err) => {
            // TODO: Display error
            logger.error(`Submitting password failed`, {err,})
          })
      },
    }, `Update Password`),
  ]
}

const renderRightColumn = (state, emit) => {
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])
  let renderer
  if (state.activeTab === 'profile') {
    renderer = renderProfile
  } else if (state.activeTab === 'security') {
    renderer = renderSecurity
  }
  return h('#right-column', [
    h(`#top-section`, [
      h(`#breadcrumbs`, [
        h(`a`, {href: `/settings`,}, `Settings`),
      ]),
      h(`hr`),
    ]),
    h(`#settings-tab-content`, renderer(state, emit)),
  ])
}
