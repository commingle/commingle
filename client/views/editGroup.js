const h = require('@arve.knudsen/hyperscript')
const sheetify = require('sheetify')
const t = require('tcomb')
const debounce = require('debounce')
const trim = require('ramda/src/trim')
const {isNullOrBlank,} = require('@arve.knudsen/stringutils')
const assert = require('assert')
const mergeRight = require('ramda/src/mergeRight')

const {isGroupAdmin,} = require('../groups')
const insertErrorNotificationPane = require('../insertErrorNotificationPane')
const modalComponent = require('../components/modal')
const focusedElement = require('../focusedElement')
const ajax = require('../ajax')
const logger = require('../logger').child({module: 'views/Group',})

const sheet = sheetify('./editGroup.styl')

const checkForm = debounce((state, emit) => {
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])
  logger.debug(`Checking whether to enable submission...`)

  // TODO

  emit.triggerRender()
}, 300)

const resetState = (state) => {
  t.Object(state, ['state',])
  const {group,} = state
  t.Object(group, ['state', 'group',])

  logger.debug(`Resetting view state`, {group,})

  state.input = {
    name: group.name,
    description: group.description,
    gitlabGroupPath: group.gitlabGroupPath,
  }
  state.isSubmissionEnabled = true
  state.isCancelEnabled = true
  state.errorMessages = {}
  state.shouldShowCancelConfirmationDialog = false
}

module.exports = {
  name: 'editGroupView',
  route: '/g/:path/edit',
  loadData: async (state, emit) => {
    t.Object(state, ['state',])
    t.Function(emit, ['emit',])
    const {path,} = state.params
    assert.ok(!isNullOrBlank(path))
    logger.debug(`Loading group ${path}...`)
    const group = await ajax.getJson(`/api/groups/${path}`)
    state.group = group
    logger.debug(`Successfully loaded group '${path}'`)
  },
  resetState,
  render: (state, emit) => {
    t.Object(state, ['state',])
    t.Object(state.global, ['state', 'global',])
    t.Function(emit, ['emit',])
    const {group,} = state
    const {loggedInUser,} = state.global

    const isPermitted = loggedInUser != null && (loggedInUser.isAdmin ||
      isGroupAdmin(loggedInUser, group.path))
    if (!isPermitted) {
      if (loggedInUser != null) {
        logger.debug(`The logged in user isn't authorized to edit the group, returning to homepage`)
      }
      return 'redirectToHomepage'
    }

    logger.debug({group,}, `Rendering form for editing group ${group.path}`)
    return h(`.${sheet}`, h('#edit-group-view', [
      renderRightColumn(state, emit),
      state.shouldShowCancelConfirmationDialog ?
        renderCancelConfirmationDialog(state, emit) : null,
    ]))
  },
}

const renderDescriptionEditor = (state, emit) => {
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])

  const inputValue = state.input.description
  t.String(inputValue, ['state', 'input', 'description',])
  const errorMsg = state.errorMessages.description
  return [
    h(`fieldset#edit-group-description-fieldset`, [
      h(`legend`, `Description`),
      h(`textarea#description-editor`, {
        value: state.input.description,
        required: true,
        oninput: (event) => {
          state.input.description = event.target.value
          checkForm(state, emit)
        },
      }),
      errorMsg != null ? h('.error-message', errorMsg) : null,
    ]),
  ]
}

const renderRightColumn = (state, emit) => {
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])

  return h(`#right-column`, [
    h(`#edit-group-pad`, [
      h(`h1`, `Edit Group`),
      h(`form#edit-group-form`, [
        h('#edit-name', [
          h('label', {
            for: 'name',
          }, 'Name'),
          renderInput('name', 'Name', state, emit, {
            focus: true,
          }),
        ]),
        h('#edit-gitlab-group', [
          h('label', {
            for: 'gitlabGroupPath',
          }, 'GitLab Group'),
          renderInput('gitlabGroupPath', 'GitLab Group Path', state, emit),
        ]),
        renderDescriptionEditor(state, emit),
      ]),
      h(`hr`),
      h(`#form-buttons`, [
        h(`button#submit-edit-group-button`, {
          type: 'submit',
          disabled: !state.isSubmissionEnabled,
          onclick: (event) => {
            event.preventDefault()
            logger.debug(`Submit button clicked`)
            submitEditGroup(state, emit)
          },
        }, `Save`),
        h(`button#cancel-edit-group-button`, {
          disabled: !state.isCancelEnabled,
          onclick: (event) => {
            event.preventDefault()
            logger.debug(`Cancel button clicked`)
            state.shouldShowCancelConfirmationDialog = true
            emit.triggerRender()
          },
        }, `Cancel`),
      ]),
    ]),
  ])
}

const renderInput = (name, placeholder, state, emit, opts={}) => {
  t.String(name, ['name',])
  t.String(placeholder, ['placeholder',])
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])
  const focus = opts.focus
  delete opts.focus

  const inputValue = state.input[name]
  if (inputValue != null) {
    t.String(inputValue, ['state', 'input', name,])
  }
  const errorMsg = state.errorMessages[name]
  const renderer = focus ? focusedElement : h
  return [
    renderer(`input#input-${name.toLowerCase()}.edit-group-input`, mergeRight({
      name,
      placeholder,
      value: inputValue,
      required: true,
      oninput: (event) => {
        state.input[name] = event.target.value
        checkForm(state, emit)
      },
    }, opts)),
    errorMsg != null ? h('.error-message', errorMsg) : null,
  ]
}

const renderCancelConfirmationDialog = (state, emit) => {
  t.Object(state, ['state',])
  t.Object(state.group, ['state', 'group',])
  const {group,} = state
  t.Function(emit, ['emit',])

  const closeModal = () => {
    state.shouldShowCancelConfirmationDialog = false
    emit.triggerRender()
  }

  logger.debug(`Rendering dialog to confirm canceling of group editing`)
  return modalComponent({
    closeCallback: closeModal,
    headerLabel: 'Cancel Editing Group?',
    content: [
      h('.modal-body-content', [
        h('p', `Are you sure you wish to cancel editing this group?`),
      ]),
      h('.button-group', [
        focusedElement('button#modal-yes.pure-button.pure-button-primary', {
          onclick: () => {
            logger.debug(`Yes button clicked - redirecting to group page`)
            emit.global('pushState', `/g/${group.path}`)
          },
        }, 'Yes'),
        h('button#modal-no.pure-button', {
          onclick: () => {
            logger.debug(`No button clicked`)
            closeModal()
          },
        }, 'No'),
      ]),
    ],
  })
}

const submitEditGroup = (state, emit) => {
  t.Object(state, ['state',])
  t.Object(state.group, ['state', 'group',])
  t.Function(emit, ['emit',])

  logger.debug({state,}, `Submitting edited group...`)
  const name = trim(state.input.name)
  const description = trim(state.input.description)
  let gitlabGroupPath = trim(state.input.gitlabGroupPath || '')
  if (isNullOrBlank(gitlabGroupPath)) {
    gitlabGroupPath = null
  }
  assert.ok(!isNullOrBlank(name))
  assert.ok(!isNullOrBlank(description))
  const groupInput = {
    name,
    description,
    gitlabGroupPath,
  }

  submitToServer(groupInput, state, emit)
    .catch((err) => {
      logger.error('Submitting group edit to server failed', {err,})
      // TODO: Display error to user
    })
}

const submitToServer = async (groupInput, state, emit) => {
  t.Object(groupInput, ['groupInput',])
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])

  state.isSubmissionEnabled = false
  state.isCancelEnabled = false
  emit.triggerRender()

  logger.debug({input: groupInput,}, `Submitting group ${state.group.path} to server`)
  let group
  try {
    group = await ajax.putJson(`/api/groups/${state.group.path}`, groupInput)
  } catch (error) {
    logger.error({error,}, `Editing group failed on server`)
    if (error.data != null) {
      t.Object(error.data, ['error', 'data',])
      state.isLoading = false
      state.errorMessages = error.data
    } else {
      insertErrorNotificationPane('Editing Group Failed:', [error.message,])
    }

    state.isSubmissionEnabled = true
    state.isCancelEnabled = true
    emit.triggerRender()
    return
  }

  logger.debug({group,}, `Group was successfully edited`)
  // Reset the loading state for group view, so that this group gets reloaded and we're not
  // stuck with stale data
  delete state.global.components.groupProfileView.loadingState
  state.group = group
  state.isSubmissionEnabled = true
  state.isCancelEnabled = true
  emit.global('pushState', `/g/${group.path}`)
}
