const h = require('@arve.knudsen/hyperscript')
const sheetify = require('sheetify')
const t = require('tcomb')
const map = require('ramda/src/map')
const titleize = require('titleize')
const jdenticon = require('jdenticon')
const Promise = require('bluebird')
const concat = require('ramda/src/concat')
const isEmpty = require('ramda/src/isEmpty')
const pipe = require('ramda/src/pipe')
const toPairs = require('ramda/src/toPairs')
const groupBy = require('ramda/src/groupBy')
const any = require('ramda/src/any')
const forEach = require('ramda/src/forEach')
const propEq = require('ramda/src/propEq')
const reject = require('ramda/src/reject')
const {DateTime,} = require('luxon')
const pluralize = require('pluralize')

const loadingComponent = require('../components/loading')(['views', 'home', 'loading',])
const createIconElem = require('../createIconElem')
const logger = require('../logger').child({module: 'views/home',})
const ajax = require('../ajax')

const sheet = sheetify('./home.styl')

module.exports = {
  name: 'homeView',
  route: '/',
  resetState: (state) => {
    t.Object(state.global, ['state', 'global',])
    const {loggedInUser,} = state.global
    if (loggedInUser == null) {
      state.activeTab = `explore`
    } else {
      state.activeTab = `agenda`
    }
    state.registeringAttendance = null
  },
  loadData: async (state, emit) => {
    t.Object(state, ['state',])
    t.Object(state.global, ['state', 'global',])
    const {loggedInUser,} = state.global
    t.Function(emit, ['emit',])
    let promises = [
      ajax.getJson(`/api/groups`),
    ]
    if (loggedInUser != null) {
      promises.push(ajax.getJson(`/api/meets?user=${loggedInUser.username}`))
    }
    const results = await Promise.all(promises)
    logger.debug(`Successfully loaded data`)
    state.filteredGroups = results[0]
    if (loggedInUser != null) {
      state.upcomingMeets = results[1]
    }
  },
  render: (state, emit) => {
    t.Object(state, ['state',])
    t.Object(state.global, ['state', 'global',])
    t.Function(emit, ['emit',])

    logger.debug(`Rendering home view`)
    return h(`.${sheet}`, [
      renderRightColumn(state, emit),
    ])
  },
}

const renderRightColumn = (state, emit) => {
  t.Object(state, ['state',])
  t.Object(state.global, ['state', 'global',])
  t.Function(emit, ['emit',])
  const {loggedInUser,} = state.global

  let renderer
  if (state.activeTab === `explore`) {
    renderer = renderExplore
  } else if (state.activeTab === `agenda`) {
    renderer = renderAgenda
  } else {
    throw new Error(`Unrecognized tab '${state.activeTab}'`)
  }
  let tabs = ['explore',]
  if (loggedInUser != null) {
    tabs = concat(['agenda',], tabs)
  }
  return h(`#right-column`, [
    // We need a container for the tab elements, so we can make it 100% wide and cover any elements
    // scrolling beneath it
    h(`nav#top-section`, [
      h(`h1#heading`, `Home`),
      h(`hr`),
    ].concat(h(`ul`, map((tab) => {
      t.String(tab, ['tab',])
      let sfx = ''
      if (tab === state.activeTab) {
        sfx = `.active`
      }
      return h(`li#${tab}-tab.tab${sfx}`, {
        onclick: () => {
          if (state.activeTab !== tab) {
            logger.debug(`Switching to ${tab} tab`)
            state.activeTab = tab
            emit.triggerRender()
          }
        },
      }, titleize(tab))
    }, tabs)))),
    h(`#home-tab-content`, [
      renderer(state, emit),
    ]),
  ])
}

const renderExplore = (state) => {
  t.Object(state, ['state',])
  t.Object(state.global, ['state', 'global',])
  const {loggedInUser,} = state.global

  const groups = state.filteredGroups || []
  if (groups.length === 0) {
    return h(`#explore-groups.empty-tab-content`, `No groups found.`)
  }

  return h(`#explore-groups`, map((group) => {
    let membershipStr
    if (loggedInUser != null) {
      forEach((member) => {
        if (membershipStr != null || member.username !== loggedInUser.username) {
          return
        }

        if (member.membership === 'Admin') {
          membershipStr = `You are an admin of this group`
        } else {
          membershipStr = `You are a member of this group`
        }
      }, group.members)
      if (membershipStr == null) {
        membershipStr = `You are not a member of this group`
      }
    }
    const avatarElem = h(`.group-avatar-container`)
    avatarElem.innerHTML = jdenticon.toSvg(group.path, 80)
    return h(`.group-container`, h(`.group-inner`, [
      h(`a`, {href: `/g/${group.path}`,}, avatarElem),
      h(`.group-content`, [
        h(`a`, {href: `/g/${group.path}`,}, [
          h(`.group-name`, group.name),
          h(`.group-description`, group.description),
          h(`.group-meta`, [
            membershipStr != null ? h(`.group-membership`, membershipStr) : null,
            h(`.group-num-members`, `${group.members.length} ${pluralize('members',
              group.members.length)}`),
          ]),
        ]),
      ]),
    ]))
  }, groups))
}

const renderAgenda = (state, emit) => {
  t.Object(state, ['state',])
  t.Array(state.upcomingMeets, ['state', 'upcomingMeets',])
  t.Function(emit, ['emit',])
  const {upcomingMeets: upcoming,} = state

  if (isEmpty(upcoming)) {
    return h(`#upcoming-meets.empty-tab-content`, `No upcoming meets found.`)
  }

  return h(`#upcoming-meets`, h(`ul`, pipe(
    groupBy((meet) => {
      logger.debug(`Grouping by ${DateTime.fromISO(meet.starts).toLocal().toISODate()}`)
      return DateTime.fromISO(meet.starts).toLocal().toISODate()
    }),
    toPairs,
    map(([dateStr, meets,]) => {
      const date = DateTime.fromISO(dateStr).toLocal()
      return h(`li.date-with-meets`, [
        h(`.meet-date`, date.toLocaleString(DateTime.DATE_FULL)),
        h(`ul.meets-for-date`, map((meet) => {
          const starts = DateTime.fromISO(meet.starts).toLocal()
          const ends = DateTime.fromISO(meet.ends).toLocal()
          const endStr = ends.diff(starts).days === 0 ? ends.toLocaleString(DateTime.TIME_SIMPLE) :
            ends.toLocaleString(DateTime.DATETIME_SHORT)
          const timeStr = `${starts.toLocaleString(DateTime.DATETIME_SHORT)} - ${endStr}`
          return h(`li.meet-container`, [
            h(`.meet-content`, [
              h(`.meet-title.meta`, h(`a`, {href: `/m/${meet.uuid}`,}, meet.title)),
              h(`.meet-group.meta`, h(`a.muted`, {href: `/g/${meet.groupPath}`,}, `${meet.groupName}`)),
              h(`.meet-time.meta`, h(`a`, {href: `/m/${meet.uuid}`,}, timeStr)),
              !meet.isOnline ? h(`.meet-location.meta`, [
                createIconElem(`location`, {classes: ['meta-icon',],}),
                h(`a`, {
                  href: `https://www.google.com/maps/search/?api=1&query=${encodeURIComponent(
                    meet.location.name)}&query_place_id=${encodeURIComponent(meet.location.placeId)}`,
                  target: '_blank',
                  rel: `noreferrer noopener`,
                }, meet.location.name),
              ]) : h(`.meet-location.meta`, [
                createIconElem(`sphere`, {classes: ['meta-icon',],}),
                `Online`,
              ]),
              h(`.meet-going`, isEmpty(meet.going) ? `Noone has signed up yet` : [
                createIconElem('users', {classes: ['meta-icon',],}),
                `${meet.attendees.length} going`,
              ]),
              renderAttendanceControls(meet, state, emit),
            ]),
          ])
        }, meets)),
      ])
    })
  )(upcoming)))
}

const renderAttendanceControls = (meet, state, emit) => {
  t.Object(meet, ['meet',])
  t.Array(meet.attendees, ['meet', 'attendees',])
  t.Object(state, ['state',])
  t.Object(state.global, ['state', 'global',])
  t.Function(emit, ['emit',])
  const {loggedInUser,} = state.global
  if (loggedInUser == null) {
    logger.debug(`User not logged in - not rendering attendance controls`)
    return null
  }
  if (any((organizer) => {
    return organizer.username === loggedInUser.username
  }, meet.hosts)) {
    logger.debug(`Logged in user is among the meet's organizers - not rendering attendance controls`)
    return null
  }
  if (state.registeringAttendance === meet.uuid) {
    logger.debug(`User attendance being registered, rendering loader`)
    return h(`#attendance-controls`, [
      loadingComponent.render(state, emit),
    ])
  }

  const isAttending = any(propEq('username', loggedInUser.username), meet.attendees)
  logger.debug({isAttending,}, `User logged in - rendering attendance controls`)

  return h(`#attendance-controls`, [
    h(`.button-group`, [
      h(`button#attend-button`, {
        onclick: () => {
          if (isAttending) {
            logger.debug(`User clicked button to no longer attend meet`)
            state.registeringAttendance = meet.uuid
            emit.triggerRender()
            ajax.delete(`/api/meets/${meet.uuid}/attendees/${loggedInUser.username}`)
              .then(() => {
                meet.attendees = reject(propEq('username', loggedInUser.username), meet.attendees)
                logger.debug(`Succesfully registered user as not attending meet`)
              })
              .finally(() => {
                state.registeringAttendance = null
                emit.triggerRender()
              })
          } else {
            logger.debug(`User clicked button to attend meet`)
            state.registeringAttendance = meet.uuid
            emit.triggerRender()
            ajax.postJson(`/api/meets/${meet.uuid}/attendees`, {})
              .then(() => {
                meet.attendees.push(loggedInUser)
                logger.debug(`Succesfully registered user as attending meet`)
              })
              .finally(() => {
                state.registeringAttendance = null
                emit.triggerRender()
              })
          }
        },
      }, [
        isAttending ? `Attending` : `Attend`,
      ]),
    ]),
  ])
}
