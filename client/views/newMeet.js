const h = require('@arve.knudsen/hyperscript')
const sheetify = require('sheetify')
const t = require('tcomb')
const debounce = require('debounce')
const partial = require('ramda/src/partial')
const isEmpty = require('ramda/src/isEmpty')
const addIndex = require('ramda/src/addIndex')
const map = require('ramda/src/map')
const trim = require('ramda/src/trim')
const {isNullOrBlank,} = require('@arve.knudsen/stringutils')
const assert = require('assert')
const includes = require('ramda/src/includes')
const filter = require('ramda/src/filter')
const reject = require('ramda/src/reject')
const any = require('ramda/src/any')
const propEq = require('ramda/src/propEq')
const pipe = require('ramda/src/pipe')
const toPairs = require('ramda/src/toPairs')
const all = require('ramda/src/all')
const mergeRight = require('ramda/src/mergeRight')
const fromPairs = require('ramda/src/fromPairs')
const pick = require('ramda/src/pick')
const prop = require('ramda/src/prop')
const {DateTime,} = require('luxon')

const loadingComponent = require('../components/loading')(['newMeetView', 'loading',])
const createIconElem = require('../createIconElem')
const ajax = require('../ajax')
const {markdownEditorWidget,} = require('../markdownEditorWidget')
const logger = require('../logger').child({module: 'views/newMeet',})

const sheet = sheetify('./newMeet.styl')

const checkForm = debounce((state, emit) => {
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])
  logger.debug(`Checking whether to enable submission...`)
  const {addMeetState,} = state

  const markdownElem = document.getElementById('wmd-input-markdown')
  assert.notEqual(markdownElem, null, `No element with ID wmd-input-markdown`)
  const inputName2Data = pipe(
    toPairs,
    map(([key, value,]) => {
      if (value == null) {
        value = ''
      }
      t.String(value, ['addMeetState', 'input', key,])
      return [key, trim(value),]
    }),
    fromPairs
  )(addMeetState.input)
  inputName2Data.description = markdownElem.value

  /*
  logger.debug(`Storing add meet form locally:`, cachedData)
  window.localStorage.setItem('addMeetData', JSON.stringify(cachedData))
  */

  const notRequired = [
    'endDate', 'endTime',
  ]
  logger.debug(`Validating form input and determining whether to enable submission`,
    inputName2Data)
  addMeetState.isSubmissionEnabled = all(([name, data,]) => {
    t.String(name, ['inputName2Data',])
    t.String(data, ['inputName2Data', name,])
    return includes(name, notRequired) || !isNullOrBlank(data)
  }, toPairs(inputName2Data))

  if (isEmpty(addMeetState.hosts)) {
    addMeetState.isSubmissionEnabled = false
  }
  if (!addMeetState.isOnline && addMeetState.location == null) {
    addMeetState.isSubmissionEnabled = false
  }
  if (addMeetState.showAttendeeLimit && addMeetState.attendeeLimit == null) {
    addMeetState.isSubmissionEnabled = false
  }

  logger.debug(`Have markdown: ${!isNullOrBlank(inputName2Data.description)}`)
  logger.debug(`Enabled submission: ${addMeetState.isSubmissionEnabled}`, {
    addMeetState,
  })
  emit.triggerRender()
}, 300)

let descriptionEditorWidget
const renderDescriptionEditor = (state, emit) => {
  if (descriptionEditorWidget == null) {
    descriptionEditorWidget = markdownEditorWidget()
  }
  return h('fieldset#add-meet-description-fieldset', [
    h('legend', 'Description'),
    h('#description-editor', [
      descriptionEditorWidget(state.addMeetState, emit, partial(checkForm, [state, emit,])),
    ]),
  ])
}

const filterHostSelection = debounce((state, emit, query) => {
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])
  t.String(query, ['query',])
  const {group, addMeetState,} = state
  t.Object(group, ['state', 'group',])
  t.Object(addMeetState, ['state', 'addMeetState',])
  query = query.toLowerCase()
  logger.debug(`Filtering host selection for adding a meet, query: '${query}'`, {group: group,})
  let hosts
  if (isNullOrBlank(query)) {
    hosts = []
  } else {
    hosts = filter((member) => {
      return (includes(query, member.username.toLowerCase()) || includes(query,
        member.name.toLowerCase())) && !any(propEq('username', member.username), addMeetState.hosts)
    }, group.members)
  }
  logger.debug(`Got host selection:`, hosts)
  addMeetState.filteredHosts = hosts
  emit.triggerRender()
}, 300)

const renderHosts = (state, emit) => {
  const {addMeetState,} = state
  t.Object(addMeetState, ['state', 'addMeetState',])
  const shouldShowDropdown = !isEmpty(addMeetState.filteredHosts) &&
    addMeetState.shouldShowHostSelectionDropdown
  return h('fieldset#add-meet-hosts-fieldset', [
    h('legend', 'Hosts'),
    h(`.typeahead-group`, [
      h(`input#add-meet-host-input.add-meet-input`, {
        placeholder: `Add a host`,
        value: addMeetState.hostQuery,
        onblur: () => {
          logger.debug(`Host selection input was blurred, hiding host selection dropdown`)
          state.addMeetState.hoveredHostIndex = null
          state.addMeetState.shouldShowHostSelectionDropdown = false
          emit.triggerRender()
        },
        onfocus: () => {
          logger.debug(`Host selection input was focused, showing host selection dropdown`)
          state.addMeetState.hoveredHostIndex = null
          state.addMeetState.shouldShowHostSelectionDropdown = true
          emit.triggerRender()
        },
        oninput: (event) => {
          addMeetState.hostQuery = event.target.value
          const query = trim(event.target.value)
          filterHostSelection(state, emit, query)
        },
      }),
      h(`ul#host-dropdown.typeahead-dropdown${!shouldShowDropdown ? '.hidden': ''}`, addIndex(map)((host, i) => {
        const suffix = i === addMeetState.hoveredHostIndex ? '.active' : ''
        return h(`li${suffix}`, {
          onmouseenter: () => {
            logger.debug(`Entering dropdown item ${i}`)
            addMeetState.hoveredHostIndex = i
            emit.triggerRender()
          },
          onmouseleave: () => {
            logger.debug(`Exiting dropdown item ${i}`)
            addMeetState.hoveredHostIndex = null
            emit.triggerRender()
          },
          onmousedown: () => {
            logger.debug(`Dropdown item ${i} was clicked`)
            const host = addMeetState.filteredHosts[i]
            const hostName = host.name
            logger.debug(`The selected host is '${hostName}'`)
            const selectedHostId = host.username
            assert.ok(!isNullOrBlank(selectedHostId))
            addMeetState.hoveredHostIndex = null
            addMeetState.shouldShowHostSelectionDropdown = false
            addMeetState.filteredHosts = []
            addMeetState.selectedHostId = selectedHostId
            addMeetState.hostQuery = ''
            addMeetState.hosts.push(host)
            checkForm(state, emit)
          },
        }, host.name)
      }, addMeetState.filteredHosts)),
    ]),
    !isEmpty(addMeetState.hosts) ? h(`ul#hosts`, map((host) => {
      return h(`li.host`, [
        h(`.remove-host`, {
          onclick: () => {
            logger.debug(`Removing host '${host.name}'`)
            addMeetState.hosts = reject(propEq('username', host.username), addMeetState.hosts)
            checkForm(state, emit)
            logger.debug(`Triggered re-render`)
          },
        }, createIconElem('user-cancel2')),
        h(`img.host-avatar`, {src: host.avatarUrl,}),
        host.name,
      ])
    }, addMeetState.hosts)) : null,
  ])
}

const renderOptionalSettings = (state, emit) => {
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])
  const {addMeetState,} = state
  t.Object(addMeetState, ['state', 'addMeetState',])

  return h(`fieldset#optional-settings`, [
    h(`legend`, `Optional Settings`),
    h(`.optional-setting-container`, [
      h(`.optional-setting-heading`, [
        h(`input#input-attendee-limit.optional-setting-checkbox`, {
          type: `checkbox`,
          name: `attendee-limit`,
          checked: addMeetState.showAttendeeLimit,
          onchange: (event) => {
            const isEnabled = event.target.checked
            logger.debug(`Enabling showing of attendee limit setting: ${isEnabled}`)
            addMeetState.showAttendeeLimit = isEnabled
            checkForm(state, emit)
          },
        }),
        h(`#label-attendee-limit`, `Attendee Limit`),
      ]),
      h(`input.optional-setting${addMeetState.showAttendeeLimit ? '' : '.hidden'}`, {
        type: `number`,
        min: 1,
        max: 9999,
        oninput: (event) => {
          const limit = !isNullOrBlank(event.target.value) ? parseInt(event.target.value, 10) : null
          assert.ok(!isNaN(limit))
          logger.debug(`Attendee limit changed to ${limit}`)
          addMeetState.attendeeLimit = limit
          checkForm(state, emit)
        },
        value: addMeetState.attendeeLimit,
        placeholder: 'Attendee limit',
      }),
    ]),
  ])
}

const filterLocations = debounce((state, emit, query) => {
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])
  t.String(query, ['query',])
  const {addMeetState,} = state
  t.Object(addMeetState, ['state', 'addMeetState',])

  if (isNullOrBlank(query)) {
    addMeetState.locationPredictions = []
    emit.triggerRender()
    return
  }

  logger.debug(`Querying for location: '${query}'...`)
  ajax.getJson(`/api/place-predictions?query=${query}`).then(async (predictions) => {
    logger.debug(`Received location predictions:`, predictions)
    if (addMeetState.locationQuery === query) {
      addMeetState.locationPredictions = predictions
      emit.triggerRender()
    } else {
      logger.debug(`Ignoring received location predictions since the query changed in the meantime`)
    }
  })
}, 300)

const renderLocationMap = (state, emit) => {
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])
  const {addMeetState,} = state
  t.Object(addMeetState, ['state', 'addMeetState',])
  t.String(addMeetState.mapImageUrl, ['state', 'addMeetState', 'mapImageUrl',])
  logger.debug(`Rendering map, URL: ${addMeetState.mapUrl}`)
  const mapUri = encodeURI(`https://google.com/maps/search/?api=1&query=${
      addMeetState.location.description}&query_place_id=${addMeetState.location.placeId}`)
  return h(`a#meet-location-map`, {
    href: mapUri,
    target: `_blank`,
    rel: `noreferrer noopener`,
  }, h(`img`, {src: addMeetState.mapImageUrl,}))
}

const renderLocation = (state, emit) => {
  t.Object(state, ['state',])
  const {addMeetState,} = state
  t.Object(addMeetState, ['state', 'addMeetState',])
  t.Function(emit, ['emit',])

  if (addMeetState.isLoadingLocation) {
    return loadingComponent.render(state, emit)
  }

  return h(`#location-group.typeahead-group`, [
    h(`#edit-is-online`, [
      h('label', 'Online'),
      h(`input`, {
        type: 'checkbox',
        checked: addMeetState.isOnline,
        onchange: (event) => {
          const isOnline = event.target.checked
          logger.debug(`Meet is online: ${isOnline}`)
          addMeetState.isOnline = isOnline
          checkForm(state, emit)
        },
      }),
    ]),
    !addMeetState.isOnline ? renderLocationEditor(state, emit) : null,
  ])
}

const renderLocationEditor = (state, emit) => {
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])
  const {addMeetState,} = state
  t.Object(addMeetState, ['state', 'addMeetState',])

  if (addMeetState.location != null) {
    logger.debug(`A location is selected, rendering it`, {location: addMeetState.location,})
    return h(`#meet-location`, [
      h(`#meet-location-address-container`, [
        h(`#meet-location-meta`, [
          h(`#meet-location-name`, addMeetState.location.name),
          h(`#meet-location-address`, addMeetState.location.address),
        ]),
        h(`button#meet-location-change-button`, {
          onclick: (event) => {
            logger.debug(`Button to change location was clicked`)
            event.preventDefault()

            addMeetState.location = null
            checkForm(state, emit)
          },
        }, `Change`),
      ]),
      renderLocationMap(state, emit),
    ])
  }

  const shouldShowDropdown = !isEmpty(addMeetState.locationPredictions) &&
    addMeetState.shouldShowLocationSelectionDropdown
  return [
    h(`input#input-location.add-meet-input`, {
      placeholder: `Location`,
      required: true,
      value: addMeetState.locationQuery,
      onblur: () => {
        logger.debug(`Location query input was blurred`)
        logger.debug(`Hiding location query dropdown`)
        addMeetState.hoveredLocationIndex = null
        addMeetState.shouldShowLocationSelectionDropdown = false
        emit.triggerRender()
      },
      onfocus: () => {
        logger.debug(`Location query input was focused`)
        logger.debug(`Showing location query dropdown`)
        addMeetState.hoveredLocationIndex = null
        addMeetState.shouldShowLocationSelectionDropdown = true
        emit.triggerRender()
      },
      oninput: (event) => {
        const query = event.target.value
        addMeetState.locationQuery = query
        filterLocations(state, emit, query)
      },
    }),
    h(`ul#location-dropdown.typeahead-dropdown${!shouldShowDropdown ? '.hidden': ''}`, addIndex(map)((prediction, i) => {
      const suffix = i === addMeetState.hoveredLocationIndex ? '.active' : ''
      return h(`li${suffix}`, {
        onmouseenter: () => {
          logger.debug(`Entering dropdown item ${i}`)
          addMeetState.hoveredLocationIndex = i
          emit.triggerRender()
        },
        onmouseleave: () => {
          logger.debug(`Exiting dropdown item ${i}`)
          addMeetState.hoveredLocationIndex = null
          emit.triggerRender()
        },
        onmousedown: () => {
          logger.debug(`Dropdown item ${i} was clicked`, {prediction,})
          addMeetState.hoveredLocationIndex = null
          addMeetState.shouldShowLocationSelectionDropdown = false
          addMeetState.locationPredictions = []
          addMeetState.locationQuery = ''
          checkForm(state, emit)

          logger.debug(`Getting map URL from server...`)
          addMeetState.isLoadingLocation = true
          emit.triggerRender()
          ajax.getJson(
            `/api/place-details?description=${prediction.description}&placeId=${prediction.place_id}`)
            .then(async (details) => {
            logger.debug(`Received place details:`, details)
            if (!state.addMeetState.isLoadingLocation) {
              logger.debug(`Ignoring received place details, due to stale query`)
              return
            }

            state.addMeetState.mapImageUrl = details.mapUrl
            state.addMeetState.location = {
              placeId: prediction.place_id,
              description: prediction.description,
              name: details.name,
              address: details.address,
            }
            state.addMeetState.isLoadingLocation = false
            emit.triggerRender()
          })
        },
      }, prediction.description)
    }, addMeetState.locationPredictions || [])),
  ]
}

module.exports = {
  name: 'newMeetView',
  route: '/g/:path/new-meet',
  loadData: async (state, emit) => {
    t.Object(state, ['state',])
    t.Function(emit, ['emit',])
    const path = state.params.path
    assert.ok(!isNullOrBlank(path))
    logger.debug(`Loading group ${path}...`)
    const group = await ajax.getJson(`/api/groups/${path}`)
    logger.debug(`Successfully loaded group '${path}'`)
    state.group = group
  },
  resetState: (state) => {
    t.Object(state, ['state',])
    state.addMeetState = {
      input: {
        title: '',
        description: '',
      },
      hostQuery: '',
      locationQuery: '',
      hosts: [],
      filteredHosts: [],
      locationPredictions: [],
      isOnline: false,
    }
  },
  render: (state, emit) => {
    t.Object(state, ['state',])
    t.Object(state.global, ['state', 'global',])
    t.Function(emit, ['emit',])
    const {addMeetState,} = state
    t.Object(addMeetState, ['state', 'addMeetState',])
    const {path: groupPath,} = state.params
    t.String(groupPath, ['state', 'params', 'path',])

    logger.debug(`Rendering`)
    return h(`.${sheet}`, h('#new-meet-view', [
      h(`h1`, `Add a Meet`),
      h(`form#new-meet-form`, [
        h(`input#input-title.add-meet-input`, {
          placeholder: `Title`,
          required: true,
          value: addMeetState.input.title,
          oninput: (event) => {
            addMeetState.input.title = event.target.value
            checkForm(state, emit)
          },
        }),
        renderLocation(state, emit),
        h(`#add-meet-start`, [
          h(`label`, `Starts`),
          h(`#add-meet-input-start`, [
            h(`input#add-meet-input-start-date`, {
              type: `date`,
              placeholder: `Date`,
              required: true,
              value: addMeetState.input.startDate,
              oninput: (event) => {
                addMeetState.input.startDate = event.target.value
                checkForm(state, emit)
              },
            }),
            h(`input#add-meet-input-start-time.time-picker`, {
              type: `time`,
              placeholder: `Time`,
              required: true,
              value: addMeetState.input.startTime,
              oninput: (event) => {
                addMeetState.input.startTime = event.target.value
                checkForm(state, emit)
              },
            }),
          ]),
        ]),
        h(`#add-meet-end`, [
          h(`label`, `Ends`),
          h(`#add-meet-input-end`, [
            h(`input#add-meet-input-end-date`, {
              type: `date`,
              placeholder: `Date`,
              value: addMeetState.input.endDate,
              oninput: (event) => {
                addMeetState.input.endDate = event.target.value
                checkForm(state, emit)
              },
            }),
            h(`input#add-meet-input-end-time.time-picker`, {
              type: `time`,
              placeholder: `Time`,
              value: addMeetState.input.endTime,
              oninput: (event) => {
                addMeetState.input.endTime = event.target.value
                checkForm(state, emit)
              },
            }),
          ]),
        ]),
        renderDescriptionEditor(state, emit),
        renderHosts(state, emit),
        renderOptionalSettings(state, emit),
      ]),
      h(`#add-meet-buttons`, [
        h(`button#submit-add-meet-button.button-primary`, {
          type: 'submit',
          disabled: !state.addMeetState.isSubmissionEnabled,
          onclick: () => {
            event.preventDefault()
            logger.debug('Submit button clicked')
            submitAddMeet(state, emit)
              .catch((err) => {
                // TODO: Display error to user
                logger.warning(`An error occurred while submitting meet`, {err,})
              })
          },
        }, `Add Meet`),
        h(`button#cancel-add-meet-button`, {
          onclick: () => {
            logger.debug(`Cancel button clicked, redirecting back to meet page`)
            setTimeout(() => {
              emit.global('pushState', `/g/${groupPath}`)
            }, 0)
          },
        }, `Cancel`),
      ]),
    ]))
  },
}

const submitAddMeet = async (state, emit) => {
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])
  const {addMeetState, group,} = state
  t.Object(addMeetState, ['state', 'addMeetState',])
  t.Object(group, ['state', 'group',])
  t.String(group.path, ['group', 'path',])

  const payload = mergeRight(mergeRight(pick(['title',], addMeetState.input),
    pick(['location', 'isOnline',], addMeetState)), {
      hostsExternal: [],
      attendeesExternal: [],
    })
  if (addMeetState.isOnline) {
    payload.location = null
  }
  payload.groupPath = group.path
  payload.hosts = map(prop('username'), addMeetState.hosts)
  payload.starts = DateTime.fromISO(
    `${addMeetState.input.startDate}T${addMeetState.input.startTime}`, {
      zone: 'local',
    }).toUTC()
  if (!isNullOrBlank(payload.endDate)) {
    payload.ends = DateTime.fromISO(
      `${addMeetState.input.endDate}T${addMeetState.input.endTime}`, {
        zone: 'local',
      }).toUTC()
  }

  const markdownElem = document.getElementById('wmd-input-markdown')
  assert.notEqual(markdownElem, null, `No element with ID wmd-input-markdown`)
  payload.description = markdownElem.value
  t.String(payload.description, ['payload', 'description',])
  if (addMeetState.showAttendeeLimit) {
    payload.attendeeLimit = addMeetState.attendeeLimit
  }
  logger.debug(`Button to add meet clicked, submitting to server`, {
    payload,
  })

  const meet = await ajax.postJson(`/api/meets`, payload)
  logger.debug(`Meet successfully created on server`, {meet,})

  // Prune client side cache
  t.Object(state.global, ['state', 'global',])
  delete state.global.components.homeView.loadingState
  delete state.global.components.groupProfileView.loadingState

  setTimeout(() => {
    emit.global('pushState', `/m/${meet.uuid}`)
  }, 0)
}
