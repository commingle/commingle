const h = require('@arve.knudsen/hyperscript')
const sheetify = require('sheetify')
const t = require('tcomb')
const assert = require('assert')
const {isNullOrBlank,} = require('@arve.knudsen/stringutils')
const {DateTime,} = require('luxon')
const map = require('ramda/src/map')
const addIndex = require('ramda/src/addIndex')
const titleize = require('titleize')
const jdenticon = require('jdenticon')
const Promise = require('bluebird')
const forEach = require('ramda/src/forEach')

const ajax = require('../ajax')
const logger = require('../logger').child({module: 'views/userProfile',})

const sheet = sheetify('./userProfile.styl')

const renderLeftColumn = (state, user) => {
  t.Object(state, ['state',])
  t.Object(user, ['user',])
  t.Array(state.userGroups, ['state.userGroups',])
  t.Array(state.pastMeets, ['state.pastMeets',])
  const joinDate = DateTime.fromISO(user.added).toLocal().toLocaleString(DateTime.DATE_FULL)
  const numGroups = state.userGroups.length
  const groupsSfx = numGroups !== 1 ? `s` : ''
  const numPrevMeets = state.pastMeets.length

  return h(`#left-column`, [
    h(`img#user-avatar`, {src: user.avatarUrl,}),
    h(`#user-names-container.bb.b--light-gray.pv3.mb0`, [
      h(`#user-name`, user.name),
      h(`#user-username.muted`, state.params.username),
    ]),
    h(`#user-metadata-container.b--light-gray.pv3.mb0.f6`, [
      h(`#user-bio.user-metadata.pt1`, user.bio),
      h(`#user-join-date.user-metadata.pt1`, `Joined ${joinDate}`),
      h(`#user-num-meets-gone-to.user-metadata.pt1`, `Has gone to ${numPrevMeets} meet${
        numPrevMeets != 1 ? 's' : ''}`),
      h(`#user-num-groups.user-metadata.pt1`, `Member of ${numGroups} group${groupsSfx}`),
    ]),
  ])
}

const renderUpcomingMeets = (state) => {
  t.Object(state, ['state',])
  const {user,} = state
  t.Object(user, ['user',])
  const {loggedInUser,} = state.global
  const isLoggedInUser = loggedInUser != null && user.username === loggedInUser.username
  const meets = state.upcomingMeets || []
  if (meets.length === 0) {
    return h(`#upcoming-meets.empty-user-info-tab-content`, isLoggedInUser ?
      `You don't have any upcoming meets.` : `${user.name} doesn't have any upcoming meets.`)
  }

  return h(`#upcoming-meets`, map((meet) => {
    t.Object(meet, ['meet',])
    const startTime = DateTime.fromISO(meet.starts).toLocal()
    const endTime = DateTime.fromISO(meet.ends).toLocal()
    const endDateAndTimeStr = !endTime.hasSame(startTime, 'day') ? endTime.toLocaleString({
      year: 'numeric',
      month: 'long',
      day: 'numeric',
      hour: 'numeric',
      minute: '2-digit',
    }) : endTime.toLocaleString({
      hour: 'numeric',
      minute: '2-digit',
    })
    const meetTime = `${startTime.toLocaleString({
      year: 'numeric',
      month: 'long',
      day: 'numeric',
      hour: 'numeric',
      minute: '2-digit',
    })} - ${endDateAndTimeStr}`
    return h(`.individual-meet`, h(`.meet-container`, h(`.meet-inner`, [
      h(`.meet-content`, [
        h(`a`, {href: `/m/${meet.uuid}`,}, [
          h(`.meet-title`, meet.title),
          h(`.meet-time`, meetTime),
          h(`.meet-location`, !meet.isOnline ? meet.location.name : 'Online'),
        ]),
      ]),
    ])))
  }, meets))
}

const renderPastMeets = (state) => {
  t.Object(state, ['state',])
  const {user,} = state
  t.Object(user, ['user',])
  const {loggedInUser,} = state.global
  const isLoggedInUser = loggedInUser != null && user.username === loggedInUser.username
  const meets = state.pastMeets || []
  if (meets.length === 0) {
    return h(`#past-meets.empty-user-info-tab-content`, isLoggedInUser ?
      `You haven't gone to any meets yet.` : `${user.name} hasn't gone to any meets.`)
  }

  return h(`#past-meets`, map((meet) => {
    t.Object(meet, ['meet',])
    const startTime = DateTime.fromISO(meet.starts).toLocal()
    const endTime = DateTime.fromISO(meet.ends).toLocal()
    const endDateAndTimeStr = !endTime.hasSame(startTime, 'day') ? endTime.toLocaleString({
      year: 'numeric',
      month: 'long',
      day: 'numeric',
      hour: 'numeric',
      minute: '2-digit',
    }) : endTime.toLocaleString({
      hour: 'numeric',
      minute: '2-digit',
    })
    const meetTime = `${startTime.toLocaleString({
      year: 'numeric',
      month: 'long',
      day: 'numeric',
      hour: 'numeric',
      minute: '2-digit',
    })} - ${endDateAndTimeStr}`
    return h(`.individual-meet`, h(`.meet-container`, h(`.meet-inner`, [
      h(`.meet-content`, [
        h(`a`, {href: `/m/${meet.uuid}`,}, [
          h(`.meet-title`, meet.title),
          h(`.meet-time`, meetTime),
          h(`.meet-location`, !meet.isOnline ? meet.location.name : 'Online'),
        ]),
      ]),
    ])))
  }, meets))
}

const renderUserActivity = (state, emit) => {
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])
  let renderer
  let activeIndex
  if (state.activeActivityTab === `upcoming`) {
    renderer = renderUpcomingMeets
    activeIndex = 0
  } else if (state.activeActivityTab === `past`) {
    renderer = renderPastMeets
    activeIndex = 1
  } else {
    throw new Error(`Unrecognized tab '${state.activeActivityTab}'`)
  }
  return h(`#user-activity`, [
    h(`#user-activity-header.eventfeed-header`, [
      h(`nav`, h(`ul`, addIndex(map)((name, i) => {
        t.String(name, ['name',])
        t.Number(i)
        let sfx = ''
        if (i === activeIndex) {
          sfx = `.active`
        }
        const id = `${name}-tab`
        return h(`li#${id}.tab${sfx}`, {
          onclick: () => {
            if (state.activeTab !== name) {
              logger.debug(`Switching to ${name} meets tab`)
              state.activeActivityTab = name
              emit.triggerRender()
            }
          },
        }, titleize(name))
      }, [`upcoming`, `past`,]))),
    ]),
    h(`#user-activity-content`, renderer(state)),
  ])
}

const renderUserGroups = (state) => {
  t.Object(state, ['state',])
  t.Object(state.global, ['state', 'global',])
  const {loggedInUser,} = state.global
  const {user,} = state
  t.Object(user, ['state.user',])
  const groups = state.userGroups || []
  if (groups.length === 0) {
    return h(`#user-groups.empty-user-info-tab-content`, `${user.name} isn't in any groups.`)
  }

  const isLoggedInUser = loggedInUser != null && user.username === loggedInUser.username

  return h(`#user-groups`, map((group) => {
    logger.debug(`Rendering group`, group)
    let membershipStr
    if (loggedInUser != null && !isLoggedInUser) {
      forEach((member) => {
        if (membershipStr != null || member.username !== user.username) {
          return
        }

        if (member.membership === 'Admin') {
          membershipStr = `You are an admin of this group`
        } else {
          membershipStr = `You are a member of this group`
        }
      }, group.members)
      if (membershipStr == null) {
        membershipStr = `You are not a member of this group`
      }
    }
    const avatarElem = h(`.group-avatar-container`)
    avatarElem.innerHTML = jdenticon.toSvg(group.path, 80)
    return h(`.group-container`, h(`.group-inner`, [
      h(`a`, {href: `/g/${group.path}`,}, avatarElem),
      h(`.group-content`, [
        h(`a`, {href: `/g/${group.path}`,}, [
          h(`.group-name`, group.name),
          h(`.group-description`, group.description),
          membershipStr != null ? h(`.group-meta`, [
            (`.group-membership`, membershipStr),
          ]) : null,
        ]),
      ]),
    ]))
  }, groups))
}

const renderRightColumn = (state, emit) => {
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])
  const {user,} = state
  t.Object(user, ['state', 'user',])

  let renderer
  let activeIndex
  if (state.activeTab === `activity`) {
    renderer = renderUserActivity
    activeIndex = 0
  } else if (state.activeTab === `groups`) {
    renderer = renderUserGroups
    activeIndex = 1
  } else {
    throw new Error(`Unrecognized tab '${state.activeTab}'`)
  }
  return h(`#right-column`, [
    // We need a container for the tab elements, so we can make it 100% wide and cover any elements
    // scrolling beneath it
    h(`#top-section`, [
      h(`#breadcrumbs`, [
        h(`span`, `Users`),
        h(`span.arrow`, `>`),
        h(`a`, {href: `/u/${user.username}`,}, user.name),
      ]),
      h(`hr`),
      h(`nav#user-info-tabs`, h(`ul`, addIndex(map)((name, i) => {
        t.String(name, ['name',])
        t.Number(i, ['i',])
        let sfx = ''
        if (i === activeIndex) {
          sfx = `.active`
        }
        const id = `${name}-tab`
        return h(`li#${id}.tab${sfx}`, {
          onclick: () => {
            if (state.activeTab !== name) {
              logger.debug(`Switching to user ${name} tab`)
              state.activeTab = name
              emit.triggerRender()
            }
          },
        }, titleize(name))
      }, ['activity', 'groups',]))),
    ]),
    h(`#user-info-tab-content`, [
      renderer(state, emit),
    ]),
  ])
}

module.exports = {
  name: 'userProfileView',
  route: '/u/:username',
  resetState: (state) => {
    state.activeTab = `activity`
    state.activeActivityTab = `upcoming`
  },
  loadData: async (state, emit) => {
    t.Object(state, ['state',])
    t.Function(emit, ['emit',])
    const username = state.params.username
    assert.ok(!isNullOrBlank(username))
    logger.debug(`Loading user ${username}...`)
    const [user, groups, upcomingMeets, pastMeets,] = await Promise.all([
      ajax.getJson(`/api/users/${username}`),
      ajax.getJson(`/api/users/${username}/groups`),
      ajax.getJson(`/api/users/${username}/upcoming-meets`),
      ajax.getJson(`/api/users/${username}/past-meets`),
    ])
    logger.debug(`Successfully loaded user ${username}`)
    state.user = user
    state.userGroups = groups
    state.upcomingMeets = upcomingMeets
    state.pastMeets = pastMeets
  },
  render: (state, emit) => {
    t.Object(state, ['state',])
    t.Object(state.global, ['state', 'global',])
    t.Function(emit, ['emit',])
    const {user,} = state
    t.Object(user, ['state.user',])
    logger.debug(`Rendering profile page of user '${state.params.username}'`, {state,})
    return h(`.${sheet}`, [
      renderLeftColumn(state, user),
      renderRightColumn(state, emit, user),
    ])
  },
}
