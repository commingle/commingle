const h = require('@arve.knudsen/hyperscript')
const sheetify = require('sheetify')
const t = require('tcomb')
const debounce = require('debounce')
const map = require('ramda/src/map')
const trim = require('ramda/src/trim')
const {isNullOrBlank,} = require('@arve.knudsen/stringutils')
const includes = require('ramda/src/includes')
const pipe = require('ramda/src/pipe')
const toPairs = require('ramda/src/toPairs')
const all = require('ramda/src/all')
const fromPairs = require('ramda/src/fromPairs')
const pick = require('ramda/src/pick')
const prop = require('ramda/src/prop')

const focusedElement = require('../focusedElement')
const ajax = require('../ajax')
const logger = require('../logger').child({module: 'views/newGroup',})

const sheet = sheetify('./newGroup.styl')

const checkForm = debounce((state, emit) => {
  t.Object(state, ['state',])
  t.Function(emit, ['emit',])
  logger.debug(`Checking whether to enable submission...`)
  const {addGroupState,} = state

  const inputName2Data = pipe(
    toPairs,
    map(([key, value,]) => {
      if (value == null) {
        value = ''
      }
      t.String(value, ['addGroupState', 'input', key,])
      return [key, trim(value),]
    }),
    fromPairs
  )(addGroupState.input)

  const notRequired = ['description', 'path',]

  logger.debug(`Validating form input and determining whether to enable submission`,
    inputName2Data)
  addGroupState.isSubmissionEnabled = all(([name, data,]) => {
    t.String(name, ['inputName2Data',])
    t.String(data, ['inputName2Data', name,])
    return includes(name, notRequired) || !isNullOrBlank(data)
  }, toPairs(inputName2Data))

  logger.debug(`Enabled submission: ${addGroupState.isSubmissionEnabled}`, {
    addGroupState,
  })
  emit.triggerRender()
}, 300)

module.exports = {
  name: 'newGroupView',
  route: '/new-group',
  resetState: (state) => {
    t.Object(state, ['state',])
    state.addGroupState = {
      isPathCalculated: true,
      input: {
        name: '',
        description: '',
        path: '',
      },
    }
  },
  render: (state, emit) => {
    t.Object(state, ['state',])
    t.Object(state.global, ['state', 'global',])
    t.Function(emit, ['emit',])
    const {addGroupState,} = state
    t.Object(addGroupState, ['state', 'addGroupState',])

    logger.debug(`Rendering`)
    return h(`.${sheet}`, h('#new-group-view', [
      h(`h1#heading`, `Add a Group`),
      h(`form#new-group-form`, [
        focusedElement(`input#input-name.add-group-input`, {
          placeholder: `Name`,
          required: true,
          value: addGroupState.input.name,
          oninput: (event) => {
            addGroupState.input.name = event.target.value
            if (addGroupState.isPathCalculated) {
              addGroupState.input.path = event.target.value.toLowerCase().replace(/[^a-z0-9]/g,
                '-').replace(/-+/g, '-').replace(/^-|-$/g, '')
            }
            checkForm(state, emit)
          },
        }),
        h(`input#input-path.add-group-input`, {
          value: addGroupState.input.path,
          pattern: `^[a-z0-9]+((-?[a-z0-9])*[a-z0-9])?$`,
          placeholder: `my-group`,
          required: true,
          oninput: (event) => {
            t.Object(event)
            logger.debug(`Path edited`)
            addGroupState.input.path = event.target.value.toLowerCase()
            addGroupState.isPathCalculated = false
            checkForm(state, emit)
          },
        }),
        h('fieldset#description-fieldset', [
          h('legend', 'Description (Optional)'),
          h('textarea#description-editor', {
            value: addGroupState.input.description,
            oninput: (event) => {
              t.Object(event)
              logger.debug(`Description edited`)
              addGroupState.input.description = event.target.value
              checkForm(state, emit)
            },
          }),
        ]),
      ]),
      h(`#add-group-buttons`, [
        h(`button#submit-add-group-button.button-primary`, {
          type: 'submit',
          disabled: !state.addGroupState.isSubmissionEnabled,
          onclick: () => {
            const {addGroupState,} = state
            t.Object(addGroupState, ['state', 'addGroupState',])
            const payload = pick(['name', 'path', 'description',], addGroupState.input)
            logger.debug(`Button to add group clicked, submitting to server`, {
              payload,
            })
            ajax.postJson(`/api/groups`, payload).then(async (group) => {
              logger.debug(`Group successfully created on server`, {group,})
              t.Object(state.global, ['state', 'global',])
              const {loggedInUser,} = state.global
              if (loggedInUser != null) {
                t.Array(loggedInUser.groups, ['state', 'global', 'loggedInUser', 'groups',])
                if (!includes(group.path, map(prop('groupPath'), loggedInUser.groups))) {
                  logger.debug(`Adding group to logged in user's groups`)
                  loggedInUser.groups.push({
                    groupPath: group.path,
                    membership: 'Admin',
                  })
                }
              }
              // Force reloading of the home view state, to refresh the list of groups
              delete state.global.components.homeView.loadingState

              setTimeout(() => {
                emit.global('pushState', `/g/${group.path}`)
              }, 0)
            })
          },
        }, `Add Group`),
        h(`button#cancel-add-group-button`, {
          onclick: () => {
            logger.debug(`Cancel button clicked, redirecting back to groups page`)
            setTimeout(() => {
              emit.global('pushState', `/groups`)
            }, 0)
          },
        }, `Cancel`),
      ]),
    ]))
  },
}
