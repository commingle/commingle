const h = require('@arve.knudsen/hyperscript')
const sheetify = require('sheetify')

const {nbsp,} = require('../specialChars')

const sheet = sheetify('./footer.styl')

module.exports = (state) => {
  return h(`.${sheet}`, [
    h('footer.bg-white.black-70.ph3.ph5-ns.pv4.pv5-ns.b--black-10', [
      h('p.lh-copy.f6.ma0', [
        `Version`, nbsp, h('strong', state.version),
      ]),
    ]),
  ])
}
