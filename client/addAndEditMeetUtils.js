const assert = require('assert')
const {isNullOrBlank,} = require('@arve.knudsen/stringutils')
const bowser = require('bowser/es5')
const {DateTime,} = require('luxon')
const t = require('tcomb')

const logger = require('./logger').child({module: 'addAndEditEventUtils',})

const getDateTimeStr = (dateStr, timeStr, required) => {
  t.String(dateStr, ['dateStr',])
  t.String(timeStr, ['timeStr',])
  t.Boolean(required, ['required',])
  if (required) {
    assert.ok(!isNullOrBlank(dateStr))
    assert.ok(!isNullOrBlank(timeStr))
  } else if (isNullOrBlank(dateStr) || isNullOrBlank(timeStr)) {
    return null
  }

  if (bowser.safari) {
    logger.debug(`On Safari, converting ${dateStr} and ${timeStr} according to specified format`)
    const dateParsed = DateTime.fromFormat(`${dateStr} ${timeStr}`, 'dd/MM/yyyy HH:mm')
    if (!dateParsed.isValid) {
      logger.debug(`Not a date: ${dateStr}`)
      return null
    } else {
      logger.debug(`Successfully parsed date '${dateStr}' to ${dateParsed.toISO()}`)
      return dateParsed.toUTC().toISO()
    }
  } else {
    return DateTime.fromISO(`${dateStr}T${timeStr}`).toUTC().toISO()
  }
}

module.exports = {
  getDateTimeStr,
}
