const {DateTime,} = require('luxon')
const h = require('@arve.knudsen/hyperscript')
const logger = require('./logger').child({module: 'dateTimePickerWidget',})
const sheetify = require('sheetify')
const t = require('tcomb')
const concat = require('ramda/src/concat')
const bowser = require('bowser/es5')
const join = require('ramda/src/join')

const sheet = sheetify('./dateTimePickerWidget.styl')

module.exports = (idPrefix, inputCallback, opts={}) => {
  t.String(idPrefix, ['idPrefix',])
  t.Function(inputCallback, ['inputCallback',])
  t.Object(opts, ['opts',])

  const {required, classes, timeValue,} = opts
  const now = DateTime.utc()
  const {dateValue,} = opts
  logger.debug(`Rendering, dateValue: ${dateValue}, timeValue: ${timeValue}`)
  const allClasses = concat(classes || [], ['datetime-picker-input',])
  const classesStr = `.${join('.', allClasses)}`
  const datePlaceholder = !bowser.safari ? 'Date' : 'Date: dd/mm/yyyy'
  const timePlaceholder = !bowser.safari ? 'Time' : 'Time: hh:mm'
  return h(`.${sheet}`, h(`.datetime-picker`, [
    h(`input#${idPrefix}-date${classesStr}`, {
      type: 'date', placeholder: datePlaceholder, required: !!required, min: now.toISODate(),
      oninput: (event) => {
        logger.debug(`Date input: ${event.target.value}`)
        inputCallback('date', event.target.value)
      },
      value: dateValue,
    }),
    h(`input#${idPrefix}-time${classesStr}`, {
      type: 'time', required: !!required, placeholder: timePlaceholder, oninput: (event) => {
        logger.debug(`Time input: ${event.target.value}`)
        inputCallback('time', event.target.value)
      },
      value: timeValue,
    }),
  ]))
}
