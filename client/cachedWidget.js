const document = require('global/document')
const onload = require('on-load')
const t = require('tcomb')

const logger = require('./logger').child({module: 'cachedWidget',})

module.exports = (createNode, opts={}) => {
  t.Function(createNode, ['createNode',])

  const {updateHandler, onloadHandler, callerId,} = opts
  if (updateHandler != null) {
    t.Function(updateHandler, ['opts', 'updateHandler',])
  }
  if (onloadHandler != null) {
    t.Function(onloadHandler, ['opts', 'onloadHandler',])
  }
  t.String(callerId, ['opts', 'callerId',])
  let element = null
  let proxy = null

  return (...args) => {
    logger.debug(`Rendering, arguments:`, args)
    if (element == null) {
      logger.debug(`Creating element`)
      element = createNode(...args)
      t.Object(element, ['element',])

      onload(element, () => {
        if (onloadHandler != null) {
          logger.debug(`Element was loaded, invoking onloadHandler`)
          onloadHandler(element, ...args)
        }
      }, () => {
        logger.debug(`Element was dismounted`)
        element = null
        proxy = null
      }, callerId)

      return element
    } else {
      if (proxy == null) {
        logger.debug(`Creating proxy`)
        proxy = document.createElement('div')
        proxy.isSameNode = function (el) {
          return (el === element)
        }
      }

      if (updateHandler != null) {
        logger.debug(`Sending update`)
        updateHandler(...args)
      }

      return proxy
    }
  }
}
