const {SError,} = require('error')
const mergeRight = require('ramda/src/mergeRight')

class BadRequestError extends SError {
  static create(message, info) {
    return super.create(message || 'Bad request', mergeRight(info, {
      statusCode: 400,
    }))
  }

  static statusCode() {
    return 400
  }
}

class NotFoundError extends SError {
  static create(message, info) {
    return super.create(message || 'Resource not found', mergeRight(info, {
      statusCode: 404,
    }))
  }

  static statusCode() {
    return 404
  }
}

module.exports = {
  BadRequestError,
  NotFoundError,
}
