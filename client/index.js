require('@babel/polyfill')
const documentReady = require('document-ready')
const choo = require('choo')
const partial = require('ramda/src/partial')
const forEach = require('ramda/src/forEach')
const mergeRight = require('ramda/src/mergeRight')
const t = require('tcomb')
const pick = require('ramda/src/pick')
const assert = require('assert')

const logger = require('./logger')
const {emitterForComponent,} = require('./chooComp')
const routeHandler = require('./routeHandler')
const notFoundView = require('./views/notFound')
const homeView = require('./views/home')
const groupsView = require('./views/groups')
const loginView = require('./views/login')
const logoutView = require('./views/logout')
const signupView = require('./views/signup')
const userProfileView = require('./views/userProfile')
const groupProfileView = require('./views/groupProfile')
const newMeetView = require('./views/newMeet')
const newGroupView = require('./views/newGroup')
const meetView = require('./views/meet')
const editMeetView = require('./views/editMeet')
const editGroupView = require('./views/editGroup')
const settingsView = require('./views/settings')

const initialState = JSON.parse(
  document.getElementById('initial-state').getAttribute('data-json')
)
logger.debug(`Initial state as sent by server:`, initialState)
window.initialState = mergeRight(initialState, {
  areApisLoaded: false,
})
const app = choo()
app.use(async (state, emitter) => {
  t.Object(state, ['state',])
  t.Object(state.events, ['state', 'events',])
  t.Object(emitter, ['emitter',])
  state.components = {}
  state.currentRoute = null

  documentReady(() => {
    logger.debug(`Document is loaded, initiating render`)
    state.areApisLoaded = true
    emitter.emit('render')
  })
})
forEach((view) => {
  t.String(view.route, ['view', 'route',])
  t.Function(view.render, ['view', 'render',])
  app.use((state, emitter) => {
    t.Object(state, ['state',])
    t.Object(state.components, ['state', 'components',])
    const componentState = mergeRight(pick(['events',], state), {
      global: state,
      components: {},
    })
    state.components[view.name] = componentState

    if (view.initialize != null) {
      t.Function(view.initialize, ['view', 'initialize',])
      logger.debug(`Calling initialize hook for view ${view.name}`)
      try {
        view.initialize(componentState, emitterForComponent([view.name,], emitter, state))
      } catch (error) {
        logger.error(`Initializing view ${view.name} failed`, error)
        emitter.emit('error', `Initializing view ${view.name} failed`, error)
        return
      }

      logger.debug(`Successfully initialized view ${view.name}, its initial state:`,
          componentState)
    }
  })

  app.route(view.route, partial(routeHandler, [view,]))
}, [
  homeView,
  loginView,
  logoutView,
  signupView,
  userProfileView,
  settingsView,
  groupProfileView,
  newMeetView,
  meetView,
  editMeetView,
  groupsView,
  newGroupView,
  editGroupView,
  notFoundView,
])

const tree = app.start()
logger.debug(`Started app`)
const containerElem = document.getElementById('container')
assert.notEqual(containerElem, null)
containerElem.appendChild(tree)
