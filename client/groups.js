const t = require('tcomb')
const any = require('ramda/src/any')

// Is a user an admin of a certain group?
const isGroupAdmin = (user, groupPath) => {
  t.Object(user, ['user',])
  t.String(groupPath, ['groupPath',])
  return any((membership) => {
    return membership.groupPath === groupPath && membership.membership.toLowerCase() === 'admin'
  }, user.groups)
}

module.exports = {
  isGroupAdmin,
}
