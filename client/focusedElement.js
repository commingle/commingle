const h = require('@arve.knudsen/hyperscript')
const onload = require('on-load')

module.exports = (tag, properties, children) => {
  const element = h(tag, properties, children)
  onload(element, (loadedElement) => {
    loadedElement.focus()
  }, null, 'focusedElement')
  return element
}
