const map = require('ramda/src/map')
const toPairs = require('ramda/src/toPairs')
const Promise = require('bluebird')
const join = require('ramda/src/join')
const reduce = require('ramda/src/reduce')
const pick = require('ramda/src/pick')
const {isNullOrBlank,} = require('@arve.knudsen/stringutils')
const assert = require('assert')

const logger = require('./logger').child({module: 'ajax',})
const {NotFoundError, BadRequestError,} = require('./errors')

logger.debug(`ajax logging`)

const ajax = async (method, uri, params, payload) => {
  const paramStr = join('&', map(([param, value,]) => {
    if (value == null) {
      value = ''
    }
    return `${encodeURIComponent(param)}=${encodeURIComponent(value)}`
  }, toPairs(params || {})))
  const queryPart = !isNullOrBlank(paramStr) ? `?${paramStr}` : ''
  const absPath = `${uri}${queryPart}`
  const payloadJson = payload != null ? JSON.stringify(payload) : null

  return await new Promise((resolve, reject) => {
    makeRequest(absPath, method, payloadJson, resolve, reject)
  })
}

const httpStatus2Exception = reduce((acc, exceptionType) => {
  acc[exceptionType.statusCode()] = exceptionType
  return acc
}, {}, [BadRequestError,])

const makeRequest = (absPath, method, payloadJson, resolve, reject) => {
  logger.debug(`Making ajax request to ${absPath}`)
  const request = new XMLHttpRequest()
  request.onreadystatechange = () => {
    if (request.readyState === XMLHttpRequest.DONE) {
      logger.debug('Received response from server:', request)
      if (request.status === 200) {
        resolveWithResponse(request.responseText, resolve, reject)
      } else {
        logger.debug(`Response was not successful: ${request.status}`)
        let error
        if (request.status === 404) {
          error = NotFoundError.create('')
        } else {
          const exceptionType = httpStatus2Exception[request.status]
          const data = !isNullOrBlank(request.responseText) ? JSON.parse(request.responseText) : {}
          if (exceptionType != null) {
            error = exceptionType.create(data.message || '', pick(['details',], data))
            logger.debug(`Translating to exception:`, {error,})
          } else {
            logger.debug(`Translating to default exception`)
            error = new Error(data.message)
          }
        }
        assert.notEqual(error, null)

        reject(error)
      }
    }
  }

  request.open(method.toUpperCase(), absPath)
  request.setRequestHeader('Content-Type', 'application/json;charset=UTF-8')
  if (payloadJson != null) {
    logger.debug(`Sending JSON`)
  } else {
    logger.debug(`Not sending JSON`)
  }
  request.send(payloadJson)
}

const resolveWithResponse = (json, resolve, reject) => {
  if (!isNullOrBlank(json)) {
    let result
    try {
      result = JSON.parse(json)
    } catch (error) {
      logger.warn(`Received malformed JSON from server:`, json)
      reject(new Error(`Parsing JSON from server failed: ${error}`))
      return
    }

    resolve(result)
  } else {
    resolve()
  }
}

module.exports = {
  getJson: (path, params) => {
    return ajax('get', path, params)
  },
  postJson: (path, payload) => {
    return ajax('post', path, null, payload)
  },
  putJson: (path, payload) => {
    return ajax('put', path, null, payload)
  },
  patchJson: (path, payload) => {
    return ajax('patch', path, null, payload)
  },
  delete: (path) => {
    return ajax('delete', path)
  },
}
