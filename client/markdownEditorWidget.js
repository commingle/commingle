const MarkdownEditor = require('@arve.knudsen/pagedown/Markdown.Editor').Editor
const MarkdownSanitizer = require('@arve.knudsen/pagedown/Markdown.Sanitizer')
const markdownExtra = require('@arve.knudsen/pagedown/Markdown.Extra').Extra
const h = require('@arve.knudsen/hyperscript')
const sheetify = require('sheetify')
const t = require('tcomb')

const logger = require('./logger').child({module: 'markdownEditorWidget',})
const cachedWidget = require('./cachedWidget')

const sheet = sheetify('./markdownEditorWidget.styl')

let markdownEditor
module.exports = {
  markdownEditorWidget: (description) => {
    return cachedWidget((state, emit, changeHandler) => {
      t.Object(state, ['state',])
      t.Function(emit, ['emit',])
      t.Function(changeHandler, ['changeHandler',])
      const converter = MarkdownSanitizer.getSanitizingConverter()
      const purpose = 'markdown' // TODO
      markdownExtra.init(converter, {
        extensions: [
          'tables',
          'fenced_code_gfm',
          'def_list',
          'attr_list',
          'footnotes',
          'strikethrough',
        ],
      })
      markdownEditor = new MarkdownEditor(converter, `-${purpose}`, {
        icons: {
          bold: 'bold',
          italic: 'italic',
          link: 'link',
          quote: 'bubble-quote',
          code: 'code',
          image: 'image',
          olist: 'list-numbered',
          ulist: 'list',
          heading: 'font-size2',
          hr: 'ruler',
          undo: 'undo2',
          redo: 'redo2',
          help: 'question',
        },
      })
      markdownEditor.hooks.set('onPreviewRefresh', changeHandler)

      return h(`.${sheet}`, [
        h('.wmd-panel', [
          h(`#wmd-button-bar-${purpose}.wmd-button-bar`),
          h(`textarea#wmd-input-${purpose}.wmd-input`),
          h(`#wmd-preview-${purpose}.wmd-preview.wmd-panel`),
        ]),
      ])
    }, {
      callerId: 'markdownEditor',
      onloadHandler: () => {
        markdownEditor.render(description)
      },
      updateHandler: (state) => {
        logger.debug(`Rendering update...`)
        t.Object(state, ['state',])
        t.String(state.input.description, ['state', 'input', 'description',])
        if (state.shouldResetMarkdownEditor) {
          logger.debug(`Resetting text to ${state.input.description}`)
          markdownEditor.render(state.input.description)
          state.shouldResetMarkdownEditor = false
        }
      },
    })
  },
  getMarkdownEditor: () => {
    return markdownEditor
  },
}
