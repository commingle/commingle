const forEach = require('ramda/src/forEach')
const toPairs = require('ramda/src/toPairs')
const isEmpty = require('ramda/src/isEmpty')
const join = require('ramda/src/join')
const w = typeof window === 'undefined' ? require('@arve.knudsen/html-element') : window
const document = w.document

module.exports = (name, opts={}) => {
  const svgNamespace = 'http://www.w3.org/2000/svg'

  const svgElem = document.createElementNS(svgNamespace, 'svg')
  const classesStr = !isEmpty(opts.classes || []) ? ` ${join(' ', opts.classes)}` : ''
  forEach(([name, listener,]) => {
    svgElem.addEventListener(name, listener)
  }, toPairs(opts.eventListeners || []))
  svgElem.setAttribute('class', `icon icon-${name}${classesStr}`)

  const useElem = document.createElementNS(svgNamespace, 'use')
  useElem.setAttributeNS('http://www.w3.org/1999/xlink', 'href', `#icon-${name}`)
  svgElem.appendChild(useElem)
  return svgElem
}
