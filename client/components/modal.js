let h = require('@arve.knudsen/hyperscript')
const sheetify = require('sheetify')

const logger = require('../logger').child({module: 'components/modal',})
const widget = require('./_widget')

const sheet = sheetify('./modal.styl')

module.exports = widget(({closeCallback, headerLabel, content,}) => {
  logger.debug(`Rendering`, content)
  return h(`.${sheet}`, [
    h(`.modal-portal`, {
      onkeyup: (event) => {
        logger.debug(`Key up received`, event)
        if (event.keyCode === 27) {
          logger.debug(`Escape key pressed, closing`)
          closeCallback()
        }
      },
    }, [
      h('.Modal__Overlay', {
        onclick: closeCallback,
      }, [
        h('.Modal__Content', {
          'tabindex': '-1',
        }, [
          h('.modal', [
            h('.modal-header', [
              h('.modal-header-content', [
                h('button.close', {
                  onclick: closeCallback,
                }, '×'),
                h('h4.modal-label', headerLabel),
              ]),
            ]),
            h('.modal-body', [
              content,
            ]),
          ]),
        ]),
      ]),
    ]),
  ])
})
