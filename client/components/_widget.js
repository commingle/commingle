module.exports = (func) => {
  return (...args) => {
    let children = [], props = {}
    if (args.length === 2) {
      [props, children,] = args
    } else if (args.length === 1) {
      children = args[0]
    } else if (args.length !== 0) {
      throw new Error(`Wrong number of arguments: ${args.length}`)
    }

    return func(children, props)
  }
}
