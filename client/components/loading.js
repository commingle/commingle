const h = require('@arve.knudsen/hyperscript')
const sheetify = require('sheetify')

const {component,} = require('../chooComp')

const sheet = sheetify('./loading.styl')

module.exports = component({
  render: () => {
    return h(`.${sheet}`, h('.spinner', [
      h('.rect1'),
      h('.rect2'),
      h('.rect3'),
      h('.rect4'),
      h('.rect5'),
    ]))
  },
})
