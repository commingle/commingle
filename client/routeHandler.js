const t = require('tcomb')
const assert = require('assert')
const forEach = require('ramda/src/forEach')
const {isNullOrBlank,} = require('@arve.knudsen/stringutils')
const toPairs = require('ramda/src/toPairs')
const onload = require('on-load')

const logger = require('./logger').child({module: 'routeHandler',})
const loadingComponent = require('./components/loading')(['routeHandler', 'loading',])
const layout = require('./views/layout')
const {emitForComponent,} = require('./chooComp')

const loadViewData = async (view, routeRepresentation, componentState, componentEmit, emit) => {
  t.Object(view, ['view',])
  t.String(routeRepresentation, ['routeRepresentation',])
  t.Object(componentState, ['componentState',])
  t.Function(componentEmit, ['componentEmit',])
  t.Function(emit, ['emit',])

  logger.debug(`Calling view's loadData hook since its data hasn't been loaded`, componentState)
  componentState.loadingState = `${routeRepresentation}:loading`

  let wasFound = true
  try {
    await view.loadData(componentState, componentEmit)
  } catch (error) {
    if (error.type === 'notFound') {
      logger.debug(`loadData hook of view ${view.name} failed due to resource not being found`)
      wasFound = false
    } else {
      logger.debug(`loadData hook of view ${view.name} failed due to an unknown error`, error)
      return
    }
  }

  if (wasFound) {
    logger.debug(`View ${view.name} finished loading data, triggering re-render`)

    if (view.resetState != null) {
      logger.debug(`Calling view's resetState hook`)
      view.resetState(componentState)
    }

    componentState.loadingState = `${routeRepresentation}:loaded`
    let pageTitle
    if (typeof view.pageTitle === 'function') {
      pageTitle = view.pageTitle(componentState)
      const title = !isNullOrBlank(pageTitle) ? pageTitle : 'Commingle'
      emit('DOMTitleChange', title)
    }
    emit('render')
  } else {
    logger.debug(`Since the view's resource wasn't found, redirecting to homepage`)
    emit('pushState', '/')
  }
}

// Prepare view for the current route.
//
// Returns whether view is ready for rendering or needs to load.
const prepareView = (view, routeRepresentation, emit, componentState, componentEmit) => {
  t.Object(view, ['view',])
  t.String(routeRepresentation, ['routeRepresentation',])
  t.Function(emit, ['emit',])
  t.Object(componentState, ['componentState',])
  t.Function(componentEmit, ['componentEmit',])

  logger.debug(`Preparing view ${view.name}`)

  let loadDataPromise
  const shouldReloadData = view.shouldReloadData != null ? view.shouldReloadData :
    () => {return false}
  if (view.loadData != null && (componentState.loadingState !== `${routeRepresentation}:loaded` ||
    shouldReloadData(componentState))) {
    loadDataPromise = loadViewData(view, routeRepresentation, componentState, componentEmit, emit)
  }

  if (loadDataPromise == null && view.resetState != null) {
    logger.debug(`Calling view's resetState hook`)
    view.resetState(componentState)
  }

  window.scrollTo(0, 0)

  let pageTitle
  if (typeof view.pageTitle === 'function') {
    pageTitle = view.pageTitle(componentState)
  } else if (typeof view.pageTitle === 'string') {
    pageTitle = view.pageTitle
  }
  const title = !isNullOrBlank(pageTitle) ? pageTitle : 'Commingle'
  emit('DOMTitleChange', title)
  return loadDataPromise == null
}

const constructRouteRepresentation = (state) => {
  let queryStr = '?'
  forEach(([key, value,]) => {
    queryStr += `${key}=${value}&`
  }, toPairs(state.query))
  queryStr = queryStr.slice(0, queryStr.length - 1)
  return `${!state.href.startsWith('/') ? '/' : ''}${state.href}${queryStr.length > 1 ?
    queryStr : ''}`
}

// Render the current route
module.exports = (view, state, emit) => {
  t.Object(state, ['state',])
  t.Object(state.events, ['state', 'events',])
  t.String(state.href, ['state', 'href',])
  t.Object(state.params, ['state', 'params',])
  t.Object(state.query, ['state', 'query',])
  t.String(state.route, ['state', 'route',])
  t.String(view.name, ['view', 'name',])
  t.Function(view.render, ['view', 'render',])
  logger.debug(`Rendering route ${state.route}, state:`, state)

  if (!state.areApisLoaded) {
    logger.debug(`APIs aren't yet loaded, rendering loading view`)
    return layout(loadingComponent.render(state, emit), state, emitForComponent(['layout',], emit),
      [])
  }

  if (view.requiresAuthentication && state.loggedInUser == null) {
    logger.debug(`The route requires authentication - redirecting to login page`)
    // Defer event because choo will ignore requests to redraw during a render pass
    setTimeout(() => {
      emit('pushState', `/login?next=${state.route}`)
    }, 0)
    return layout(loadingComponent.render(state, emit), state, emitForComponent(['layout',], emit),
      [])
  }

  const componentState = state.components[view.name]
  t.Object(componentState, ['state', 'components', view.name,])
  t.Object(componentState.global, ['state', 'components', view.name, 'global',])
  const componentEmit = emitForComponent([view.name,], emit)
  forEach((propName) => {
    componentState[propName] = state[propName]
  }, ['href', 'params', 'query', 'route',])

  // When rendering a certain route, we will load the corresponding view's data if it's not
  // already cached for the route. Otherwise we just re-use it
  let rendered
  let renderedLeftColumnSections = null
  let isViewReady
  const routeRepresentation = constructRouteRepresentation(state)
  if (routeRepresentation !== state.currentRoute) {
    logger.debug(
      `The displayed route changed from ${state.currentRoute} to ${routeRepresentation}`)
    state.currentRoute = routeRepresentation
    isViewReady = prepareView(view, routeRepresentation, emit, componentState, componentEmit)
  } else {
    if (componentState.loadingState === `${routeRepresentation}:loading`) {
      isViewReady = false
    } else {
      if (view.loadData != null) {
        assert.strictEqual(componentState.loadingState, `${routeRepresentation}:loaded`)
      }
      logger.debug(`The view is ready`)
      isViewReady = true
    }
  }

  if (isViewReady) {
    logger.debug(`Calling render hook for view ${view.name}, its state:`, componentState)
    assert.ok(state.areApisLoaded, `External APIs should be loaded by now`)
    rendered = view.render(componentState, componentEmit)
    if (typeof rendered !== 'string') {
      assert.notEqual(rendered, null, `View's render hook should return a DOM object or tree`)
      if (view.onLoad != null || view.onUnload != null) {
        onload(rendered, () => {
          if (view.onLoad != null) {
            logger.debug(`View was mounted, calling its onLoad hook`)
            view.onLoad(componentState, emitForComponent([view.name,], emit))
          }
        }, () => {
          if (view.onUnload != null) {
            logger.debug(`View was unmounted, calling its onUnload hook`)
            view.onUnload(componentState, emit)
          }
        }, view.name)
      }
      if (view.renderLeftColumnSections != null) {
        logger.debug(`Rendering view's left column sections`)
        renderedLeftColumnSections = view.renderLeftColumnSections(componentState, componentEmit)
      }
    } else {
      t.String(rendered, ['rendered',])
      let redirectTo
      if (rendered === 'redirectToHomepage') {
        logger.debug(`View indicated to redirect to the homepage`)
        redirectTo = '/'
      } else {
        const m = /redirectTo:[^:]+/.exec(rendered)
        assert.notEqual(m, null, `Invalid render result: ${rendered}`)
        redirectTo = m[1]
      }
      // Defer event because choo will ignore requests to redraw during a render pass
      setTimeout(() => {
        emit.global('pushState', redirectTo)
      }, 0)
      rendered = loadingComponent.render(state, emit)
    }
  } else {
    logger.debug(`View is in loading state, rendering loading view`)
    rendered = loadingComponent.render(state, emit)
  }
  if (!Array.isArray(rendered)) {
    t.Object(rendered, ['rendered',])
  }

  return layout(rendered, state, emitForComponent(['layout',], emit), renderedLeftColumnSections)
}
